# Growth-SOS-MSB-2021

This repository forms the supplementary data and analysis scripts for the manuscript:

Jaramillo-Riveri, S., Broughton, J., McVey, A., Pilizota, T., Scott, M., & El Karoui, M. (2022). Growth-dependent heterogeneity in the DNA damage response in Escherichia coli. Mol Syst Biol. 18:e10441 DOI: https://doi.org/10.15252/msb.202110441

It also contains segment data from the snapshot agar-pad experiments (/fluo_snapshot/segmentfiles) and mother machine data tables (mmc_segmentation-and-lineage-data).

The raw images can be found at: https://www.ebi.ac.uk/biostudies/studies/S-BSST833#

# Image aquisition

Microscope was controlled from MATLAB via MicroManager. See: https://gitlab.com/MEKlab/MicroscopeControl

Microscopy images can be found at https://doi.org/10.7488/ds/3064. Only one experimental repeat per condition was uploaded due to storage limitations. The entire dataset is available upon request.

# Snapshot data

## Cell segmentation and post-processing

Images were segmented using an in-house algorithm and curated manually
for errors. See: https://gitlab.com/MEKlab/Segment

Scripts for post-processing the images and generating plots for the paper
figures can be found in "fluo_snapshot/". 

Please note the the Segment/src functions must be within MATLAB path
for these scripts to work.

# Mother machine data

## Mother machine design

The mother machine silicone wafer was designed using a custom OpenSCAD script,
see: https://gitlab.com/MEKlab/Microfluidics

## Segmenatation and lineage tracking

Microscopy images were segmented and quantified using BACMMAN. Data exported from BACMMAN corresponding to the channels are located in "/mmc_segmentation-and-lineage-data/BACMMAN export" and were reorganised as separate tables located in "/mmc_segmentation-and-lineage-data/Data tables" which were used for post analysis.

## Post-processing

All post-processing scripts used to analyse data andgenerate figures can be found at "mmc_postanalysis/". 

Scripts are meant to be run in the order indicated by their number:
meaning "mmc0" followed by "mmc1" and so one. The reason is that poor
quality lineages or with conflicting trajectories are discarded
progressively through the analysis. 

Please note the location of output from the segmentation and tracking
analysis must match for the scrips to run.


