
mmc0_aux();
PaperPosition = [0,0,17,10];
outlog = fopen('mmc7_summaryplots.log','w');
plotsfolder = 'mmc7_summaryplots_plots/'; % Where plots will be saved
load('mmc6_alpha_output.mat');
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder
makeplots=1;

[nstrains,nmedias] = size(tables.(areakey));

% ====================== SUMARY PLOTS // SCATTER // PERTAINING TO DIVISION
if(makeplots)
    fig1 = figure('visible','off');
    fig2 = figure('visible','off');
    fig3 = figure('visible','off');
    fig4 = figure('visible','off');
    fig5 = figure('visible','off');
    fprintf(outlog,'Saving scatter plots related to division times\n');
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            grs = [];
            r2s = [];
            divs = [];
            gfps = [];
            dlbirth = [];
            dldiv = [];
            for nr = 1:nrepeats % for each repeat
                gratetable = tables.(gratekey){ns,nm}{nr};
                divtable = tables.(divtimekey){ns,nm}{nr};
                r2table = tables.(r2key){ns,nm}{nr};
                gfptable = tables.('max_gfp'){ns,nm}{nr};
                dlbirthtable = tables.('birthlens'){ns,nm}{nr};
                dldivtable = tables.('divlens'){ns,nm}{nr};
                divs = [divs;divtable(not(isnan(divtable)))];
                grs = [grs;gratetable(not(isnan(divtable)))];
                r2s = [r2s;r2table(not(isnan(divtable)))];
                gfps = [gfps;gfptable(not(isnan(divtable)))];
                dlbirth = [dlbirth;dlbirthtable(not(isnan(divtable)))];
                dldiv = [dldiv;dldivtable(not(isnan(divtable)))];
            end
            if(not((length(divs)==length(grs))&&(length(divs)==length(gfps))))
                fprintf(outlog,'WARNING: inconsistent number of points\n');
                continue;
            end
            set(0,'CurrentFigure',fig1); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(gfps,divs,'logx',true);
            figlabels('xlabel','Max GFP [A.U]',...
                      'ylabel','Division time [h]',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.5 50 0 18]);
            xticks([1 5 20 50]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig2); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(divs(not(isnan(grs))),grs(not(isnan(grs))));
            figlabels('xlabel','Division time [h]',...
                      'ylabel','Length growth-rate',...
                      'title',[strains{ns},'/',medias{nm}]);            
            xticks([1 5 10 15]);
            axis([0 18 0 2]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig3); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(divs(not(isnan(r2s))),grs(not(isnan(r2s))));
            figlabels('xlabel','Division time [h]',...
                      'ylabel','Length growth-rate R$^2$',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0 18 0 1]);
            xticks([1 5 10 15]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig4); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(dlbirth,dldiv);
            figlabels('xlabel','Length at birth [$\mu$m]',...
                      'ylabel','Length at division [$\mu$m]',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0 15 0 15]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig5); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(gfps,dldiv,'logx',true);
            figlabels('xlabel','Max GFP [A.U]',...
                      'ylabel','Length at division [$\mu$m]',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.5 50 0 30]);
            set(gca,'FontSize',10);
            xticks([1 5 20 50]);
        end
    end
    figs = [fig1,fig2,fig3,fig4,fig5];
    names = {'SUMMARIES_SCATTER_GFP_DivTime',...
             'SUMMARIES_SCATTER_DivTime_GrRate',...
             'SUMMARIES_SCATTER_DivTime_GrRateR2',...
             'SUMMARIES_SCATTER_BirthLength_DivisionLength',...
             'SUMMARIES_SCATTER_GFP_DivisionLength'};
    for nfig = 1:length(figs)
        fig = figs(nfig);
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = PaperPosition;
        print('-r600',fig,['./',plotsfolder,names{nfig},'.png'],'-dpng');
    end
    close all;
end

% ====================== SUMARY PLOTS // SCATTER // PERTAINING TO GROWTH RATE
if(makeplots)
    fig1 = figure('visible','off');
    fig2 = figure('visible','off');
    fig3 = figure('visible','off');
    fig4 = figure('visible','off');
    fig5 = figure('visible','off');
    fprintf(outlog,'Saving scatter plots related to growth-rates\n');
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            xvals = [];
            yvals = [];
            grs  = [];
            r2s  = [];
            gfps = [];
            mchs = [];
            for nr = 1:nrepeats % for each repeat
                gratetable = tables.(gratekey){ns,nm}{nr};
                r2table = tables.(r2key){ns,nm}{nr};
                gfptable = tables.('max_gfp'){ns,nm}{nr};
                mchtable = tables.('mean_mcherry'){ns,nm}{nr};
                grs = [grs;gratetable(not(isnan(gratetable)))];
                r2s = [r2s;r2table(not(isnan(gratetable)))];
                gfps = [gfps;gfptable(not(isnan(gratetable)))];
                mchs = [mchs;mchtable(not(isnan(gratetable)))];
            end
            set(0,'CurrentFigure',fig1); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(gfps,grs,'logx',true);
            figlabels('xlabel','Max GFP [A.U]',...
                      'ylabel','Length growth-rate',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.5 50 0 2]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig2); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(gfps,r2s,'logx',true);
            figlabels('xlabel','Max GFP [A.U]',...
                      'ylabel','Length growth-rate R$^2$',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.5 50 0 1]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig3); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(mchs,grs);
            figlabels('xlabel','Mean mkate [A.U]',...
                      'ylabel','Length growth-rate',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.25 2 0 3]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig4); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(mchs,r2s);
            figlabels('xlabel','Mean mkate [A.U]',...
                      'ylabel','Length growth-rate R$^2$',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0.25 2 0 3]);
            set(gca,'FontSize',10);
            set(0,'CurrentFigure',fig5); % 
            subplot(2,3,3*(ns-1)+nm); hold on;
            dscatter(grs,r2s);
            figlabels('xlabel','Length growth-rate',...
                      'ylabel','Length growth-rate R$^2$',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([0 2 0 1]);
            set(gca,'FontSize',10);
        end
    end
    PaperPosition = [0,0,17,10];
    figs = [fig1,fig2,fig3,fig4,fig5];
    names = {'SUMMARIES_SCATTER_GFP_GrRate',...
             'SUMMARIES_SCATTER_GFP_GrRateR2',...
             'SUMMARIES_SCATTER_mKate_GrRate',...
             'SUMMARIES_SCATTER_mKate_GrRateR2',...
             'SUMMARIES_SCATTER_GrRate_GrRateR2'};
    for nfig = 1:length(figs)
        fig = figs(nfig);
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = PaperPosition;
        print('-r600',fig,['./',plotsfolder,names{nfig},'.png'],'-dpng');
    end
    close all;
end

% ====================== SUMARY PLOTS // DISTRIBUTIONS
if(makeplots)
    fig1 = figure('visible','off');
    fig2 = figure('visible','off');
    fig3 = figure('visible','off');
    fig4 = figure('visible','off');
    fig5 = figure('visible','off');
    fig6 = figure('visible','off');
    fig7 = figure('visible','off');
    fig8 = figure('visible','off');
    fig9 = figure('visible','off');
    fig10 = figure('visible','off');
    mediapos = [1,2,3];
    colors = lines(3);
    fprintf(outlog,'Saving figures with the distributions associated to cell cycle variables\n');
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                divtimes = tables.('divtimes'){ns,nm}{nr};
                bltable = tables.('birthlens'){ns,nm}{nr};
                dltable = tables.('divlens'){ns,nm}{nr};
                aratestable = tables.('area_grates'){ns,nm}{nr};
                ar2atestable = tables.('area_grates_r2'){ns,nm}{nr};
                lratestable = tables.('len_grates'){ns,nm}{nr};
                lr2atestable = tables.('len_grates_r2'){ns,nm}{nr};
                meangfptable = tables.('mean_gfp'){ns,nm}{nr};
                maxgfptable = tables.('max_gfp'){ns,nm}{nr};
                meanmchtable = tables.('mean_mcherry'){ns,nm}{nr};
                set(0,'CurrentFigure',fig1); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(0,20,80);
                vals = getvalids(divtimes(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Division time [h]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 15]);
                ylim([0 0.5]);
                %set(gca,'YScale','log');
                set(gca,'FontSize',10);
                set(0,'CurrentFigure',fig2); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(0,20,80);
                vals = getvalids(bltable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Birth length [$\mu$m]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 15]);
                ylim([0 0.5]);
                set(0,'CurrentFigure',fig3); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(0,20,80);
                vals = getvalids(dltable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Division length [$\mu$m]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 15]);
                ylim([0 0.5]);
                set(0,'CurrentFigure',fig4); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(-1,4,80);
                vals = getvalids(aratestable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Area growth-rate [1/h]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([-0.5 2]);
                ylim([0 0.4]);
                set(0,'CurrentFigure',fig5); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(-0.01,1.01,80);
                vals = getvalids(ar2atestable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Area growth-rate R$^2$',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 1]);
                ylim([0 0.3]);
                set(0,'CurrentFigure',fig6); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(-1,4,80);
                vals = getvalids(lratestable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Length growth-rate [1/h]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([-0.5 2]);
                ylim([0 0.4]);
                set(0,'CurrentFigure',fig7); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(-0.01,1.01,80);
                vals = getvalids(lr2atestable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Length growth-rate R$^2$',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 1]);
                ylim([0 0.5]);
                set(0,'CurrentFigure',fig8); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = logspace(log10(0.5),log10(50),80);
                vals = getvalids(meangfptable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Mean GFP [A.U.]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                set(gca,'XScale','log');
                xticks([1 5 20 50]);
                ylim([0 0.35]);
                set(0,'CurrentFigure',fig9); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = logspace(log10(0.5),log10(50),80);
                vals = getvalids(maxgfptable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Max GFP [A.U.]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                set(gca,'XScale','log');
                xticks([1 5 20 50]);
                ylim([0 0.35]);
                set(0,'CurrentFigure',fig10); %
                subplot(2,3,3*(ns-1)+nm); hold on;
                bins = linspace(0,4,80);
                vals = getvalids(meanmchtable(:));
                rf = relhist(vals,bins);
                plot(bins,rf,'-','LineWidth',2,'Color',colors(nr,:));
                figlabels('xlabel','Mean mKate2 [A.U.]',...
                          'ylabel','Rel. frequency',...
                          'title',[strains{ns},'/',medias{nm}]);
                xlim([0 4]);
                ylim([0 0.3]);
            end            
        end
    end
    figs = [fig1,fig2,fig3,fig4,fig5,fig6,fig7,fig8,fig9,fig10];
    names = {'SUMMARIES_DISTRIBUTION_DivisionTime',...
             'SUMMARIES_DISTRIBUTION_LengthAtBirth',...
             'SUMMARIES_DISTRIBUTION_LengthAtDivision',...
             'SUMMARIES_DISTRIBUTION_AreaGrowthRate',...
             'SUMMARIES_DISTRIBUTION_AreaGrowthRateR2',...
             'SUMMARIES_DISTRIBUTION_LengthGrowthRate',...
             'SUMMARIES_DISTRIBUTION_LengthGrowthRateR2',...
             'SUMMARIES_DISTRIBUTION_GFPMean',...
             'SUMMARIES_DISTRIBUTION_GFPMax',...
             'SUMMARIES_DISTRIBUTION_mKateMean'};
    for nfig = 1:length(figs)
        fig = figs(nfig);
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = PaperPosition;
        print('-r600',fig,['./',plotsfolder,names{nfig},'.png'],'-dpng');
    end
    close all;
end

% ====================== SUMARY PLOTS // ALPHA
PaperPosition = [0,0,17,6];
fig = figure('visible','off');
% $$$ for ns = 2%1:nstrains % for each strain folder
% $$$     for nm = 1:nmedias % for each media folder
% $$$         nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
% $$$         for nr = 1:nrepeats % for each repeat                    
% $$$             color = repeat2color.(repeats{ns,nm}{nr});
% $$$             alphas = tables.('alpha'){ns,nm}{nr};
% $$$             alpha_p5s = tables.('alpha_p5'){ns,nm}{nr};
% $$$             alpha_p95s = tables.('alpha_p95'){ns,nm}{nr};
% $$$             set(0,'CurrentFigure',fig); %
% $$$             subplot(1,3,nm); hold on;
% $$$             xe = 0.25;
% $$$             for i = 1:length(alphas)
% $$$                 plot([sosthrvec(i)-xe,sosthrvec(i)+xe],[alpha_p95s(i),alpha_p95s(i)],'color',color,'LineWidth',1);
% $$$                 plot([sosthrvec(i),sosthrvec(i)],[alpha_p5s(i),alpha_p95s(i)],'color',color,'LineWidth',1);
% $$$                 plot([sosthrvec(i)-xe,sosthrvec(i)+xe],[alpha_p5s(i),alpha_p5s(i)],'color',color,'LineWidth',1);
% $$$             end
% $$$         end
% $$$     end
% $$$ end
for ns = 2%1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat                    
            color = repeat2color.(repeats{ns,nm}{nr});
            alphas = tables.('alpha'){ns,nm}{nr};
            alpha_p5s = tables.('alpha_p5'){ns,nm}{nr};
            alpha_p95s = tables.('alpha_p95'){ns,nm}{nr};
            set(0,'CurrentFigure',fig); %
            subplot(1,3,nm); hold on;
            myupdownfill(sosthrvec,alpha_p5s,alpha_p95s,color,0.2,0);%
            plot(sosthrvec,alpha_p5s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,alpha_p95s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,alphas,':','LineWidth',1,'Color',color);
            plot(sosthrvec,alphas,'o','LineWidth',1,...
                 'MarkerSize',5,...
                 'MarkerFaceColor',color,...
                 'Color',color);
            figlabels('ylabel','$\alpha$ [1/h]',...
                      'xlabel','GFP Threshold [A.U.]',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([min(sosthrvec) max(sosthrvec) 0 0.045]);
        end
    end
end
fig.PaperUnits = 'centimeters';
fig.PaperPosition = PaperPosition;
print('-r600',fig,['./',plotsfolder,'SUMMARY_ALPHA_MAXLLH.png'],'-dpng');
close(fig);

