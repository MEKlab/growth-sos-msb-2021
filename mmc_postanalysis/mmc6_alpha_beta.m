
mmc0_aux();
outlog = fopen('mmc6_alpha.log','w');
plotsfolder = 'mmc6_alpha_beta_plots/'; % Where plots will be saved
load('mmc4_secondfilter_output.mat');
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

nsmooth = 5; % we will smooth the trajectories to remove some of the noise.
sosthrvec = 5:20;
alpharange1 = logspace(log10(1e-10),log10(1e-1),1000);
alpharange2 = linspace(0,0.1,200);
betarange1 = logspace(log10(1e-10),log10(5e-1),1000);
betarange2 = linspace(0,0.3,200);
PaperPosition = [0,0,17,10];
niter = 100000;
epsilon = 0.005;
                
[nstrains,nmedias] = size(tables.(areakey));
% initialize the 'alpha' table
tables.('alpha') = cell(nstrains,nmedias);
tables.('alpha_p5') = cell(nstrains,nmedias);
tables.('alpha_p95') = cell(nstrains,nmedias);
tables.('alpha_T0') = cell(nstrains,nmedias);
tables.('alpha_T1') = cell(nstrains,nmedias);
tables.('beta') = cell(nstrains,nmedias);
tables.('beta_p5') = cell(nstrains,nmedias);
tables.('beta_p95') = cell(nstrains,nmedias);
tables.('beta_T0') = cell(nstrains,nmedias);
tables.('beta_T1') = cell(nstrains,nmedias);
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat
            tables.('alpha'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_p5'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_p95'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_T0'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_T1'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('beta'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('beta_p5'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('beta_p95'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('beta_T0'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('beta_T1'){ns,nm}{nr} = zeros(size(sosthrvec));
        end
    end
end

for nthr = 1:length(sosthrvec)
    highsosthr = sosthrvec(nthr);    
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                fig = figure(); hold on; % 'visible','off'
                color = repeat2color.(repeats{ns,nm}{nr});
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr)/60;
                gfptable = tables.(gfpkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                alpha_t1vec = [];
                alpha_t0vec = [];
                beta_t1vec = [];
                beta_t0vec = [];
                for lin = 1:size(gfptable,1)
                    gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
                    times = getvalids(timetable(lin,:));
                    if((length(gfpsp)>0)&&(gfpsp(1)<highsosthr))
                        % we care only lineages that begin with low SOS
                        if(sum(gfpsp>=highsosthr)>0) % some high SOS
                            timeth_pos = getfirst(find(gfpsp>=highsosthr));
                            alpha_t1vec = [alpha_t1vec,times(timeth_pos)-times(1)];
                            if(timeth_pos<length(times)) % not the last point
                                if(sum(gfpsp(timeth_pos+1:end)<=highsosthr)>0)
                                    timeth_pos2 = getfirst(find(gfpsp(timeth_pos+1:end)<=highsosthr))+timeth_pos;
                                    beta_t1vec = [beta_t1vec,times(timeth_pos2)-times(timeth_pos)];
                                    plot(times(timeth_pos:end)-times(timeth_pos),gfpsp(timeth_pos:end))
                                else
                                    beta_t0vec = [beta_t0vec,times(end)-times(timeth_pos)];
                                end
                            end
                        else % lineage never reached
                            alpha_t0vec = [alpha_t0vec,times(end)-times(1)];
                        end
                    end
                end
                plot([0,14],[highsosthr,highsosthr],':','LineWidth',2,'Color',[0.3,0.3,0.3]);
                figlabels('xlabel','Time since crossing threshold [h]',...
                          'ylabel','GFP [a.u.]',...
                          'title',[strains{ns},'/',medias{nm}]);
                fig.PaperUnits = 'centimeters';
                fig.PaperPosition = PaperPosition;
                print('-r600',fig,['./',plotsfolder,'EXAMPLE_SOS-BETA-CURVES_',strains{ns},'_',repeat,'_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
                close(fig);
            end
        end
    end
end


for nthr = 1:length(sosthrvec)
    highsosthr = sosthrvec(nthr);
    fig = figure('visible','off'); %
    fig2 = figure('visible','off');
    fig3 = figure('visible','off');
    fig4 = figure('visible','off');
    for ns = 2%1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                color = repeat2color.(repeats{ns,nm}{nr});
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr)/60;
                gfptable = tables.(gfpkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                alpha_t1vec = [];
                alpha_t0vec = [];
                beta_t1vec = [];
                beta_t0vec = [];
                for lin = 1:size(gfptable,1)
                    gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
                    times = getvalids(timetable(lin,:));
                    if((length(gfpsp)>0)&&(gfpsp(1)<highsosthr))
                        % we care only lineages that begin with low SOS
                        if(sum(gfpsp>=highsosthr)>0) % some high SOS
                            timeth_pos = getfirst(find(gfpsp>=highsosthr));
                            alpha_t1vec = [alpha_t1vec,times(timeth_pos)-times(1)];
                            if(timeth_pos<length(times)) % not the last point
                                if(sum(gfpsp(timeth_pos+1:end)<=highsosthr)>0)
                                    timeth_pos2 = getfirst(find(gfpsp(timeth_pos+1:end)<=highsosthr))+timeth_pos;
                                    beta_t1vec = [beta_t1vec,times(timeth_pos2)-times(timeth_pos)];
                                else
                                    beta_t0vec = [beta_t0vec,times(end)-times(timeth_pos)];
                                end
                            end
                        else % lineage never reached
                            alpha_t0vec = [alpha_t0vec,times(end)-times(1)];
                        end
                    end
                end
                %% ALPHA
                [lhs,alpha_maxl,maxlh] = getalphalhs2(alpha_t1vec,alpha_t0vec,interval,alpharange1);
                [alphawalk,naccepted] = alphaMC(alpha_t1vec, alpha_t0vec, interval, niter, epsilon);
                p5 = prctile(alphawalk,5);
                p95 = prctile(alphawalk,95);
                tables.('alpha'){ns,nm}{nr}(nthr) = alpha_maxl;
                tables.('alpha_p5'){ns,nm}{nr}(nthr) = p5;
                tables.('alpha_p95'){ns,nm}{nr}(nthr) = p95;
                tables.('alpha_T0'){ns,nm}{nr}(nthr) = length(alpha_t0vec);
                tables.('alpha_T1'){ns,nm}{nr}(nthr) = length(alpha_t1vec);
                set(0,'CurrentFigure',fig); 
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(alpharange1,1.*lhs,'-','LineWidth',2,'Color',color);
                plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color);
                plot([alpha_maxl,alpha_maxl],[-1000,maxlh],':','LineWidth',1,'Color',color);
                plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                figlabels('xlabel','$\alpha$ [1/h]',...
                          'ylabel','log likelihood',...
                          'title',[strains{ns},'/',medias{nm}]);
                axis([min(alpharange1) max(alpharange1) -1000 -10]);
                set(0,'CurrentFigure',fig2); 
                subplot(2,3,3*(ns-1)+nm); hold on;
                rf = relhist(alphawalk,alpharange2);
                leftind = find(alpharange2>=p5);
                leftind = leftind(1);
                rightind = find(alpharange2>=p95);
                rightind = rightind(1);
                myupdownfill(alpharange2(leftind:rightind),zeros(size(leftind:rightind)),rf(leftind:rightind),color,0.2,0);%
                plot([alpha_maxl,alpha_maxl],[0,max(rf)],':','LineWidth',1,'Color',color);
                plot(alpharange2,rf,'-','LineWidth',1,'Color',color);
                %plot([alpha_maxl],[max(rf)],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                ylim([0 0.25]);
                xlim([0 0.05]);
                figlabels('xlabel','$\alpha$ [1/h]',...
                          'ylabel','p.d.f',...
                          'title',[strains{ns},'/',medias{nm}]);
                %% BETA
                [lhs,beta_maxl,maxlh] = getalphalhs2(beta_t1vec,beta_t0vec,interval,alpharange1);
                [betawalk,naccepted] = alphaMC(beta_t1vec, beta_t0vec, interval, niter, epsilon);
                p5 = prctile(betawalk,5);
                p95 = prctile(betawalk,95);
                tables.('beta'){ns,nm}{nr}(nthr) = beta_maxl;
                tables.('beta_p5'){ns,nm}{nr}(nthr) = p5;
                tables.('beta_p95'){ns,nm}{nr}(nthr) = p95;
                tables.('beta_T0'){ns,nm}{nr}(nthr) = length(beta_t0vec);
                tables.('beta_T1'){ns,nm}{nr}(nthr) = length(beta_t1vec);
                set(0,'CurrentFigure',fig3); 
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(betarange1,1.*lhs,'-','LineWidth',2,'Color',color);
                plot([beta_maxl],[maxlh],'o','LineWidth',1,'Color',color);
                plot([beta_maxl,beta_maxl],[-1000,maxlh],':','LineWidth',1,'Color',color);
                plot([beta_maxl],[maxlh],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                figlabels('xlabel','$\beta$ [1/h]',...
                          'ylabel','log likelihood',...
                          'title',[strains{ns},'/',medias{nm}]);
                axis([min(betarange1) max(betarange1) -300 0]);
                set(0,'CurrentFigure',fig4); 
                subplot(2,3,3*(ns-1)+nm); hold on;
                rf = relhist(betawalk,betarange2);
% $$$                 leftind = find(betarange2>=p5);
% $$$                 leftind = leftind(1);
% $$$                 rightind = find(betarange2>=p95);
% $$$                 rightind = rightind(1);
% $$$                 myupdownfill(betarange2(leftind:rightind),zeros(size(leftind:rightind)),rf(leftind:rightind),color,0.2,0);%
                plot([beta_maxl,beta_maxl],[0,max(rf)],':','LineWidth',1,'Color',color);
                plot(betarange2,rf,'-','LineWidth',1,'Color',color);
                %plot([alpha_maxl],[max(rf)],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                %ylim([0 0.25]);
                %xlim([0 0.05]);
                figlabels('xlabel','$\beta$ [1/h]',...
                          'ylabel','p.d.f',...
                          'title',[strains{ns},'/',medias{nm}]);
            end
        end
    end
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = PaperPosition;
    print('-r600',fig,['./',plotsfolder,'ALPHA_LLH_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig);
    fig2.PaperUnits = 'centimeters';
    fig2.PaperPosition = PaperPosition;
    print('-r600',fig2,['./',plotsfolder,'ALPHA_PDF_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig2);
    fig3.PaperUnits = 'centimeters';
    fig3.PaperPosition = PaperPosition;
    print('-r600',fig3,['./',plotsfolder,'BETA_LLH_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig3);
    fig4.PaperUnits = 'centimeters';
    fig4.PaperPosition = PaperPosition;
    print('-r600',fig4,['./',plotsfolder,'BETA_PDF_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig4);
end

fclose(outlog);
save('mmc6_alpha_output','tables','intervals','repeats','datafolder', ...
     'strains','medias','rmvsets','repeat2color','sosthrvec');

load('mmc6_alpha_output');


% ====================== SUMARY PLOTS // ALPHA
PaperPosition = [0,0,17,6];
fig = figure('visible','off'); % 
for ns = 2%1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat                    
            color = repeat2color.(repeats{ns,nm}{nr});
            betas = tables.('beta'){ns,nm}{nr};
            beta_p5s = tables.('beta_p5'){ns,nm}{nr};
            beta_p95s = tables.('beta_p95'){ns,nm}{nr};
            set(0,'CurrentFigure',fig); %
            subplot(1,3,nm); hold on;
            myupdownfill(sosthrvec,beta_p5s,beta_p95s,color,0.2,0);%
            plot(sosthrvec,beta_p5s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,beta_p95s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,betas,':','LineWidth',1,'Color',color);
            plot(sosthrvec,betas,'o','LineWidth',1,...
                 'MarkerSize',5,...
                 'MarkerFaceColor',color,...
                 'Color',color);
            figlabels('ylabel','$\beta$ [1/h]',...
                      'xlabel','GFP Threshold [A.U.]',...
                      'title',[strains{ns},'/',medias{nm}]);
            axis([min(sosthrvec) max(sosthrvec) 0 0.3]);
        end
    end
end
fig.PaperUnits = 'centimeters';
fig.PaperPosition = PaperPosition;
print('-r600',fig,['./',plotsfolder,'SUMMARY_BETA_MAXLLH.png'],'-dpng');
close(fig);

