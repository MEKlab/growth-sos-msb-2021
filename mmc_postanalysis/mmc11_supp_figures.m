% Making plots to be used as supplementary figures.

mmc0_aux();
PaperPosition = [0,0,17,10];
plotsfolder = 'mmc11_supp_figures_plots/'; % Where plots will be saved
load('mmc6_alpha_output.mat');
%system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

% ========== 1) Cell-cycle parameters showing distributions and over time
[nstrains,nmedias] = size(tables.(areakey));
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
        fig1 = figure('visible','off');
        ha11 = axes(); hold on;
        ha12 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
        fig2 = figure('visible','off');
        ha21 = axes(); hold on;
        ha22 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
        fig3 = figure('visible','off');
        ha31 = axes(); hold on;
        ha32 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
        fig4 = figure('visible','off');
        ha41 = axes(); hold on;
        ha42 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
        for nr = 1:nrepeats % for each repeat
            interval = intervals{ns,nm}(nr)/60;
            gfptable = tables.(gfpkey){ns,nm}{nr};
            mchtable = tables.(mchkey){ns,nm}{nr};
            gratetable = tables.(gratekey){ns,nm}{nr};
            divtable = tables.(divtimekey){ns,nm}{nr};
            divpostable = tables.('divpositions'){ns,nm}{nr};
            gfptable2 = tables.('mean_gfp'){ns,nm}{nr};
            meanmchtable2 = tables.('mean_mcherry'){ns,nm}{nr};
            divmed = zeros(1,size(gfptable,2));
            divup = zeros(1,size(gfptable,2));
            divdw = zeros(1,size(gfptable,2));
            grmed = zeros(1,size(gfptable,2));
            grup = zeros(1,size(gfptable,2));
            grdw = zeros(1,size(gfptable,2));
            gfpmed = zeros(1,size(gfptable,2));
            gfpup = zeros(1,size(gfptable,2));
            gfpdw = zeros(1,size(gfptable,2));
            mchmed = zeros(1,size(gfptable,2));
            mchup = zeros(1,size(gfptable,2));
            mchdw = zeros(1,size(gfptable,2));
            color = repeat2color.(repeats{ns,nm}{nr});
            for t = 1:size(gfptable,2)
                divs = [];
                grs = [];
                gfps = [];
                mchs = [];
                for lin = 1:size(divtable)
                    pos = divpostable(lin,:);
                    cands = find(pos<=t);
                    if(length(cands)>0)
                        div = divtable(lin,cands(end));
                        if(div>0)
                            divs = [divs,divtable(lin,cands(end))];
                            grs = [grs,gratetable(lin,cands(end))];
                            gfps = [gfps,gfptable2(lin,cands(end))];
                            mchs = [mchs,meanmchtable2(lin,cands(end))];
                        end
                    end
                end
                [divmed(t), divdw(t), divup(t)] = safemedian(divs);
                [grmed(t), grdw(t), grup(t)] = safemedian(grs);
                [gfpmed(t), gfpdw(t), gfpup(t)] = safemedian(gfps);
                [mchmed(t), mchdw(t), mchup(t)] = safemedian(mchs);
            end
            tvec = interval.*[0:size(gfptable,2)-1];
            % DIVS
            axes(ha11); hold on;
            vals = divtable(divtable(:)>0);
            bins = linspace(0,median(vals)*4,20);
            rf = 100*relhist(vals,bins);
            plot(bins,rf,'-','LineWidth',2,'Color',color);
            if(nm==1)
                xlim([0 20])
            elseif(nm==2)
                xlim([0 10])
            else
                xlim([0 5])
            end
            axes(ha12); hold on;
            nnanpos = not(isnan(divdw));
            myupdownfill(tvec(nnanpos),divdw(nnanpos),divup(nnanpos),color,0.2,0);
            plot(tvec,divmed,'-','LineWidth',1,'Color',color);
            xlim([0 20]);
            xticks([0 5 10 15 20]);
            if(nm==1)
                ylim([0 8])
            elseif(nm==2)
                ylim([0 3.5])
            else
                ylim([0 2])
            end
            % GRATES
            axes(ha21); hold on;
            vals = getvalids(gratetable(divtable(:)>0));
            bins = linspace(0,median(vals)*4,25);
            rf = 100*relhist(vals,bins);
            plot(bins,rf,'-','LineWidth',2,'Color',color);
            if(nm==1)
                xlim([0 1])
            elseif(nm==2)
                xlim([0 2])
            else
                xlim([0 4])
            end
            axes(ha22); hold on;
            nnanpos = not(isnan(grdw));
            myupdownfill(tvec(nnanpos),grdw(nnanpos),grup(nnanpos),color,0.2,0);
            plot(tvec,grmed,'-','LineWidth',1,'Color',color);
            xlim([0 20]);
            xticks([0 5 10 15 20]);
            if(nm==1)
                ylim([0 0.4])
            elseif(nm==2)
                ylim([0 0.7])
            else
                ylim([0 1.3])
            end
            % GFPS
            axes(ha31); hold on;
            vals = getvalids(gfptable2(divtable(:)>0));
            bins = linspace(0,median(vals)*4,25);
            rf = 100*relhist(vals,bins);
            plot(bins,rf,'-','LineWidth',2,'Color',color);
            if(nm==1)
                xlim([0 4])
            elseif(nm==2)
                xlim([0 4])
            else
                xlim([0 4])
            end
            axes(ha32); hold on;
            nnanpos = not(isnan(gfpdw));
            myupdownfill(tvec(nnanpos),gfpdw(nnanpos),gfpup(nnanpos),color,0.2,0);
            plot(tvec,gfpmed,'-','LineWidth',1,'Color',color);
            xlim([0 20]);
            xticks([0 5 10 15 20]);
            if(nm==1)
                ylim([0 1.7])
            elseif(nm==2)
                ylim([0 1.7])
            else
                ylim([0 1.7])
            end
            % MKATE
            axes(ha41); hold on;
            vals = getvalids(meanmchtable2(divtable(:)>0));
            bins = linspace(0,median(vals)*4,25);
            rf = 100*relhist(vals,bins);
            plot(bins,rf,'-','LineWidth',2,'Color',color);
            if(nm==1)
                xlim([0 8])
            elseif(nm==2)
                xlim([0 8])
            else
                xlim([0 8])
            end
            axes(ha42); hold on;
            nnanpos = not(isnan(mchdw));
            myupdownfill(tvec(nnanpos),mchdw(nnanpos),mchup(nnanpos),color,0.2,0);
            plot(tvec,mchmed,'-','LineWidth',1,'Color',color);
            xlim([0 20]);
            xticks([0 5 10 15 20]);
            if(nm==1)
                ylim([0 3])
            elseif(nm==2)
                ylim([0 3])
            else
                ylim([0 3])
            end
        end
        set(0,'CurrentFigure',fig1); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'DIVS_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig1);    
        set(0,'CurrentFigure',fig2); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'GRS_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig2);    
        set(0,'CurrentFigure',fig3); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'CCGFP_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig3);    
        set(0,'CurrentFigure',fig4); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'CCMKATE_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig4);    
    end
end

% 2ND FIGURE. We'll make it about alpha estimation.
sosthrvec2 = [7,10,13];
alpharange1 = logspace(log10(1e-10),log10(1e-1),1000);
alpharange2 = linspace(0,0.1,200);
[nstrains,nmedias] = size(tables.(areakey));
niter = 100000;
epsilon = 0.001;
nsmooth = 5; % we will smooth the trajectories to remove some of the noise.

xlims = [0.08,0.06,0.05]
for nthr = 1:length(sosthrvec2)
    highsosthr = sosthrvec2(nthr);
    for ns = 2%1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
            fig1 = figure(); % 'visible','off'
            ha11 = axes(); hold on;
            ha12 = axes('Units','Pixels','Position',[350,190,150,200]); hold on;
            for nr = 1:nrepeats % for each repeat
                color = repeat2color.(repeats{ns,nm}{nr});
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr)/60;
                gfptable = tables.(gfpkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                t1vec = [];
                t0vec = [];
                for lin = 1:size(gfptable,1)
                    gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
                    times = getvalids(timetable(lin,:));
                    if((length(gfpsp)>0)&&(gfpsp(1)<highsosthr))
                        % we care only lineages that begin with low SOS
                        if(sum(gfpsp>=highsosthr)>0) % some high SOS
                            t1vec = [t1vec,times(getfirst(find(gfpsp>=highsosthr)))-times(1)];
                        else % lineage never reached
                            t0vec = [t0vec,times(end)-times(1)];
                        end
                    end
                end                
                [lhs,alpha_maxl,maxlh] = getalphalhs2(t1vec,t0vec,interval,alpharange1);
                [alphawalk,naccepted] = alphaMC(t1vec, t0vec, interval, niter, epsilon);
                p5 = prctile(alphawalk,5);
                p95 = prctile(alphawalk,95);
                axes(ha11); hold on;
                rf = relhist(alphawalk,alpharange2);
                leftind = find(alpharange2>=p5);
                leftind = leftind(1);
                rightind = find(alpharange2>=p95);
                rightind = rightind(1);
                myupdownfill(alpharange2(leftind:rightind),zeros(size(leftind:rightind)),rf(leftind:rightind),color,0.2,0);%
                plot([alpha_maxl,alpha_maxl],[0,max(rf)],':','LineWidth',1,'Color',color);
                plot(alpharange2,rf,'-','LineWidth',1,'Color',color);
                %plot([alpha_maxl],[max(rf)],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                %ylim([0 1]);
                xlim([0 xlims(nthr)]);
                axes(ha12); hold on;
                w = 0.45;
                xvec = [nr-w,nr+w];
                myupdownfill([xvec(1),xvec(2)],[0,0],[length(t1vec),length(t1vec)],color,1,0);%
                plot([xvec(1),xvec(1),xvec(2),xvec(2)],[0,length(t1vec),length(t1vec),0],'LineWidth',0.5,'Color',[0.2,0.2,0.2]);
                w = 0.45;
                xvec = 4+[nr-w,nr+w];
                myupdownfill([xvec(1),xvec(2)],[0,0],[length(t0vec),length(t0vec)],color,0.2,0);%
                plot([xvec(1),xvec(1),xvec(2),xvec(2)],[0,length(t0vec),length(t0vec),0],'LineWidth',0.5,'Color',[0.2,0.2,0.2]);
                xticks([2,6]);
                xticklabels({'',''})
            end
            set(0,'CurrentFigure',fig1); % 
            mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                           'outname',[plotsfolder,'ALPHA_PDF_SOS-',num2str(highsosthr),'_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
            close(fig1);    
        end
    end
end

% Now one with all the values as a function of GFP levels

for ns = 2%1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        fig = figure(); % 'visible','off'
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat                    
            color = repeat2color.(repeats{ns,nm}{nr});
            alphas = tables.('alpha'){ns,nm}{nr};
            alpha_p5s = tables.('alpha_p5'){ns,nm}{nr};
            alpha_p95s = tables.('alpha_p95'){ns,nm}{nr};
            hold on;
            myupdownfill(sosthrvec,alpha_p5s,alpha_p95s,color,0.2,0);%
            plot(sosthrvec,alpha_p5s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,alpha_p95s,':','LineWidth',0.5,'Color',color);
            plot(sosthrvec,alphas,':','LineWidth',1,'Color',color);
            plot(sosthrvec,alphas,'o','LineWidth',1,...
                 'MarkerSize',5,...
                 'MarkerFaceColor',color,...
                 'Color',color);
            axis([min(sosthrvec) max(sosthrvec) 0 0.045]);
        end
        set(0,'CurrentFigure',fig); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'ALPHA_VS_SOS_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig);    
    end
end

% 3) We'll made it about SOS correlation with elongation

% 1) Show high SOS not dividing? percentage of cells that divide after 2 median division
% times? as function of max SOS? 
% 2) Max size v/s time colored by SOS

ns = 2;
nm = 1;
nr = 3;
interval = intervals{ns,nm}(nr);
lentable = tables.(lenkey){ns,nm}{nr};
gfptable = tables.(gfpkey){ns,nm}{nr};
mchtable = tables.(mchkey){ns,nm}{nr};
divtable = tables.(divkey){ns,nm}{nr};
timetable = tables.('times'){ns,nm}{nr};
% $$$ rows = []
% $$$ for lin = 1:size(lentable,1)
% $$$     if(sum(gfptable(lin,:)>20) && sum(divtable(lin,:)>3))
% $$$         rows = [rows,lin];
% $$$     end
% $$$ end
% $$$ %rows2 = [ 310 ];
% $$$ rows2 = [377];
% $$$ rows2 = [199];
% $$$ rows2 = [399];
% $$$ rows2 = [310];
% $$$ fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
% $$$                    'rows',rows2,'timetable',timetable);

lin = 233;
toff = 2;
fig1 = figure('visible','off');
ha11 = axes(); hold on;
fig2 = figure('visible','off');
ha21 = axes(); hold on;
lens = getvalids(lentable(lin,:));
gfps = getvalids(gfptable(lin,:));
times = getvalids(timetable(lin,:));
axes(ha11); hold on;
plot(times,lens,'.-','Color',[0,0,0.7]);
plot(times(1),lens(1),'o-',...
     'MarkerSize',8,...
     'MarkerFaceColor',[0.7,0.7,1],...
     'Color',[0,0,0.7]);
plot(times(end),lens(end),'o-',...
         'MarkerSize',8,...
         'MarkerFaceColor',[0,0,1],...
         'Color',[0,0,0.7]);
yticks([1,2,4,6,8,10]);
xticks([0,5,10,15,20,25]);
axis([-1 22 1 6]);
set(gca,'YScale','log');
axes(ha21); hold on;
plot(times,gfps,'.-','Color',[0,0.7,0]);
set(gca,'YScale','log');
yticks([1,2.5,5,10,25,50])
xticks([0,5,10,15,20,25]);;
axis([-1 22 0.3 50]);
divs = getvalids(divtable(lin,:));
[~,maxp] = max(gfps(1:(divs(1)+1)));
plot(times(maxp),gfps(maxp),'o-',...
     'MarkerSize',8,...
     'MarkerFaceColor',[0.1,0.5,0.1],...
     'Color',[0,0.7,0]);
for ndiv = 1:length(divs)
    divp = divs(ndiv)-toff;
    tx1 = interval*(divp+toff+0.5)/60;
    axes(ha11); hold on;
    plot([tx1,tx1],[1,10],'--','LineWidth',1,'Color',[0.3,0.3,0.3]);
    plot(times(divp+1),lens(divp+1),'o-',...
         'MarkerSize',8,...
         'MarkerFaceColor',[0,0,1],...
         'Color',[0,0,0.7]);
    plot(times(divp+2),lens(divp+2),'o-',...
         'MarkerSize',8,...
         'MarkerFaceColor',[0.7,0.7,1],...
         'Color',[0,0,0.7]);    
    axes(ha21); hold on;
    plot([tx1,tx1],[0.3,50],'--','LineWidth',1,'Color',[0.3,0.3,0.3]);
    if(ndiv==length(divs))
        [~,maxp] = max(gfps((divp+2):end));
    else
        [~,maxp] = max(gfps((divp+2):(divs(ndiv+1)+1)));
    end
    maxp = maxp+divp+1;
    plot(times(maxp),gfps(maxp),'o-',...
         'MarkerSize',8,...
         'MarkerFaceColor',[0.1,0.5,0.1],...
         'Color',[0,0.7,0]);
end
set(0,'CurrentFigure',fig1); % 
mypdfpngexpfig('PaperPosition',[0 0 12 7],...
               'outname',[plotsfolder,'EXAMPLE_TIME_MAXOBS_STRAIN-',num2str(ns),'_MEDIA',num2str(nm)]);
close(fig1);   
set(0,'CurrentFigure',fig2); % 
mypdfpngexpfig('PaperPosition',[0 0 12 7],...
               'outname',[plotsfolder,'EXAMPLE_TIME_MAXGFP_STRAIN-',num2str(ns),'_MEDIA',num2str(nm)]);
close(fig2);   


[nstrains, nmedias] = size(tables.(areakey));
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        fig1 = figure('visible','off');
        ha11 = axes(); hold on;
        fig2 = figure('visible','off');
        ha21 = axes(); hold on;
        fig3 = figure('visible','off');
        ha31 = axes(); hold on;
        nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
        xs1 = [];
        ys1 = [];
        xs2 = [];
        ys2 = [];
        xs3 = [];
        ys3 = [];
        for nr = 1:nrepeats % for each repeat
            divtable = tables.(divtimekey){ns,nm}{nr};
            gfptable2 = tables.('max_gfp'){ns,nm}{nr};
            obstimes = tables.('obstime'){ns,nm}{nr};
            lastlen = tables.('last_length'){ns,nm}{nr};
            firstlen = tables.('init_length'){ns,nm}{nr};
            lastmch = tables.('last_mcherry'){ns,nm}{nr};
            pos = (gfptable2(:)>0) & (not(obstimes(:)==0));
            gfps = gfptable2(pos);
            obst = obstimes(pos);
            blens = firstlen(pos);
            lens = lastlen(pos);
            mchs = lastmch(pos);
            % for 1st plots
            xs1 = [xs1;gfps];
            ys1 = [ys1;obst];
            % for 2nd plots
            pos = (gfptable2(:)>0) & (not(obstimes(:)==0)) & (not(isnan(firstlen(:))));
            gfps = gfptable2(pos);
            blens = firstlen(pos);
            lens = lastlen(pos);
            obst = obstimes(pos);
            xs2 = [xs2;gfps];
            ys2 = [ys2;log(lens./blens)./obst];
            % for 3rd plots
            pos = (gfptable2(:)>0) & (divtable(:)>0);
            gfps = gfptable2(pos);
            divs = divtable(pos);
            xs3 = [xs3;gfps];
            ys3 = [ys3;divs];
        end
        axes(ha11); hold on;
        dscatter(xs1,ys1,'logx',true,'logy',true);
        axis([0 50 0.1 20]);
        xticks([1,2.5,5,10,25,50]);
        yticks([0.1,0.5,1,2.5,5,10,20]);
        alpha(.75)
        set(0,'CurrentFigure',fig1); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'MAXSOS_MAXOBS_STRAIN-',num2str(ns),'_MEDIA',num2str(nm)]);
        close(fig1);   
        axes(ha31); hold on;
        dscatter(xs3,ys3,'logx',true,'logy',true);
        axis([0 50 0.1 20]);
        xticks([1,2.5,5,10,25,50]);
        yticks([0.1,0.5,1,2.5,5,10,20]);
        alpha(.75)
        set(0,'CurrentFigure',fig3); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'MAXSOS_DIVTIMES_STRAIN-',num2str(ns),'_MEDIA',num2str(nm)]);
        close(fig3);   
        axes(ha21); hold on;
        dscatter(xs2(ys2>0),ys2(ys2>0),'logx',true);
        alpha(.75)
        xticks([1,2.5,5,10,25,50]);
        if(nm==3)
            axis([0 50 0 2]);
            yticks([0,0.5,1,1.5,2.0]);
        elseif(nm==2)
            axis([0 50 0 1]);
            yticks([0,0.25,0.5,0.75,1]);
        else
            axis([0 50 0 0.5]);
            yticks([0,0.25,0.5]);
        end
        set(0,'CurrentFigure',fig2); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'MAXSOS_APP-ELONG-RATE_STRAIN-',num2str(ns),'_MEDIA',num2str(nm)]);
        close(fig2);   
    end
end