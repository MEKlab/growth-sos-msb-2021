
mmc0_aux();
PaperPosition = [0,0,17,10];
outlog = fopen('mmc8_summarytable.log','w');
outfolder = 'mmc8_summarytable_tables/'; % Where plots will be saved
load('mmc6_alpha_output.mat');
system(['rm ',outfolder,'*']); % remove all previous plots in that folder

strains = {'wt','2pal'}; % change to wt

[nstrains,nmedias] = size(tables.(areakey));

maxv = 2*3*3; % two strains, 3 media, 2 repeats
repeat_array = cell(maxv,1);
strain_array = cell(maxv,1);
media_array = cell(maxv,1);
interval_array = zeros(maxv,1);
duration_array = zeros(maxv,1);
nchannels_array = zeros(maxv,1);
ndivisions_array = zeros(maxv,1);
nedit1_array = zeros(maxv,1);
nedit2_array = zeros(maxv,1);
divtime_array = zeros(maxv,1);
divtime_std_array = zeros(maxv,1);
blength_array = zeros(maxv,1);
blength_std_array = zeros(maxv,1);
dlength_array = zeros(maxv,1);
dlength_std_array = zeros(maxv,1);
lengrate_array = zeros(maxv,1);
lengrate_std_array = zeros(maxv,1);
lengrate_r2_array = zeros(maxv,1);
lengrate_r2_std_array = zeros(maxv,1);
meangfp_array = zeros(maxv,1);
meangfp_std_array = zeros(maxv,1);
meanmch_array = zeros(maxv,1);
meanmch_std_array = zeros(maxv,1);
alpha_mat = zeros(maxv,length(sosthrvec));
alphap5_mat = zeros(maxv,length(sosthrvec));
alphap95_mat = zeros(maxv,length(sosthrvec));
alphaT0_mat = zeros(maxv,length(sosthrvec));
alphaT1_mat = zeros(maxv,length(sosthrvec));
vcount = 0;
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat            
            vcount=vcount+1;            
            % date
            repeat = repeats{ns,nm}{nr};
            repeat = split(repeat,'_');
            repeat = repeat{2};
            repeat_array{vcount} = repeat; 
            % strain
            strain_array{vcount} = strains{ns}; 
            % media
            media_array{vcount} = medias{nm}; 
            % time interval
            interval_array(vcount) = intervals{ns,nm}(nr)/60;
            interval_array(vcount) = round(interval_array(vcount),-1*getsig(interval_array(vcount))+1);
            % number of lineages
            lentable = tables.(lenkey){ns,nm}{nr};
            nchannels_array(vcount) = size(lentable,1);
            % total duration of tracking
            duration_array(vcount) = intervals{ns,nm}(nr)*size(lentable,2)/60;
            duration_array(vcount) = round(duration_array(vcount),-1*getsig(duration_array(vcount))+1);
            % number of lineages where last div was removed
            nedit1_array(vcount) = sum(tables.edited1{ns,nm}{nr});
            % number of lineages edited due to odd growth
            nedit2_array(vcount) = sum(tables.edited2{ns,nm}{nr});
            % number of divisions
            divtable = tables.(divtimekey){ns,nm}{nr};
            ndivisions_array(vcount) = sum(divtable(:)>0);
            % division time avg and std
            divtime_array(vcount) = mean(divtable(divtable>0));
            divtime_std_array(vcount) = std(divtable(divtable>0));
            divtime_array(vcount) = round(divtime_array(vcount),-1*getsig(divtime_std_array(vcount))+1);
            divtime_std_array(vcount) = round(divtime_std_array(vcount),-1*getsig(divtime_std_array(vcount))+1);
            % length at birth avg and std
            dlbirthtable = tables.('birthlens'){ns,nm}{nr};
            blength_array(vcount) = mean(dlbirthtable(divtable>0));
            blength_std_array(vcount) = std(dlbirthtable(divtable>0));
            blength_array(vcount) = round(blength_array(vcount),-1*getsig(blength_std_array(vcount))+1);
            blength_std_array(vcount) = round(blength_std_array(vcount),-1*getsig(blength_std_array(vcount))+1);
            % length at division avg and std
            dldivtable = tables.('divlens'){ns,nm}{nr};
            dlength_array(vcount) = mean(dldivtable(divtable>0));
            dlength_std_array(vcount) = std(dldivtable(divtable>0));
            dlength_array(vcount) = round(dlength_array(vcount),-1*getsig(dlength_std_array(vcount))+1);
            dlength_std_array(vcount) = round(dlength_std_array(vcount),-1*getsig(dlength_std_array(vcount))+1);
            % length growth rate avg and std
            gratetable = tables.('len_grates'){ns,nm}{nr};
            [lengrate_array(vcount),lengrate_std_array(vcount)] = safemean(gratetable(divtable>0));
            lengrate_array(vcount) = round(lengrate_array(vcount),-1*getsig(lengrate_std_array(vcount))+1);
            lengrate_std_array(vcount) = round(lengrate_std_array(vcount),-1*getsig(lengrate_std_array(vcount))+1);
            % length growth rate r2 avg and std
            r2table = tables.('len_grates_r2'){ns,nm}{nr};
            [lengrate_r2_array(vcount),lengrate_r2_std_array(vcount)] = safemean(r2table(divtable>0));
            lengrate_r2_array(vcount) = round(lengrate_r2_array(vcount),-1*getsig(lengrate_r2_std_array(vcount))+1);
            lengrate_r2_std_array(vcount) = round(lengrate_r2_std_array(vcount),-1*getsig(lengrate_r2_std_array(vcount))+1);
            % mean GFP avg and std
            gfptable = tables.('mean_gfp'){ns,nm}{nr};
            meangfp_array(vcount) = mean(gfptable(divtable>0));
            meangfp_std_array(vcount) = std(gfptable(divtable>0));
            meangfp_array(vcount) = round(meangfp_array(vcount),-1*getsig(meangfp_std_array(vcount))+1);
            meangfp_std_array(vcount) = round(meangfp_std_array(vcount),-1*getsig(meangfp_std_array(vcount))+1);
            % mean mkate avg and std
            meanmchtable = tables.('mean_mcherry'){ns,nm}{nr};
            meanmch_array(vcount) = mean(meanmchtable(divtable>0));
            meanmch_std_array(vcount) = std(meanmchtable(divtable>0));
            meanmch_array(vcount) = round(meanmch_array(vcount),-1*getsig(meanmch_std_array(vcount))+1);
            meanmch_std_array(vcount) = round(meanmch_std_array(vcount),-1*getsig(meanmch_std_array(vcount))+1);
            % alpha stats for different sos intervals
            for n = 1:length(sosthrvec)
                s = getsig(tables.('alpha_p5'){ns,nm}{nr}(n))-1; % using p5 to get the significance
                alpha_mat(vcount,n)  = round(tables.('alpha'){ns,nm}{nr}(n),-1*s);
                alphap5_mat(vcount,n) = round(tables.('alpha_p5'){ns,nm}{nr}(n),-1*s);
                alphap95_mat(vcount,n) = round(tables.('alpha_p95'){ns,nm}{nr}(n),-1*s);
                alphaT0_mat(vcount,n) = tables.('alpha_T0'){ns,nm}{nr}(n);
                alphaT1_mat(vcount,n) = tables.('alpha_T1'){ns,nm}{nr}(n);
            end
        end
    end
end

% date
date = repeat_array(1:vcount);
% strain
strain = strain_array(1:vcount);
% media
media = media_array(1:vcount);
% time interval
interval = interval_array(1:vcount);
% number of lineages
n_lineages = nchannels_array(1:vcount);
% total duration of tracking
tracking_time = duration_array(1:vcount);
% number of lineages where last div was removed
n_lineages_last_div_rmv = nedit1_array(1:vcount);
% number of lineages edited due to odd growth
n_lineages_truncated_odd_growth = nedit2_array(1:vcount);
% number of divisions
n_divisions = ndivisions_array(1:vcount);
% division time avg and std
division_time_avg = divtime_array(1:vcount);
division_time_std = divtime_std_array(1:vcount);
% length at birth avg and std
length_at_birth_avg = blength_array(1:vcount);
length_at_birth_std = blength_std_array(1:vcount);
% length at division avg and std
length_at_division_avg = dlength_array(1:vcount);
length_at_division_std = dlength_std_array(1:vcount);            
% length growth rate avg and std
length_grrate_avg = lengrate_array(1:vcount);
length_grrate_std = lengrate_std_array(1:vcount);
% length growth rate r2 avg and std
length_grrate_r2_avg = lengrate_r2_array(1:vcount);
length_grrate_r2_std = lengrate_r2_std_array(1:vcount);
% mean GFP avg and std
cellcycle_mean_gfp_avg = meangfp_array(1:vcount);
cellcycle_mean_gfp_std = meangfp_std_array(1:vcount);
% mean mkate avg and std
cellcycle_mean_mkate_avg = meanmch_array(1:vcount);
cellcycle_mean_mkate_std = meanmch_std_array(1:vcount);
            
% MAIN
mainT = table(strain,media,date,...
              interval,tracking_time,n_lineages,...
              n_lineages_last_div_rmv,n_lineages_truncated_odd_growth,...
              n_divisions,division_time_avg,division_time_std,...
              length_at_birth_avg,length_at_birth_std,length_at_division_avg,length_at_division_std,...
              length_grrate_avg,length_grrate_std,length_grrate_r2_avg,length_grrate_r2_std,...
              cellcycle_mean_gfp_avg,cellcycle_mean_gfp_std,cellcycle_mean_mkate_avg,cellcycle_mean_mkate_std);...

writetable(mainT,['./',outfolder,'MMC_SUMMARY_MAIN.xlsx'])


% ALPHA
% alpha stats for different sos intervals
for n = 1:length(sosthrvec)
    thrstr = num2str(sosthrvec(n));
    colvec = alpha_mat(1:vcount,n);
    eval(['alpha_maxlh_SOS_',thrstr,'=colvec;']);
    colvec = alphap5_mat(1:vcount,n);
    eval(['alpha_p5_SOS_',thrstr,'=colvec;']);
    colvec = alphap95_mat(1:vcount,n);
    eval(['alpha_p95_SOS_',thrstr,'=colvec;']);
    colvec = alphaT0_mat(1:vcount,n);
    eval(['nbelow_SOS_',thrstr,'=colvec;']);
    colvec = alphaT1_mat(1:vcount,n);
    eval(['nabove_SOS_',thrstr,'=colvec;']);
end
alphaT = table(strain,media,date,...
               interval,tracking_time,n_lineages,...
               nbelow_SOS_6,nabove_SOS_6,alpha_maxlh_SOS_6,alpha_p5_SOS_6,alpha_p95_SOS_6,...
               nbelow_SOS_7,nabove_SOS_7,alpha_maxlh_SOS_7,alpha_p5_SOS_7,alpha_p95_SOS_7,...
               nbelow_SOS_8,nabove_SOS_8,alpha_maxlh_SOS_8,alpha_p5_SOS_8,alpha_p95_SOS_8,...
               nbelow_SOS_9,nabove_SOS_9,alpha_maxlh_SOS_9,alpha_p5_SOS_9,alpha_p95_SOS_9,...
               nbelow_SOS_10,nabove_SOS_10,alpha_maxlh_SOS_10,alpha_p5_SOS_10,alpha_p95_SOS_10,...
               nbelow_SOS_11,nabove_SOS_11,alpha_maxlh_SOS_11,alpha_p5_SOS_11,alpha_p95_SOS_11,...
               nbelow_SOS_12,nabove_SOS_12,alpha_maxlh_SOS_12,alpha_p5_SOS_12,alpha_p95_SOS_12,...
               nbelow_SOS_13,nabove_SOS_13,alpha_maxlh_SOS_13,alpha_p5_SOS_13,alpha_p95_SOS_13,...
               nbelow_SOS_14,nabove_SOS_14,alpha_maxlh_SOS_14,alpha_p5_SOS_14,alpha_p95_SOS_14,...
               nbelow_SOS_15,nabove_SOS_15,alpha_maxlh_SOS_15,alpha_p5_SOS_15,alpha_p95_SOS_15,...
               nbelow_SOS_16,nabove_SOS_16,alpha_maxlh_SOS_16,alpha_p5_SOS_16,alpha_p95_SOS_16);
writetable(alphaT,['./',outfolder,'MMC_SUMMARY_ALPHA.xlsx'])