% This scripts continues from the the output of mmc_cellcycle.m
% and tries to identify cells with low-growth rates at the end or the
% begining of the trajectories, and removes those points and any division
% event associated to them.

% NOTE: minimal growth-rate threshold in mmc0_aux
mmc0_aux();
PaperPosition = [0,0,20,10];
outlog = fopen('mmc4_secondfilter.log','w');
plotsfolder = 'mmc4_secondfilter_plots/'; % Where plots will be saved
load('mmc3_cellcycle_output.mat');
% We will add a table that tracks the time associated to each point, as we are
% going to be removing points, and later would be impossible to infer the
% timejust by extracting the nonnull values.
[tables] = constructtimetable(tables, intervals, repeats);
% We will be using the initial growth rates
%gratekey = 'mid_len_grates';
%r2key = 'mid_len_grates_r2';
%
makeplots = 1;
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

% ====================== IDENTIFYING AND TRUNCATING NON-GROWING CELLS 
if(makeplots)
    %% FIRST: make a plot for identified cases before and after truncation.
    fprintf(outlog,['Plotting trajectories before/after removing non-growing ending points ' ...
                    'as Filtering_nongrowing...\n']);    
    [nstrains,nmedias] = size(tables.(lenkey));
    PaperPosition = [0,0,20,10];
    for ns = 1:nstrains % for each strain folder
        fprintf(outlog,['... strain ', num2str(ns),'\n']);
        for nm = 1:nmedias % for each media folder
            fprintf(outlog,['....... media ', num2str(nm),'\n']);
            nrepeats = length(tables.(lenkey){ns,nm}); % number of repeats
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                gratetable = tables.(gratekey){ns,nm}{nr};
                %r2table = tables.(r2key){ns,nm}{nr};
                toplot = zeros(size(lentable,1),1);
                for lin = 1:size(lentable,1)
                    grates = gratetable(lin,:);
                    divsp = getvalids(divtable(lin,:));
                    lensp = getvalids(lentable(lin,:));
                    gratesp = grates(1:(length(divsp)+1));
                    neggrs = (gratesp<=mingrate)|isnan(gratesp); % or NaN
                    if(length(neggrs)==1) % no division
                        if((neggrs(1)>0))
                        %if((neggrs(1)>0) && not(max(lensp)/min(lensp)>3))
                            % REMOVE IT ALL!
                            toplot(lin) = 1;
                            lentable(lin,:) = NaN;
                            mchtable(lin,:) = NaN;
                            gfptable(lin,:) = NaN;
                            timetable(lin,:) = NaN;
                            divtable(lin,:) = NaN;
                        end
                    elseif(sum(neggrs)==length(gratesp))
                        % REMOVE IT ALL!
                        toplot(lin) = 1;
                        lentable(lin,:) = NaN;
                        mchtable(lin,:) = NaN;
                        gfptable(lin,:) = NaN;
                        timetable(lin,:) = NaN;
                        divtable(lin,:) = NaN;
                    else
                        if(neggrs(1)>0) % first growth-rate is below threshold
                            toplot(lin) = 1;
                            ngrmv = 1;
                            ndrmv = 1;
                            while((gratesp(ngrmv+1)<=mingrate)||isnan(gratesp(ngrmv+1)))
                                ngrmv = ngrmv+1;
                                ndrmv = ndrmv+1;
                            end
                            divframe = divsp(ndrmv);
                            lentable(lin,1:divframe+1) = NaN;
                            mchtable(lin,1:divframe+1) = NaN;
                            gfptable(lin,1:divframe+1) = NaN;
                            timetable(lin,1:divframe+1) = NaN;
                            divtable(lin,1:ndrmv) = NaN;
                            gratetable(lin,1:ndrmv) = NaN;
                        end
                        if((neggrs(end)>0)) % last growth-rate is below threshold
                            toplot(lin) = 1;
                            ngrmv = length(gratesp);
                            ndrmv = length(divsp);
                            if(ndrmv==0)
                                fprintf(outlog,'Ooops!\n');
                            end
                            while((gratesp(ngrmv-1)<=mingrate)||isnan(gratesp(ngrmv-1)))
                                ngrmv = ngrmv-1;
                                ndrmv = ndrmv-1;
                            end
                            divframe = divsp(ndrmv);
                            lentable(lin,divframe+2:end) = NaN;
                            mchtable(lin,divframe+2:end) = NaN;
                            gfptable(lin,divframe+2:end) = NaN;
                            timetable(lin,divframe+2:end) = NaN;
                            divtable(lin,ndrmv:end) = NaN;
                            gratetable(lin,ngrmv:end) = NaN;
                        end
                    end
                end
                rows = find(toplot>0);
                if(length(rows)>0)
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'gratetable',gratetable,'timetable',timetable,'visible',0);
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = PaperPosition;
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_Filtering_nongrowing_',strains{ns},'_',repeat,'_AFTER.png'],'-dpng');
                    close(fig);
                    lentable = tables.(lenkey){ns,nm}{nr};
                    gfptable = tables.(gfpkey){ns,nm}{nr};
                    mchtable = tables.(mchkey){ns,nm}{nr};
                    divtable = tables.(divkey){ns,nm}{nr};
                    gratetable = tables.(gratekey){ns,nm}{nr};
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'gratetable',gratetable,'visible',0);
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = PaperPosition;
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_Filtering_nongrowing_',strains{ns},'_',repeat,'_BEFORE.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end


%% SECOND: save the actual truncations
fprintf(outlog,['Saving filtered trajectories\n']);
timekeys = {'areamats','lengthmats','gfpmat_scaled','mchmats','mchmat_scaled','times'};
divkeys = {'divpositions','divtimes','birthareas','divareas','birthlens','divlens'};
grkeys = {'area_grates','area_grates_r2','area_grates_err',...
          'len_grates','len_grates_r2','len_grates_err',...
          'mid_area_grates','mid_area_grates_r2','mid_area_grates_err',...
          'mid_len_grates','mid_len_grates_r2','mid_len_grates_err'};
tables.('edited2') = cell(length(strains),length(medias));
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
        for nr = 1:nrepeats
            repeat = repeats{ns,nm}{nr};
            interval = intervals{ns,nm}(nr);
            lentable = tables.(lenkey){ns,nm}{nr};
            divtable = tables.(divkey){ns,nm}{nr};
            gratetable = tables.(gratekey){ns,nm}{nr};
            timetable = tables.('times'){ns,nm}{nr};
            edited = zeros(size(gratetable,1),1);
            for lin = 1:size(gratetable,1)
                divsp = getvalids(divtable(lin,:));
                gratesp = gratetable(lin,1:(length(divsp)+1));
                lensp = getvalids(lentable(lin,:));
                neggrs = gratesp<=mingrate;
                neggrs = (gratesp<=mingrate)|isnan(gratesp); % or NaN
                if(length(neggrs)==1) % no division
                    if((neggrs(1)>0))
                        % REMOVE IT ALL!
                        edited(lin) = 1;
                        % Update tables with time as columns
                        for key = timekeys
                            tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                        end
                        % Update tables with division as columns
                        for key = divkeys
                            tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                        end
                        % Update tables with cellcycle as columns
                        for key = grkeys
                            tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                        end
                    end
                elseif(sum(neggrs)==length(gratesp))
                    % REMOVE IT ALL!
                    edited(lin) = 1;
                    % Update tables with time as columns
                    for key = timekeys
                        tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                    end
                    % Update tables with division as columns
                    for key = divkeys
                        tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                    end
                    % Update tables with cellcycle as columns
                    for key = grkeys
                        tables.(key{1}){ns,nm}{nr}(lin,:) = NaN;
                    end
                else
                    if(neggrs(1)>0) % first growth-rate is below threshold
                        edited(lin) = 1;
                        ngrmv = 1;
                        ndrmv = 1;
                        while((gratesp(ngrmv+1)<=mingrate)||isnan(gratesp(ngrmv+1)))
                            ngrmv = ngrmv+1;
                            ndrmv = ndrmv+1;
                        end
                        divframe = divsp(ndrmv);
                        % Update tables with time as columns
                        for key = timekeys
                            tables.(key{1}){ns,nm}{nr}(lin,1:divframe+1) = NaN;
                        end
                        % Update tables with division as columns
                        for key = divkeys
                            tables.(key{1}){ns,nm}{nr}(lin,1:ndrmv) = NaN;
                        end
                        % Update tables with cellcycle as columns
                        for key = grkeys
                            tables.(key{1}){ns,nm}{nr}(lin,1:ndrmv) = NaN;
                        end
                    end
                    if(neggrs(end)>0) % last growth-rate is below threshold
                        edited(lin) = 1;
                        ngrmv = length(gratesp);
                        ndrmv = length(divsp);
                        if(ndrmv==0)
                            fprintf(outlog,'Ooops!\n');
                        end
                        while((gratesp(ngrmv-1)<=mingrate)||isnan(gratesp(ngrmv-1)))
                            ngrmv = ngrmv-1;
                            ndrmv = ndrmv-1;
                        end
                        divframe = divsp(ndrmv);
                        % Update tables with time as columns
                        for key = timekeys
                            tables.(key{1}){ns,nm}{nr}(lin,divframe+2:end) = NaN;
                        end
                        % Update tables with division as columns
                        for key = divkeys
                            tables.(key{1}){ns,nm}{nr}(lin,ndrmv:end) = NaN;
                        end
                        % Update tables with cellcycle as columns
                        for key = grkeys
                            tables.(key{1}){ns,nm}{nr}(lin,ngrmv:end) = NaN;
                        end
                    end
                end                
            end
            tables.('edited2'){ns,nm}{nr} = edited;
            rows = find(edited>0);
            if(length(rows)>0)
                fprintf(outlog,[' Strain ',num2str(ns),' Repeat ',repeats{ns,nm}{nr},',',...
                                ' lineages edited: ',num2str(rows'),'\n']);
            end
        end
    end
end

fclose(outlog);
save('mmc4_secondfilter_output','tables','intervals','repeats','datafolder','strains','medias','rmvsets','repeat2color');

