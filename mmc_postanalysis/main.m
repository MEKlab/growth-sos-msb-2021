
display('Preprocessing data');
mmc1_preprocess();
clear all;

display(['Truncating based on mCherry signal stability and removing some last ' ...
         'divisions with few points']);
mmc2_firstfilter();
clear all;

display('Computing cell-cycle ralated variables');
mmc3_cellcycle();
clear all;

display('Truncating linages based on their elongation growth-rates');
mmc4_secondfilter();
clear all;

display('Plotting trajectories of interest');
mmc5_plottingtrajectories();
clear all;

display('Estimating alpha');
mmc6_alpha();
clear all;

display('Making summary plots');
mmc7_summaryplots();
clear all;

display('Making summary tables');
mmc8_summarytable();
clear all;

display('Exporting data');
mmc9_export();
clear all;