
mmc0_aux();
PaperPosition = [0,0,17,10];
outlog = fopen('mmc9_export.log','w');
outfolder = 'mmc9_export_tables/'; % Where plots will be saved
load('mmc6_alpha_output.mat');
system(['rm ',outfolder,'*']); % remove all previous plots in that folder

[nstrains,nmedias] = size(tables.(areakey));

maxv = 1e06;
repeat_array = cell(maxv,1);
strain_array = zeros(maxv,1);
channel_array = zeros(maxv,1);
cycle_array = zeros(maxv,1);
media_array = zeros(maxv,1);
grate_array = zeros(maxv,1);
r2_array = zeros(maxv,1);
blength_array = zeros(maxv,1);
divtime_array = zeros(maxv,1);
maxgfp_array = zeros(maxv,1);
meanmkate_array = zeros(maxv,1);
dlength_array = zeros(maxv,1);
vcount = 0;
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat
            repeat = repeats{ns,nm}{nr};
            repeat = split(repeat,'_');
            repeat = repeat{2};
            gratetable = tables.(gratekey){ns,nm}{nr};
            divtable = tables.(divtimekey){ns,nm}{nr};
            r2table = tables.(r2key){ns,nm}{nr};
            gfptable = tables.('max_gfp'){ns,nm}{nr};
            meanmchtable = tables.('mean_mcherry'){ns,nm}{nr};
            dlbirthtable = tables.('birthlens'){ns,nm}{nr};
            dldivtable = tables.('divlens'){ns,nm}{nr};
            for lin = 1:size(gratetable)
                pos = find(divtable(lin,:)>0);
                for n = 1:length(pos)
                    p = pos(n);
                    vcount=vcount+1;
                    repeat_array{vcount} = repeat;
                    strain_array(vcount) = ns;
                    media_array(vcount) = nm;               
                    channel_array(vcount) = lin;
                    cycle_array(vcount) = n;
                    divtime_array(vcount) = divtable(lin,p);
                    grate_array(vcount) = gratetable(lin,p);
                    r2_array(vcount) = r2table(lin,p);
                    blength_array(vcount) = dlbirthtable(lin,p);
                    dlength_array(vcount) = dldivtable(lin,p);
                    maxgfp_array(vcount) = gfptable(lin,p);
                    meanmkate_array(vcount) = meanmchtable(lin,p);
                end
            end
        end
    end
end

repeat = repeat_array(1:vcount);
strain = strain_array(1:vcount);
media = media_array(1:vcount);
channel = channel_array(1:vcount);
cell_cycle = cycle_array(1:vcount);
division_time = divtime_array(1:vcount);
growth_rate = grate_array(1:vcount);
growth_rate_r2 = r2_array(1:vcount);
birth_length = blength_array(1:vcount);
division_length = dlength_array(1:vcount);
max_gfp = maxgfp_array(1:vcount);
mean_mkate = meanmkate_array(1:vcount);

T = table(strain,media,repeat,channel,cell_cycle,division_time,growth_rate,growth_rate_r2, ...
          birth_length,division_length,max_gfp,mean_mkate);

writetable(T,['./',outfolder,'MMC_Cell-Cycle-data.xlsx'])