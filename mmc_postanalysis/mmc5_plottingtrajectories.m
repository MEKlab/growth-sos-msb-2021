
mmc0_aux();
PaperPosition = [0,0,20,10];
outlog = fopen('mmc5_plottingtrajectories.log','w');
plotsfolder = 'mmc5_plottingtrajectories_plots/'; % Where plots will be saved
load('mmc4_secondfilter_output.mat');
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

% low growth rate threshold is defined in mmc0_aux
lowdivtimethr = 0.4;
highdivtimethr = 8;
highsosthr = 7;
makeplots = 1;
[nstrains,nmedias] = size(tables.(lenkey));

% ====================== PLOT REMAINDER TRAJECTORIES WITH GROWTH BELOW THRESHOLD
% We will plot every trajectory that remains with some growth rate below the
% threshold. That is because that cell cycle was in the middle of the
% trajectory and not on the edges, or because it had no division event.
if(makeplots)
    fprintf(outlog,['Plotting trajectories that remain with growth below threshold ' ...
                    'as REMAINDER_Nongrowing...','\n']);
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                gratetable = tables.(gratekey){ns,nm}{nr};
                toplot = zeros(size(gratekey,1),1);
                for lin = 1:size(lentable,1)
                    divs = divtable(lin,:);
                    grates = gratetable(lin,:);
                    divsp = getvalids(divs);
                    % get grrates using the length of divsp, in case some are nan?
                    gratesp = grates(1:(length(divsp)+1)); 
                    neggrs = gratesp<=mingrate;
                    if(sum(neggrs)>0) % 
                        toplot(lin) = 1;
                    end
                end        
                rows = find(toplot>0);
                if(length(rows)>0)
                    fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                    ' reminder non-growth lineages: ',num2str(rows)],'\n');
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'gratetable',gratetable,'timetable',timetable,'visible',0);
                    PaperPosition = [0,0,20,10];
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = PaperPosition;
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_REMAINDER_Nongrowing_',strains{ns},'_',repeat,'.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end

% ====================== PLOT TRAJECTORIES WITH VERY SHORT DIVISIONS
% We will plot the trajectories of all case where some "very short" division
% event was observed, just to confirm we are not carrying errors from the
% segmentation.
if(makeplots)
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(lenkey){ns,nm}); % number of repeatsb
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                divtable2 = tables.(divtimekey){ns,nm}{nr};
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                gratetable = tables.(gratekey){ns,nm}{nr};
                toplot = zeros(size(lentable,1),1);
                for lin = 1:size(lentable,1)
                    if(sum(divtable2(lin,:)<=lowdivtimethr)>0)
                        toplot(lin) = 1;
                    end
                end
                rows = find(toplot>0);
                if(length(rows)>0)
                    fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                    ' lineages with very short divisions: ',num2str(rows'),'\n']);
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'timetable',timetable,'visible',0);
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = [0 0 20 10];
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_SHORT_DIV_Example',strains{ns},'_',repeat,'.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end

% ====================== PLOT TRAJECTORIES WITH VERY LONG DIVISIONS
% We will plot the trajectories of all case where some "very long" division
% event was observed, just to confirm we are not carrying errors from the
% segmentation.
if(makeplots)
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(lenkey){ns,nm}); % number of repeatsb
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                divtable2 = tables.(divtimekey){ns,nm}{nr};
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                gratetable = tables.(gratekey){ns,nm}{nr};
                toplot = zeros(size(lentable,1),1);
                for lin = 1:size(lentable,1)
                    if(sum(divtable2(lin,:)>=highdivtimethr)>0)
                        toplot(lin) = 1;
                    end
                end
                rows = find(toplot>0);
                if(length(rows)>0)
                    fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                    ' lineages with very long divisions: ',num2str(rows'),'\n']);
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'timetable',timetable,'visible',0);
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = [0 0 20 10];
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_LONG_DIV_Example',strains{ns},'_',repeat,'.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end

% ====================== PLOT HIGH SOS TRAJECTORIES THAT SHOW DIVISIONS
% We will plot the trajectories of all case where some "very high" SOS signal
% and a subsequent division event was observed, just to confirm we are not
% carrying errors from the segmentation.
if(makeplots)
    fprintf(outlog,['Plotting trajectories have high SOS and divide','\n']);
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                gratetable = tables.(gratekey){ns,nm}{nr};
                toplot = zeros(size(gratekey,1),1);
                for lin = 1:size(lentable,1)
                    divsp = getvalids(divtable(lin,:));
                    gfpsp = getvalids(gfptable(lin,:));
                    if((length(divsp)>0) && sum(gfpsp>=highsosthr)) % some high SOS event and some division
                        pos = getfirst(find(gfpsp>=highsosthr));
                        if(sum(divsp>=pos)) % divides after high SOS
                            toplot(lin) = 1;
                        end
                    end
                end        
                rows = find(toplot>0);
                if(length(rows)>0)
                    fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                    ' high-sos dividing lineages: ',num2str(rows),'\n']);
% $$$                     fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
% $$$                                        'rows',rows,'gratetable',gratetable,'timetable',timetable);
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'timetable',timetable,'visible',0);
                    PaperPosition = [0,0,20,10];
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = PaperPosition;
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_HIGHSOS_Dividing_',strains{ns},'_',repeat,'.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end

if(makeplots)
    fprintf(outlog,['Plotting trajectories have high SOS and apparently return to low SOS\n']);
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                [tohigh,tolow,nottohigh,nottolow] = getGFPintertimes(gfptable,timetable,highsosthr);
                rows = find(tolow(:,1)>0);
                if(length(rows)>0)
                    fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                    ' high-sos returning to low lineages: ',num2str(rows'),'\n']);
                    fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                       'rows',rows,'timetable',timetable,'visible',0);
                    fig.PaperUnits = 'centimeters';
                    fig.PaperPosition = PaperPosition;
                    print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_HIGHSOS_ReturningToLow_',strains{ns},'_',repeat,'.png'],'-dpng');
                    close(fig);
                end
            end
        end
    end
end

if(makeplots)
    fprintf(outlog,['Plotting trajectories have high SOS and apparently have a second ' ...
                    'induction event\n']);
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
            for nr = 1:nrepeats
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                divtable = tables.(divkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                [tohigh,tolow,nottohigh,nottolow] = getGFPintertimes(gfptable,timetable,highsosthr);
                if(size(tohigh,2)>1)
                    rows = find(tohigh(:,2)>0);
                    if(length(rows)>0)
                        fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},',',...
                                        ' high-sos twice lineages: ',num2str(rows'),'\n']);
                        fig = plotlineages(lentable, divtable, gfptable, mchtable, interval,...
                                           'rows',rows,'timetable',timetable,'visible',0);
                        fig.PaperUnits = 'centimeters';
                        fig.PaperPosition = PaperPosition;
                        print('-r600',fig,['./',plotsfolder,'TRAJECTORIES_HIGHSOS_Twice',strains{ns},'_',repeat,'.png'],'-dpng');
                        close(fig);
                    end
                end
            end
        end
    end
end

fclose(outlog);