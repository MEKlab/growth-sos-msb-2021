function [tables] = constructtimetable(tables, intervals, repeats)

    lenkey = 'lengthmats';
    [nstrains,nmedias] = size(tables.(lenkey));
    tables.('times') = cell(nstrains,nmedias);
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(lenkey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                interval = intervals{ns,nm}(nr);
                lentable = tables.(lenkey){ns,nm}{nr};
                timetable = NaN*zeros(size(lentable));
                for lin = 1:size(lentable,1)
                    lensp = getvalids(lentable(lin,:));
                    time = [0:(length(lensp)-1)]*interval/60; 
                    timetable(lin,1:length(lensp)) = time;
                end
                tables.('times'){ns,nm}{nr} = timetable;
            end
        end
    end 
end