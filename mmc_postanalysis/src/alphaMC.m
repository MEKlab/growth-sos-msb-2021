function [alphawalk,naccepted] = alphaMC(t1vec, t0vec, interval, niter, epsilon)

[~, currentalpha, currentlh] = getalphalhs2(t1vec, t0vec, interval, 0.1);

naccepted = 0;

alphawalk = zeros(1,niter);

for i = 1:niter
    
    candidate = currentalpha + unifrnd(-epsilon, epsilon, 1);
    
    while(candidate<=0) % resample in case is bigger than 0
        candidate = currentalpha + unifrnd(-epsilon,epsilon,1);
    end
    
    [candidatelh,~,~] = getalphalhs2(t1vec,t0vec,interval,candidate);
    
    prob_ratio = min([1,exp(candidatelh - currentlh)]);
    if rand(1) < prob_ratio
        currentalpha = candidate;
        currentlh = candidatelh;
        naccepted = naccepted+1;
    end
    
% $$$     prob_delta = candidatelh - currentlh;
% $$$     if log(rand(1)) < prob_delta
% $$$         currentalpha = candidate;
% $$$         currentlh = candidatelh;
% $$$         naccepted = naccepted+1;
% $$$     end
    
    alphawalk(i) = currentalpha;
    
end

end