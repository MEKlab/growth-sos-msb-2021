function [bestlam, lambdavec, costs] = estimatepopgrowth(interval,divs,lmin,lmax)

    divsp = divs/interval;
    maxn = max(divsp);
    minn = min(divsp);

    xvals = minn:maxn;
    freqs = zeros(size(xvals));
    for n = 1:length(xvals)
        x = xvals(n);
        freqs(n) = sum(divsp==x);
    end
    %freqs = smooth(freqs,10)';
    freqs = freqs./sum(freqs);
    
    lambdavec = linspace(lmin,lmax,5000);
    costs = zeros(size(lambdavec));
    for nc = 1:length(costs)
        costs(nc) = getcost(lambdavec(nc));
    end
    
    [~,indx] = min(costs);
    bestlam = lambdavec(indx);
    
    function [cost] = getcost(lambda)
        intlam = lambda*interval;
        ds = (1/intlam)*(exp(intlam)-1)*sum(exp(-intlam.*xvals).*freqs);
        cost = (1 - 2.*sum(ds))^2;
    %lt = lambda.*interval;
    %integ = (1/(lambda^2))*sum((lt.*xvals+1).*exp(-lt.*xvals) - lt.*xvals.*exp(-lt.*(xvals-1)));
    %cost = (1 - 2*integ)^2;
    end

end