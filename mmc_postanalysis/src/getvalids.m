function [valuesp] = getvalids(values)

    valuesp = values(not(isnan(values)));

end