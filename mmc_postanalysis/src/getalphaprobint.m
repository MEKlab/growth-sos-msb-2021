function [probint,lim0] = getalphaprobint(tohightimes,nonhightimes,interval,alpha)

    c = sum(tohightimes)+sum(nonhightimes);
    b = length(tohightimes)*interval;
    t = interval;
    probint = -(1/c).* (exp(-alpha.*c).*...
                       (1-exp(alpha.*t)).^(-b).*...
                       (exp(alpha.*t)-1).^(b).* ...
                       hypergeom([-b,-c/t],1-c/t,exp(alpha.*t)));
    
    lim0 = 0;
    
end