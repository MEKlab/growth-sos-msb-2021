function [llh,alpha_maxlh,maxlh] = getalphalhs2(tohightimes,nonhightimes,interval,alpha)

    c = sum(tohightimes)+sum(nonhightimes);
    llh = length(tohightimes).*(log(exp(alpha.*interval)-1)) - alpha.*c;
    
    b = length(tohightimes)*interval;
    alpha_maxlh = (1/interval)*log(c/(c-b));

    maxlh = length(tohightimes).*(log(exp(alpha_maxlh.*interval)-1)) - alpha_maxlh.*c;
end