function [fig] = plotlineages(lentable, divtable, gfptable, mchtable, interval, ...
                              STR1, STR1val, STR2, STR2val, STR3, STR3val, STR4, STR4val)

    OPTIONAL_STRS = {'rows','gratetable','timetable','visible'};
    
    rows = 1:size(lentable,1);
    plotgrrate = 0;
    infertime = 1;
    visible = 1;
    
    if nargin < 6
    elseif nargin == 7
        Input_Values = {STR1, STR1val};
    elseif nargin == 9
        Input_Values = {STR1, STR1val; STR2, STR2val};
    elseif nargin == 11
        Input_Values = {STR1, STR1val; STR2, STR2val; STR3, STR3val};
    elseif nargin == 13
        Input_Values = {STR1, STR1val; STR2, STR2val; STR3, STR3val; STR4, STR4val};
    end

    for i=1:1:((nargin-5)/2)
        STR = Input_Values{i,1};
        STRval = Input_Values{i,2};
        z = strmatch(STR,OPTIONAL_STRS,'exact');
        if z == 1 % 'rows'
            rows = STRval;
        elseif z == 2 % 'gratetable'
            gratetable = STRval;
            plotgrrate = 1;
        elseif z == 3 % 'timetable'
            timetable = STRval;
            infertime = 0;
        elseif z == 4 % 'visible'
            visible = STRval;
        else
            error ('WTH!');
        end
    end
    div2time = @(x) (x).*interval/60;
    lincolors = lines(length(rows));
    if(visible)
        fig = figure();
    else
        fig = figure('visible','off');
    end
    
    for nlin = 1:length(rows)
        lin = rows(nlin);
        divs = divtable(lin,:);
        lensp = getvalids(lentable(lin,:));
        gfpsp = getvalids(gfptable(lin,:));
        mchsp = getvalids(mchtable(lin,:));
        % check dimensions
        if(not((length(lensp)==length(gfpsp)) && (length(lensp)==length(mchsp))))
            length(lensp)
            length(gfpsp)
            length(mchsp)
            display([' While plotting, inconsistent number of time-points in ', ...
                     'lineage ',num2str(lin)]);
            continue;
        end
        divsp = getvalids(divs);
        % actual time in hours
        if(infertime)
            time = [0:(length(lensp)-1)]*interval/60; 
        else
            time = getvalids(timetable(lin,:));
        end
        if(length(time)==0)
            continue;
        end
        subplot(2,2,1);  hold on;
        plot(time,lensp,'.-','Color',lincolors(nlin,:));
        set(gca,'YScale','log');
        figlabels('ylabel','Length [$\mu$m]',...
                  'xlabel','time [h]',...
                  'title',['Cell length']);
        xlim([0 size(lentable,2)*interval/60]);
        subplot(2,2,2);  hold on;
        
        if(plotgrrate)
            grates = gratetable(lin,:);
            prev = min(time);
            xvec = [];
            yvec = [];
            if(length(divsp)>0)
                for ndiv = 1:size(divtable,2)
                    if(~isnan(divtable(lin,ndiv)) && ~isnan(grates(ndiv)))
                        xvec = [xvec,prev,div2time(divtable(lin,ndiv))];
                        yvec = [yvec,grates(ndiv),grates(ndiv)];
                        prev = div2time(divtable(lin,ndiv)+1);
                    end
                end
                % add the last growth-rate, as there is potentially no
                % division after.
                temp1 = getvalids(divtable(lin,:));
                temp2 = getvalids(gratetable(lin,:));
                xvec = [xvec,div2time(temp1(end)),max(time)];
                yvec = [yvec,temp2(end),temp2(end)];
            else
                xvec = [xvec,max(time)];
                yvec = [yvec,grates(1)];
            end
            plot(xvec,yvec,'o-',...
                 'MarkerFaceColor',lincolors(nlin,:),...
                 'Color',lincolors(nlin,:));
            figlabels('ylabel','Length growth-rate [1/h]',...
                      'xlabel','Time [h]',...
                      'title',['Growth rate']);
            xlim([0 size(lentable,2)*interval/60]);
        else
            plot(time,gfpsp,'.-','Color',lincolors(nlin,:));
            set(gca,'YScale','log');
            figlabels('ylabel','GFP [A.U.]',...
                      'xlabel','time [h]',...
                      'title',['GFP']);
            xlim([0 size(lentable,2)*interval/60]);
        end
        subplot(2,2,3);  hold on;
        plot((divsp)*interval/60,max(gfpsp)*ones(size(divsp)),'o',...
             'LineWidth',2,...
             'MarkerFaceColor',lincolors(nlin,:),...
             'Color',lincolors(nlin,:));
        figlabels('ylabel','Max GFP [A.U.]',...
                  'xlabel','Division events [h]',...
                  'title',['Divisions']);
        xlim([0 size(lentable,2)*interval/60]);
        subplot(2,2,4);  hold on;
        plot(time,mchsp,'.-','Color',lincolors(nlin,:));
        set(gca,'YScale','log');
        figlabels('ylabel','mKate2 [A.U.]',...
                  'xlabel','time [h]',...
                  'title',['mKate2']);
        xlim([0 size(lentable,2)*interval/60]);
    end
end