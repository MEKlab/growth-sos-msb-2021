function [tables] = computecellcycle(tables, intervals, repeats)
    
    minpts = 3; % minimum number of points to estimate growth rates
    divkey = 'divpositions';
    areakey = 'areamats';
    lenkey = 'lengthmats';
    gfpkey = 'gfpmat_scaled';
    mchkey = 'mchmats';
    [nstrains,nmedias] = size(tables.(areakey));
    % NEW TABLES TO BE COMPUTED
    tables.('divtimes') = cell(nstrains,nmedias);
    tables.('birthareas') = cell(nstrains,nmedias);
    tables.('divareas') = cell(nstrains,nmedias);
    tables.('birthlens') = cell(nstrains,nmedias);
    tables.('divlens') = cell(nstrains,nmedias);
    tables.('area_grates') = cell(nstrains,nmedias);
    tables.('area_grates_r2') = cell(nstrains,nmedias);
    tables.('area_grates_err') = cell(nstrains,nmedias);
    tables.('len_grates') = cell(nstrains,nmedias);
    tables.('len_grates_r2') = cell(nstrains,nmedias);
    tables.('len_grates_err') = cell(nstrains,nmedias);
    tables.('mean_gfp') = cell(nstrains,nmedias);
    tables.('max_gfp') = cell(nstrains,nmedias);
    tables.('mean_mcherry') = cell(nstrains,nmedias);
    display(['Estimating cell cycle values']);
    for ns = 1:nstrains % for each strain folder
        display(['... strain ', num2str(ns)]);
        for nm = 1:nmedias % for each media folder
            display(['....... media ', num2str(nm)]);
            nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                interval = intervals{ns,nm}(nr);
                divtable = tables.(divkey){ns,nm}{nr};
                areatable = tables.(areakey){ns,nm}{nr};
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                % EMPTY TABLES THAT WILL BE COMPLETED
                area_grtable = NaN*zeros(size(divtable));
                area_r2 = NaN*zeros(size(divtable));
                area_err = NaN*zeros(size(divtable));
                len_grtable = NaN*zeros(size(divtable));
                len_r2 = NaN*zeros(size(divtable));
                len_err = NaN*zeros(size(divtable));
                divtimes = NaN*zeros(size(divtable));
                birthareas = NaN*zeros(size(divtable));
                divareas = NaN*zeros(size(divtable));
                birthlens = NaN*zeros(size(divtable));
                divlens = NaN*zeros(size(divtable));
                meangfps = NaN*zeros(size(divtable));
                maxgfps = NaN*zeros(size(divtable));
                meanmchs = NaN*zeros(size(divtable));
                for lin = 1:size(areatable,1) % for each lineage
                    areasp = getvalids(areatable(lin,:));
                    lensp = getvalids(lentable(lin,:));
                    divsp = getvalids(divtable(lin,:));
                    gfpsp = getvalids(gfptable(lin,:));
                    mchsp = getvalids(mchtable(lin,:));
                    % DO SOME SAFETY CHECKINGS
                    if(not((length(lensp)==length(gfpsp)) &&...
                           (length(lensp)==length(areasp)) &&...
                           (length(lensp)==length(mchsp))))
                        display(['WARNING: Inconsistent number of time-points in ', ...
                                 'lineage ',num2str(lin)]);
                        continue;
                    end
                    if(length(lensp)==0)
                        display(['WARNING: Empty lineage: ',num2str(lin),...
                                 ' Repeat: ',repeats{ns,nm}{nr}]);
                        continue;
                    end
                    if(not(max(divsp)<=(length(areasp)-1)))
                        display(['WARNING: Division event at non recorded time-point in ', ...
                                 'lineage ',num2str(lin)]);
                        continue;
                    end
                    % INFER THE TIME VECTOR
                    time = [0:(length(lensp)-1)]*interval/60; 
                    startp = 1;
                    if(sum(divsp)==0) % no division
                        if(length(time)>=minpts) 
                            % estimate growth-rates
                            % Cell area growth-rate
                            [rate,err,r2] = calculategrowthrate(time,areasp);
                            area_grtable(lin,1) = rate;
                            area_r2(lin,1) = r2;
                            area_err(lin,1) = err;
                            % Cell length growth-rate
                            [rate,err,r2] = calculategrowthrate(time,lensp);
                            len_grtable(lin,1) = rate;
                            len_r2(lin,1) = r2;
                            len_err(lin,1) = err;                            
                        end
                        % Fluorescence cell-cycle metrics
                        meangfps(lin,1) = mean(gfpsp);
                        maxgfps(lin,1) = max(gfpsp);
                        meanmchs(lin,1) = mean(mchsp);
                    else 
                        for ndiv = 1:length(divsp) % for each cell cycle
                            if(divsp(ndiv)==0) 
                                % first division just at time 0, skip
% $$$                                 display(['WARNING: Division event at time 0, ', ...
% $$$                                          'lineage ',num2str(lin), ' Repeat: ',repeats{ns,nm}{nr}]);
                                startp = 2;
                                continue;
                            end
                            endp = divsp(ndiv)+1;
                            if((endp-startp+1)>=minpts) % estimate growth-rates
                                % Cell area growth-rate
                                [rate,err,r2] = calculategrowthrate(time(startp:endp),areasp(startp:endp));
                                area_grtable(lin,ndiv) = rate;
                                area_r2(lin,ndiv) = r2;
                                area_err(lin,ndiv) = err;
                                % Cell length growth-rate
                                [rate,err,r2] = calculategrowthrate(time(startp:endp),lensp(startp:endp));
                                len_grtable(lin,ndiv) = rate;
                                len_r2(lin,ndiv) = r2;
                                len_err(lin,ndiv) = err;                                
                            end
                            % Fluorescence cell-cycle metrics
                            meangfps(lin,ndiv) = mean(gfpsp(startp:endp));
                            maxgfps(lin,ndiv) = max(gfpsp(startp:endp));
                            meanmchs(lin,ndiv) = mean(mchsp(startp:endp));
                            % save division times and metrics
                            if(ndiv>1) % do not include the first one
                                divtimes(lin,ndiv) = (endp-startp)*interval/60; 
                                birthareas(lin,ndiv) = areasp(startp);
                                divareas(lin,ndiv) = areasp(endp);
                                birthlens(lin,ndiv) = lensp(startp);
                                divlens(lin,ndiv) = lensp(endp);
                            end
                            startp = endp+1;
                        end
                        % try to estimate growth rate at the end
                        endp = length(areasp);
                        if(startp>=endp)
                            display(['WARNING: Lineage ends on a division event, ' ...
                                     'lineage: ',num2str(lin), ' Repeat: ',repeats{ns,nm}{nr}]);
                            continue;
                        end
                        if((endp-startp+1)>=minpts) % estimate growth-rates
                            % Cell area growth-rate
                            [rate,err,r2] = calculategrowthrate(time(startp:endp),areasp(startp:endp));
                            area_grtable(lin,ndiv+1) = rate;
                            area_r2(lin,ndiv+1) = r2;
                            area_err(lin,ndiv+1) = err;
                            % Cell length growth-rate
                            [rate,err,r2] = calculategrowthrate(time(startp:endp),lensp(startp:endp));
                            len_grtable(lin,ndiv+1) = rate;
                            len_r2(lin,ndiv+1) = r2;
                            len_err(lin,ndiv+1) = err;
                        end
                        % Fluorescence cell-cycle metrics
                        meangfps(lin,ndiv+1) = mean(gfpsp(startp:endp));
                        maxgfps(lin,ndiv+1) = max(gfpsp(startp:endp));
                        meanmchs(lin,ndiv+1) = mean(mchsp(startp:endp));
                    end
                end
                % SAVE TABLES
                tables.('divtimes'){ns,nm}{nr} = divtimes;
                tables.('area_grates'){ns,nm}{nr} = area_grtable;
                tables.('area_grates_r2'){ns,nm}{nr} = area_r2;
                tables.('area_grates_err'){ns,nm}{nr} = area_err;
                tables.('len_grates'){ns,nm}{nr} = len_grtable;
                tables.('len_grates_r2'){ns,nm}{nr} = len_r2;
                tables.('len_grates_err'){ns,nm}{nr} = len_err;
                tables.('birthareas'){ns,nm}{nr} = birthareas;
                tables.('divareas'){ns,nm}{nr} = divareas;
                tables.('birthlens'){ns,nm}{nr} = birthlens;
                tables.('divlens'){ns,nm}{nr} = divlens;
                tables.('mean_gfp'){ns,nm}{nr} = meangfps;
                tables.('max_gfp'){ns,nm}{nr} = maxgfps;
                tables.('mean_mcherry'){ns,nm}{nr} = meanmchs;
            end
        end
    end
end