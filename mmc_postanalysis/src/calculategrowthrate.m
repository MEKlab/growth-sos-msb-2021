function [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time,values,STR1,STR1val,STR2,STR2val)
    
    initial_points = 5;
    window = 5;
    OPTIONAL_STRS = {'initial_points','window'};
    if nargin < 4
    elseif nargin == 4
        Input_Values = {STR1, STR1val};
    elseif nargin == 6
        Input_Values = {STR1, STR1val; STR2, STR2val};
    else
        error('Incorrect paired values');
    end
    for i=1:1:((nargin-2)/2)
        STR = Input_Values{i,1};
        STRval = Input_Values{i,2};
        z = strmatch(STR,OPTIONAL_STRS,'exact');
        if z == 1 % 'initial_points'
            initial_points = STRval;
        elseif z == 2 % 'window'
            window = STRval;
        else
            error ('Unknown Key paired value');
        end
    end
    
    if(~(length(time)==length(values)))
        error('Time and value vectors are not the same size');
    end
    
    [pfit,gof] = fit(time',log(values)','poly1');
    cfit = confint(pfit);                            
    rate = pfit.p1;
    r2 = gof.rsquare;
    err = abs(cfit(1,1)-cfit(2,1))/2;
    
% $$$     if(length(time)>initial_points)
% $$$         upto = initial_points; 
% $$$         [pfit,gof] = fit(time(1:upto)',log(values(1:upto))','poly1');
% $$$         cfit = confint(pfit);                            
% $$$         init_rate = pfit.p1;
% $$$         init_r2 = gof.rsquare;
% $$$         init_err = abs(cfit(1,1)-cfit(2,1))/2;                
% $$$     else % values are less than specified. Let's return whatever we estimated with
% $$$          % the whole set.
% $$$         init_rate = rate;
% $$$         init_r2 = r2;
% $$$         init_err = err;
% $$$     end
    if(length(time)>window)
        midsize = min(values)+log(2)*(max(values)-min(values));
        midpt = find(values>=midsize);
        midpt = midpt(1);
        if(length(values)<midpt+floor(window/2)) % hits at the end
            righp = length(values);
            leftp = length(values)-window+1;
        elseif(1>midpt-floor(window/2)) % hits at the beginning
            leftp = 1;
            righp = window;
        else % it cannot be eiher cause we asked total length to be bigger
             % than window
            leftp = midpt-floor(window/2);
            righp = midpt+floor(window/2);
        end
        [pfit,gof] = fit(time(leftp:righp)',log(values(leftp:righp))','poly1');
        cfit = confint(pfit);                            
        mid_rate = pfit.p1;
        mid_r2 = gof.rsquare;
        mid_err = abs(cfit(1,1)-cfit(2,1))/2;                
    else
        mid_rate = rate;
        mid_r2 = r2;
        mid_err = err;
    end
end