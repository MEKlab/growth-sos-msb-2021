function [sig] = getsig(val)

    if(val>1e06)
        sig = 6;
    elseif(val>1e05)
        sig = 5;
    elseif(val>1e04)
        sig = 4;
    elseif(val>1e03)
        sig = 3;
    elseif(val>1e02)
        sig = 2;
    elseif(val>1e01)
        sig = 1;
    elseif(val>1)
        sig = 0;
    elseif(val>1e-1)
        sig = -1;
    elseif(val>1e-2)
        sig = -2;
    elseif(val>1e-3)
        sig = -3;
    elseif(val>1e-4)
        sig = -4;
    elseif(val>1e-5)
        sig = -5;
    elseif(val>1e-6)
        sig = -6;
    elseif(val>1e-7)
        sig = -7
    elseif(val>1e-8)
        sig = -8;
    elseif(val==0)
        sig = 1;
    else
        error('out of bounds');
    end

end