function [med, p25, p75] = safemedian(vals)

    vals = vals(not(isnan(vals)));
    med = median(vals);
    p25 = prctile(vals,25);
    p75 = prctile(vals,75);
    
end
