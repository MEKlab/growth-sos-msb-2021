function [hf] = relhist(x,bins)

h = histc(x,bins);
hf = h./sum(h);

end