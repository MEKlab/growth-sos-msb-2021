function [totalllh] = getalphalhs(tohightimes,nonhightimes,interval,alpha)

totalllh = 0;

obsprobs = exp(-alpha.*(tohightimes-interval))-exp(-alpha.*(tohightimes));
obsllhs = log(obsprobs);

negprobs = exp(-alpha.*nonhightimes);
negllhs = log(negprobs);

totalllh = sum(obsllhs) + sum(negllhs);

    
end