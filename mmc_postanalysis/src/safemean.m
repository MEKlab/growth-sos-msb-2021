function [vavg, vstd] = safemean(vals)

    vals = vals(not(isnan(vals)));
    vavg = mean(vals);
    vstd = std(vals);

end