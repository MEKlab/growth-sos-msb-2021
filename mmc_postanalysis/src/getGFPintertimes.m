function [tohigh,tolow,nottohigh,nottolow] = getGFPintertimes(gfptable,timetable,sosthr)

getfirst = @(x) x(1); % first element of a vector
getlast = @(x) x(end); % 
maxevents = 20;
nlin = size(gfptable,1);
tohigh = NaN*zeros(nlin,maxevents);
tolow = NaN*zeros(nlin,maxevents);
nottohigh = NaN;%*zeros(nlin,maxevents);
nottolow = NaN;%*zeros(nlin,maxevents);
nhigh = 0; % for counting events
nlow = 0; % for counting events
nsmooth = 10;
for lin = 1:nlin
    gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
    times = getvalids(timetable(lin,:));
    if(not(length(gfpsp)==length(times))) % safety check
        display(['WARNING: odd number of points in lineage',num2str(lin)]);
        continue; % skip it for now
    end
    if(length(gfpsp)==0) % safety check
        %%display(['WARNING: no points in lineage',num2str(lin)]);
        continue; % skip it for now
    end
    highpts = gfpsp>=sosthr;
    start = 1;
    if(highpts(start)==1)
        currentlow = 0;
        nhigh = nhigh+1;
    else
        currentlow = 1;
        nlow = nlow+1;
    end
    tohighdeltas = [];
    tolowdeltas = [];
    while(start<length(gfpsp))
        pos = find(highpts(start:end)==currentlow);
        if(length(pos)>0)
            transpt = getfirst(pos) + start -1;
            deltat = times(transpt) - times(start);
            if(currentlow==1) % current low point
                tohighdeltas = [tohighdeltas,deltat];
                currentlow = 0; % current point has changed high
                nhigh = nhigh+1; % new high event
            else % current high point
                tolowdeltas = [tolowdeltas,deltat];
                currentlow = 1; % current point has changed low
                nlow = nlow+1; % new low event
            end
            start = transpt;
        else % exit
            start = length(gfpsp);
        end
    end
    % Save the inter event times
    if(length(tohighdeltas)>0)
        tohigh(lin,1:length(tohighdeltas)) = tohighdeltas;
    end
    if(length(tolowdeltas)>0)
        tolow(lin,1:length(tolowdeltas)) = tolowdeltas;
    end
end

% Resize the matrix at the end
if(sum(sum(not(isnan(tohigh))>0)))
    tohigh = tohigh(:,1:getlast(find(sum(not(isnan(tohigh)))>0)));
else
    tohigh = NaN*zeros(nlin,1);
end
if(sum(sum(not(isnan(tolow))>0)))
    tolow = tolow(:,1:getlast(find(sum(not(isnan(tolow)))>0)));
else
    tolow = NaN*zeros(nlin,1);
end

end