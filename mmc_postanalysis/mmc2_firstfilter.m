% The goal will be to take the output from mmc_preprocess.m and two filters: ->
%
% 1) Removing division events that take place within some minimum time-points
% distance to the end of the lineage recording. The reason is that later we
% will use growth-rate to filter lineages, and we can't calculate a
% growth-rate if the time-points after division are too few. You can find the
% lineages edited this way looking at the 'edited1' key in tables.
% 
% 2) In some case the distribution of mKate signal took sometime to
% stabilize. We will identify when the signal becomes stable by following a
% proxy for the derivative, and removing the time-points in between. Plots
% will be generated summarizing

mmc0_aux();
makeplots = 1;
PaperPosition = [0,0,20,10];
outlog = fopen('mmc2_firstfilter.log','w');
plotsfolder = 'mmc2_firstfilter_plots/'; % Where plots will be saved
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder
load('mmc1_preprocess_output.mat');

% ====================== REMOVING END DIVISIONS
% Later we will remove ending portions of the trajectory if they end in
% non-growing cells. As we are using a minimum 3 points to calculate
% growth-rate, for consistency we should accept divisions only if there is a
% minimum number of time-points after that division.
minpoints = 3; 
fprintf(outlog,['Removing divisions where less than ',num2str(minpoints),' points after\n']);
% Correct all of them
tables.('edited1') = cell(length(strains),length(medias));
for ns = 1:length(strains) % for each strain folder
    for nm = 1:length(medias) % for each media folder
        nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat
            % get the tables
            divtable = tables.(divkey){ns,nm}{nr};
            aretable = tables.(areakey){ns,nm}{nr};
            lentable = tables.(lenkey){ns,nm}{nr};
            gfptable = tables.(gfpkey){ns,nm}{nr};
            mchtable = tables.(mchkey){ns,nm}{nr};
            edited = zeros(size(aretable,1),1);
            for lin = 1:size(aretable,1) % for each lineage
                areasp = getvalids(aretable(lin,:));
                divsp = getvalids(divtable(lin,:));
                % Remove divisions that have less than minpoints time
                % point after. We will retrograde and remove as many
                % division as needed.
                while((length(areasp)>0) && ...
                      (length(divsp)>0) && ...
                      ((max(divsp)+1)>=(length(getvalids(aretable(lin,:)))-minpoints)))
                    edited(lin) = 1;
                    % remove last division remove those points in the area, length, gfp, and mkate
                    % tables.
                    divtable(lin,length(divsp)) = NaN; 
                    aretable(lin,max(divsp)+2:end) = NaN; 
                    lentable(lin,max(divsp)+2:end) = NaN;
                    gfptable(lin,max(divsp)+2:end) = NaN;
                    mchtable(lin,max(divsp)+2:end) = NaN;
                    divsp = getvalids(divtable(lin,:));
                end
            end
            % save the edits
            tables.(divkey){ns,nm}{nr} = divtable;
            tables.(areakey){ns,nm}{nr} = aretable;
            tables.(lenkey){ns,nm}{nr} = lentable;
            tables.(gfpkey){ns,nm}{nr} = gfptable;
            tables.(mchkey){ns,nm}{nr} = mchtable;
            tables.('edited1'){ns,nm}{nr} = edited;
            fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},...
                     ', ',num2str(sum(edited)),' lineages edited\n']);
            rows = find(edited>0);
            if(length(rows)>0)
                fprintf(outlog,['  in detail: ',num2str(rows'),'\n']);
            end
        end
    end
end

% ====================== IDENTIFYING STABLE RANGE OF MCHERRY SIGNAL
% We want to identify the time window of the experiments where the median mKate
% signal is "stable". We do this by estimating the derivative of the signal with
% some heuristics. The parameters for deciding what was stable were manually
% tuned based on visual inspection.
nsmooth = 10; 
mchcut = 0.0025;
% making figures
if(makeplots)
    fig1 = figure('visible','off');
    fig2 = figure('visible','off');
end
[nstrains,nmedias] = size(tables.(areakey));
truncatefrom = cell(length(strains),length(medias)); 
fprintf(outlog,['Estimating when median mKate signal is stable and saving plots\n']);
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
        truncat = zeros(nrepeats,1);
        for nr = 1:nrepeats % for each repeat
            color = repeat2color.(repeats{ns,nm}{nr});
            interval = intervals{ns,nm}(nr);
            mchtable = tables.(mchkey){ns,nm}{nr};
            gfptable = tables.(gfpkey){ns,nm}{nr};
            gfpmed = zeros(1,size(gfptable,2));
            gfpup = zeros(1,size(gfptable,2));
            gfpdw = zeros(1,size(gfptable,2));
            mchmed = zeros(1,size(gfptable,2));
            mchup = zeros(1,size(gfptable,2));
            mchdw = zeros(1,size(gfptable,2));
            for t = 1:size(gfptable,2)
                gfps = getvalids(gfptable(:,t));
                gfpmed(t) = prctile(gfps,50);
                gfpup(t) = prctile(gfps,75);
                gfpdw(t) = prctile(gfps,25);                
                mchs = getvalids(mchtable(:,t));
                mchmed(t) = prctile(mchs,50);
                mchup(t) = prctile(mchs,75);
                mchdw(t) = prctile(mchs,25);
            end
            time = [1:size(gfptable,2)]*interval/60; 
            truncat(nr) = getfirst(find(abs(diff(smooth(mchmed,nsmooth)))./median(mchmed)<mchcut));
            stime = truncat(nr)*interval/60;
            if(makeplots)
                set(0,'CurrentFigure',fig1); % GFP figure
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(time,gfpmed,'-','LineWidth',2,'Color',color);
                plot(time,gfpup,':','LineWidth',0.5,'Color',color);
                plot(time,gfpdw,':','LineWidth',0.5,'Color',color);
                plot([stime,stime],[0.1,10],':','LineWidth',1,'Color',color);
                myupdownfill(time,gfpdw,gfpup,color,0.1,0);%
                figlabels('xlabel','time [h]',...
                          'ylabel','GFP [A.U.]',...
                          'title',[strains{ns},'/',medias{nm}]);
                set(gca,'YScale','log');
                set(gca,'FontSize',10);
                ylim([0.5 10]);
                xlim([0 18]);
                yticks([0.5 1 5 10]);
                set(0,'CurrentFigure',fig2); % mkate figure
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(time,mchmed,'-','LineWidth',2,'Color',color);
                plot(time,mchup,':','LineWidth',0.5,'Color',color);
                plot(time,mchdw,':','LineWidth',0.5,'Color',color);            
                plot([stime,stime],[0,5000],':','LineWidth',1,'Color',color);
                myupdownfill(time,mchdw,mchup,color,0.1,0);%
                figlabels('xlabel','time [h]',...
                          'ylabel','mKate2 [A.U.]',...
                          'title',[strains{ns},'/',medias{nm}]);
                %set(gca,'YScale','log');
                set(gca,'FontSize',10);
                ylim([0 3.5]);
                yticks([0 1 2 3]);
                xlim([0 18]);
            end
        end
        truncatefrom{ns,nm} = truncat;
    end
end
if(makeplots)
    figs = [fig1,fig2];
    names = {'TRUNCATION_BEFORE_GFP_quartiles_over_time',...
             'TRUNCATION_BEFORE_mKate_quartiles_over_time'};
    for nfig = 1:length(figs)
        fig = figs(nfig);
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = PaperPosition;
        print('-r600',fig,['./',plotsfolder,names{nfig},'.png'],'-dpng');
    end
    close all;
end

% ====================== TRUNCATING DATASETS BASED ON MKATE STABILITY 
% We will use the time points inferred previously. However, we will truncate
% only if that means we need to remove more than 3 points, otherwise we will
% not worry about it.
fprintf(outlog,['Truncating trajectories based on mKate stability\n']);
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
        truncat = truncatefrom{ns,nm};
        for nr = 1:nrepeats % for each repeat
            timethr = truncat(nr);
            if(timethr>4) % if is less than 4... let's not bother about it
                fprintf(outlog,[' Strain ',strains{ns},' Repeat ',repeats{ns,nm}{nr},...
                         ', ',num2str(timethr-1),' time points removed\n']);
                divtable = tables.(divkey){ns,nm}{nr};
                aretable = tables.(areakey){ns,nm}{nr};
                lentable = tables.(lenkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                mchtable = tables.(mchkey){ns,nm}{nr};
                % edit time dependent tables
                aretable = aretable(:,timethr:end);
                lentable = lentable(:,timethr:end);
                gfptable = gfptable(:,timethr:end);
                mchtable = mchtable(:,timethr:end);
                % edit divisions for each lineage
                for lin = 1:size(divtable,1) 
                    % divisions start from 0. Now timethr-1 should be 0.
                    divs = divtable(lin,:);
                    divsp = getvalids(divs) - timethr + 1;
                    divsp = divsp(divsp>=0); % find all positive or 0
                    divs(:) = NaN; % set all to NaN
                    divs(1:length(divsp)) = divsp; % replace
                    divtable(lin,:) = divs; % update
                end
                if(sum(isnan(aretable'))==size(aretable,2))
                    fprintf(outlog,[' WARNING: some lineages were completely lost ' ...
                             'after time-point removal\n'])
                end
                % save the edits
                tables.(divkey){ns,nm}{nr} = divtable;
                tables.(areakey){ns,nm}{nr} = aretable;
                tables.(lenkey){ns,nm}{nr} = lentable;
                tables.(gfpkey){ns,nm}{nr} = gfptable;
                tables.(mchkey){ns,nm}{nr} = mchtable;
            end
        end
    end
end

if(makeplots)
    % re-making figures
    fig1 = figure('visible','off');
    fig2 = figure('visible','off');
    [nstrains,nmedias] = size(tables.(areakey));
    truncatefrom = cell(length(strains),length(medias)); 
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
            truncat = zeros(nrepeats,1);
            for nr = 1:nrepeats % for each repeat
                color = repeat2color.(repeats{ns,nm}{nr});
                mchtable = tables.(mchkey){ns,nm}{nr};
                gfptable = tables.(gfpkey){ns,nm}{nr};
                gfpmed = zeros(1,size(gfptable,2));
                gfpup = zeros(1,size(gfptable,2));
                gfpdw = zeros(1,size(gfptable,2));
                mchmed = zeros(1,size(gfptable,2));
                mchup = zeros(1,size(gfptable,2));
                mchdw = zeros(1,size(gfptable,2));
                for t = 1:size(gfptable,2)
                    gfps = getvalids(gfptable(:,t));
                    gfpmed(t) = prctile(gfps,50);
                    gfpup(t) = prctile(gfps,75);
                    gfpdw(t) = prctile(gfps,25);                
                    mchs = getvalids(mchtable(:,t));
                    mchmed(t) = prctile(mchs,50);
                    mchup(t) = prctile(mchs,75);
                    mchdw(t) = prctile(mchs,25);
                end
                time = [1:size(gfptable,2)]*interval/60; 
                set(0,'CurrentFigure',fig1); % GFP figure
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(time,gfpmed,'-','LineWidth',2,'Color',color);
                plot(time,gfpup,':','LineWidth',0.5,'Color',color);
                plot(time,gfpdw,':','LineWidth',0.5,'Color',color);
                myupdownfill(time,gfpdw,gfpup,color,0.1,0);%
                figlabels('xlabel','time [h]',...
                          'ylabel','GFP [A.U.]',...
                          'title',[strains{ns},'/',medias{nm}]);
                set(gca,'YScale','log');
                set(gca,'FontSize',10);
                ylim([0.5 10]);
                xlim([0 18]);
                yticks([0.5 1 5 10]);
                set(0,'CurrentFigure',fig2); % mkate figure
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(time,mchmed,'-','LineWidth',2,'Color',color);
                plot(time,mchup,':','LineWidth',0.5,'Color',color);
                plot(time,mchdw,':','LineWidth',0.5,'Color',color);            
                myupdownfill(time,mchdw,mchup,color,0.1,0);%
                figlabels('xlabel','time [h]',...
                          'ylabel','mKate2 [A.U.]',...
                          'title',[strains{ns},'/',medias{nm}]);
                %set(gca,'YScale','log');
                set(gca,'FontSize',10);
                ylim([0 3.5]);
                yticks([0 1 2 3]);
                xlim([0 18]);
            end
        end
    end
    figs = [fig1,fig2];
    names = {'TRUNCATION_AFTER_GFP_quartiles_over_time',...
             'TRUNCATION_AFTER_mKate_quartiles_over_time'};
    for nfig = 1:length(figs)
        fig = figs(nfig);
        fig.PaperUnits = 'centimeters';
        fig.PaperPosition = PaperPosition;
        print('-r600',fig,['./',plotsfolder,names{nfig},'.png'],'-dpng');
    end
    close all;
end

fclose(outlog);
save('mmc2_firstfilter_output','tables','intervals','repeats','datafolder','strains','medias','rmvsets','repeat2color');
