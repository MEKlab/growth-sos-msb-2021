
mmc0_aux();
outlog = fopen('mmc6_alpha.log','w');
plotsfolder = 'mmc6_alpha_plots/'; % Where plots will be saved
load('mmc4_secondfilter_output.mat');
system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

nsmooth = 5; % we will smooth the trajectories to remove some of the noise.
sosthrvec = 5:20;
alpharange1 = logspace(log10(1e-10),log10(1e-1),1000);
alpharange2 = linspace(0,0.1,200);
PaperPosition = [0,0,17,10];
niter = 100000;
epsilon = 0.001;
                
[nstrains,nmedias] = size(tables.(areakey));
% initialize the 'alpha' table
tables.('alpha') = cell(nstrains,nmedias);
tables.('alpha_p5') = cell(nstrains,nmedias);
tables.('alpha_p95') = cell(nstrains,nmedias);
tables.('alpha_T0') = cell(nstrains,nmedias);
tables.('alpha_T1') = cell(nstrains,nmedias);
for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat
            tables.('alpha'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_p5'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_p95'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_T0'){ns,nm}{nr} = zeros(size(sosthrvec));
            tables.('alpha_T1'){ns,nm}{nr} = zeros(size(sosthrvec));
        end
    end
end

for nthr = 1:length(sosthrvec)
    highsosthr = sosthrvec(nthr);
    fig = figure('visible','off');
    fig2 = figure('visible','off');
    for ns = 1:nstrains % for each strain folder
        for nm = 1:nmedias % for each media folder
            nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
            for nr = 1:nrepeats % for each repeat
                color = repeat2color.(repeats{ns,nm}{nr});
                repeat = repeats{ns,nm}{nr};
                interval = intervals{ns,nm}(nr)/60;
                gfptable = tables.(gfpkey){ns,nm}{nr};
                timetable = tables.('times'){ns,nm}{nr};
                t1vec = [];
                t0vec = [];
                for lin = 1:size(gfptable,1)
                    gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
                    times = getvalids(timetable(lin,:));
                    if((length(gfpsp)>0)&&(gfpsp(1)<highsosthr))
                        % we care only lineages that begin with low SOS
                        if(sum(gfpsp>=highsosthr)>0) % some high SOS
                            t1vec = [t1vec,times(getfirst(find(gfpsp>=highsosthr)))-times(1)];
                        else % lineage never reached
                            t0vec = [t0vec,times(end)-times(1)];
                        end
                    end
                end
                set(0,'CurrentFigure',fig); 
                [lhs,alpha_maxl,maxlh] = getalphalhs2(t1vec,t0vec,interval,alpharange1);
                [alphawalk,naccepted] = alphaMC(t1vec, t0vec, interval, niter, epsilon);
                p5 = prctile(alphawalk,5);
                p95 = prctile(alphawalk,95);
                tables.('alpha'){ns,nm}{nr}(nthr) = alpha_maxl;
                tables.('alpha_p5'){ns,nm}{nr}(nthr) = p5;
                tables.('alpha_p95'){ns,nm}{nr}(nthr) = p95;
                tables.('alpha_T0'){ns,nm}{nr}(nthr) = length(t0vec);
                tables.('alpha_T1'){ns,nm}{nr}(nthr) = length(t1vec);
                subplot(2,3,3*(ns-1)+nm); hold on;
                plot(alpharange1,1.*lhs,'-','LineWidth',2,'Color',color);
                plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color);
                plot([alpha_maxl,alpha_maxl],[-1000,maxlh],':','LineWidth',1,'Color',color);
                plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                figlabels('xlabel','$\alpha$ [1/h]',...
                          'ylabel','log likelihood',...
                          'title',[strains{ns},'/',medias{nm}]);
                axis([min(alpharange1) max(alpharange1) -1000 -10]);
                set(0,'CurrentFigure',fig2); 
                subplot(2,3,3*(ns-1)+nm); hold on;
                rf = relhist(alphawalk,alpharange2);
                leftind = find(alpharange2>=p5);
                leftind = leftind(1);
                rightind = find(alpharange2>=p95);
                rightind = rightind(1);
                myupdownfill(alpharange2(leftind:rightind),zeros(size(leftind:rightind)),rf(leftind:rightind),color,0.2,0);%
                plot([alpha_maxl,alpha_maxl],[0,max(rf)],':','LineWidth',1,'Color',color);
                plot(alpharange2,rf,'-','LineWidth',1,'Color',color);
                %plot([alpha_maxl],[max(rf)],'o','LineWidth',1,'Color',color,'MarkerFaceColor',[1 1 1]);
                ylim([0 0.25]);
                xlim([0 0.05]);
                figlabels('xlabel','$\alpha$ [1/h]',...
                          'ylabel','p.d.f',...
                          'title',[strains{ns},'/',medias{nm}]);
            end
        end
    end
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = PaperPosition;
    print('-r600',fig,['./',plotsfolder,'ALPHA_LLH_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig);
    fig2.PaperUnits = 'centimeters';
    fig2.PaperPosition = PaperPosition;
    print('-r600',fig2,['./',plotsfolder,'ALPHA_PDF_SOSTHR_',num2str(highsosthr),'.png'],'-dpng');
    close(fig2);
end

fclose(outlog);
save('mmc6_alpha_output','tables','intervals','repeats','datafolder','strains','medias','rmvsets','repeat2color','sosthrvec');