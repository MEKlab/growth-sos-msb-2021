% The goal will be to take the output from mmc_firstfilter.m and use it to
% compute all cellcycle related metrics

mmc0_aux();
outlog = fopen('mmc3_cellcycle.log','w');
load('mmc2_firstfilter_output.mat');

minpts = 3; % minimum number of points to estimate growth rates
midgrwindow = 10; % minimum to estimate the "mid" growth rate. This is mostly
                  % to pick elongating cells, so we will prefer a big window
                  % to avoid noise in the measurements.
[nstrains,nmedias] = size(tables.(areakey));
% NEW TABLES TO BE COMPUTED
tables.('divtimes') = cell(nstrains,nmedias);
tables.('obstime') = cell(nstrains,nmedias);
tables.('birthareas') = cell(nstrains,nmedias);
tables.('divareas') = cell(nstrains,nmedias);
tables.('birthlens') = cell(nstrains,nmedias);
tables.('divlens') = cell(nstrains,nmedias);
tables.('area_grates') = cell(nstrains,nmedias);
tables.('area_grates_r2') = cell(nstrains,nmedias);
tables.('area_grates_err') = cell(nstrains,nmedias);
tables.('len_grates') = cell(nstrains,nmedias);
tables.('len_grates_r2') = cell(nstrains,nmedias);
tables.('len_grates_err') = cell(nstrains,nmedias);
tables.('mid_area_grates') = cell(nstrains,nmedias);
tables.('mid_area_grates_r2') = cell(nstrains,nmedias);
tables.('mid_area_grates_err') = cell(nstrains,nmedias);
tables.('mid_len_grates') = cell(nstrains,nmedias);
tables.('mid_len_grates_r2') = cell(nstrains,nmedias);
tables.('mid_len_grates_err') = cell(nstrains,nmedias);
tables.('mean_gfp') = cell(nstrains,nmedias); 
tables.('max_gfp') = cell(nstrains,nmedias);
tables.('mean_mcherry') = cell(nstrains,nmedias);
tables.('last_mcherry') = cell(nstrains,nmedias);
tables.('init_length') = cell(nstrains,nmedias);
tables.('last_length') = cell(nstrains,nmedias);
fprintf(outlog,['Estimating cell cycle values\n']);
for ns = 1:nstrains % for each strain folder
    fprintf(outlog,['... strain ', num2str(ns),'\n']);
    for nm = 1:nmedias % for each media folder
        fprintf(outlog,['....... media ', num2str(nm),'\n']);
        nrepeats = length(tables.(areakey){ns,nm}); % number of repeats
        for nr = 1:nrepeats % for each repeat
            interval = intervals{ns,nm}(nr);
            divtable = tables.(divkey){ns,nm}{nr};
            areatable = tables.(areakey){ns,nm}{nr};
            lentable = tables.(lenkey){ns,nm}{nr};
            gfptable = tables.(gfpkey){ns,nm}{nr};
            mchtable = tables.(mchkey){ns,nm}{nr};
            % EMPTY TABLES THAT WILL BE COMPLETED
            area_grtable = NaN*zeros(size(divtable));
            area_r2 = NaN*zeros(size(divtable));
            area_err = NaN*zeros(size(divtable));
            len_grtable = NaN*zeros(size(divtable));
            len_r2 = NaN*zeros(size(divtable));
            len_err = NaN*zeros(size(divtable));
            mid_area_grtable = NaN*zeros(size(divtable));
            mid_area_r2 = NaN*zeros(size(divtable));
            mid_area_err = NaN*zeros(size(divtable));
            mid_len_grtable = NaN*zeros(size(divtable));
            mid_len_r2 = NaN*zeros(size(divtable));
            mid_len_err = NaN*zeros(size(divtable));
            divtimes = NaN*zeros(size(divtable));
            birthareas = NaN*zeros(size(divtable));
            divareas = NaN*zeros(size(divtable));
            birthlens = NaN*zeros(size(divtable));
            divlens = NaN*zeros(size(divtable));
            meangfps = NaN*zeros(size(divtable));
            maxgfps = NaN*zeros(size(divtable));
            meanmchs = NaN*zeros(size(divtable));
            obstime = NaN*zeros(size(divtable));
            last_mcherry = NaN*zeros(size(divtable));
            last_length = NaN*zeros(size(divtable));
            init_length = NaN*zeros(size(divtable));
            for lin = 1:size(areatable,1) % for each lineage
                areasp = getvalids(areatable(lin,:));
                lensp = getvalids(lentable(lin,:));
                divsp = getvalids(divtable(lin,:));
                gfpsp = getvalids(gfptable(lin,:));
                mchsp = getvalids(mchtable(lin,:));
                % DO SOME SAFETY CHECKINGS
                if(not((length(lensp)==length(gfpsp)) &&...
                       (length(lensp)==length(areasp)) &&...
                       (length(lensp)==length(mchsp))))
                    fprintf(outlog,['WARNING: Inconsistent number of time-points in ', ...
                                    'lineage ',num2str(lin),'\n']);
                    continue;
                end
                if(length(lensp)==0)
                    fprintf(outlog,['WARNING: Empty lineage: ',num2str(lin),...
                                    ' Repeat: ',repeats{ns,nm}{nr},'\n']);
                    continue;
                end
                if(not(max(divsp)<=(length(areasp)-1)))
                    fprintf(outlog,['WARNING: Division event at non recorded time-point in ', ...
                                    'lineage ',num2str(lin),'\n']);
                    continue;
                end
                % INFER THE TIME VECTOR
                time = [0:(length(lensp)-1)]*interval/60; 
                startp = 1;
                if(sum(divsp)==0) % no division
                    if(length(time)>=minpts) 
                        % estimate growth-rates
                        % Cell area growth-rate
                        [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time,areasp,...
                                                                          'window',midgrwindow);
                        area_grtable(lin,1) = rate;
                        area_r2(lin,1) = r2;
                        area_err(lin,1) = err;
                        mid_area_grtable(lin,1) = mid_rate;
                        mid_area_r2(lin,1) = mid_r2;
                        mid_area_err(lin,1) = mid_err;
                        % Cell length growth-rate
                        [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time,lensp,...
                                                                          'window',midgrwindow);
                        len_grtable(lin,1) = rate;
                        len_r2(lin,1) = r2;
                        len_err(lin,1) = err;     
                        mid_len_grtable(lin,1) = mid_rate;
                        mid_len_r2(lin,1) = mid_r2;
                        mid_len_err(lin,1) = mid_err;     
                    end
                    % Fluorescence cell-cycle metrics
                    meangfps(lin,1) = mean(gfpsp);
                    maxgfps(lin,1) = max(gfpsp);
                    meanmchs(lin,1) = mean(mchsp);
                    obstime(lin,1) = max(time);
                    last_mcherry(lin,1) = mchsp(end);
                    last_length(lin,1) = lensp(end);
                    init_length(lin,1) = lensp(1);
                else 
                    for ndiv = 1:length(divsp) % for each cell cycle
                        if(divsp(ndiv)==0) 
                            % first division just at time 0, skip
                            fprintf(outlog,['WARNING: Division event at time 0, ', ...
                                            'lineage ',num2str(lin), ' Repeat: ',repeats{ns,nm}{nr},'\n']);
                            obstime(lin,1) = 0;
                            last_length(lin,1) = lensp(startp);
                            init_length(lin,1) = lensp(startp);
                            last_mcherry(lin,1) = mchsp(startp);
                            maxgfps(lin,1) = gfpsp(startp);
                            startp = 2;
                            continue;
                        end
                        endp = divsp(ndiv)+1;
                        if((endp-startp+1)>=minpts) % estimate growth-rates
                                                    % Cell area growth-rate
                            [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time(startp:endp),areasp(startp:endp),...
                                                                              'window',midgrwindow);
                            area_grtable(lin,ndiv) = rate;
                            area_r2(lin,ndiv) = r2;
                            area_err(lin,ndiv) = err;
                            mid_area_grtable(lin,ndiv) = mid_rate;
                            mid_area_r2(lin,ndiv) = mid_r2;
                            mid_area_err(lin,ndiv) = mid_err;
                            % Cell length growth-rate
                            [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time(startp:endp),lensp(startp:endp),...
                                                                              'window',midgrwindow);
                            len_grtable(lin,ndiv) = rate;
                            len_r2(lin,ndiv) = r2;
                            len_err(lin,ndiv) = err;     
                            mid_len_grtable(lin,ndiv) = mid_rate;
                            mid_len_r2(lin,ndiv) = mid_r2;
                            mid_len_err(lin,ndiv) = mid_err;     
                        end
                        % Fluorescence cell-cycle metrics
                        meangfps(lin,ndiv) = mean(gfpsp(startp:endp));
                        maxgfps(lin,ndiv) = max(gfpsp(startp:endp));
                        meanmchs(lin,ndiv) = mean(mchsp(startp:endp));
                        last_mcherry(lin,ndiv) = mchsp(endp);
                        % save division times and metrics
                        divareas(lin,ndiv) = areasp(endp);
                        divlens(lin,ndiv) = lensp(endp);
                        last_length(lin,ndiv) = lensp(endp);
                        init_length(lin,ndiv) = lensp(startp);
                        obstime(lin,ndiv) = (endp-startp)*interval/60; 
                        if(ndiv>1) % do not include the first one for these
                            divtimes(lin,ndiv) = (endp-startp)*interval/60; 
                            birthareas(lin,ndiv) = areasp(startp);
                            birthlens(lin,ndiv) = lensp(startp);
                        end
                        startp = endp+1;
                    end
                    % try to estimate growth rate at the end
                    endp = length(areasp);
                    if(startp>=endp)
                        fprintf(outlog,['WARNING: Lineage ends on a division event, ' ...
                                        'lineage: ',num2str(lin), ' Repeat: ',repeats{ns,nm}{nr},'\n']);
                        continue;
                    end
                    if((endp-startp+1)>=minpts) % estimate growth-rates
                                                % Cell area growth-rate
                        [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time(startp:endp),areasp(startp:endp),...
                                                                          'window',midgrwindow);
                        area_grtable(lin,ndiv+1) = rate;
                        area_r2(lin,ndiv+1) = r2;
                        area_err(lin,ndiv+1) = err;
                        mid_area_grtable(lin,ndiv+1) = mid_rate;
                        mid_area_r2(lin,ndiv+1) = mid_r2;
                        mid_area_err(lin,ndiv+1) = mid_err;
                        % Cell length growth-rate
                        [rate,err,r2,mid_rate,mid_err,mid_r2] = calculategrowthrate(time(startp:endp),lensp(startp:endp),...
                                                                          'window',midgrwindow);
                        len_grtable(lin,ndiv+1) = rate;
                        len_r2(lin,ndiv+1) = r2;
                        len_err(lin,ndiv+1) = err;
                        mid_len_grtable(lin,ndiv+1) = mid_rate;
                        mid_len_r2(lin,ndiv+1) = mid_r2;
                        mid_len_err(lin,ndiv+1) = mid_err;
                    end
                    % Fluorescence cell-cycle metrics
                    meangfps(lin,ndiv+1) = mean(gfpsp(startp:endp));
                    maxgfps(lin,ndiv+1) = max(gfpsp(startp:endp));
                    meanmchs(lin,ndiv+1) = mean(mchsp(startp:endp));
                    obstime(lin,ndiv+1) = (endp-startp)*interval/60;
                    last_length(lin,ndiv+1) = lensp(endp);
                    init_length(lin,ndiv+1) = lensp(startp);
                    last_mcherry(lin,ndiv+1) = mchsp(endp);
                end
            end
            % SAVE TABLES
            tables.('divtimes'){ns,nm}{nr} = divtimes;
            tables.('area_grates'){ns,nm}{nr} = area_grtable;
            tables.('area_grates_r2'){ns,nm}{nr} = area_r2;
            tables.('area_grates_err'){ns,nm}{nr} = area_err;
            tables.('len_grates'){ns,nm}{nr} = len_grtable;
            tables.('len_grates_r2'){ns,nm}{nr} = len_r2;
            tables.('len_grates_err'){ns,nm}{nr} = len_err;
            tables.('mid_area_grates'){ns,nm}{nr} = mid_area_grtable;
            tables.('mid_area_grates_r2'){ns,nm}{nr} = mid_area_r2;
            tables.('mid_area_grates_err'){ns,nm}{nr} = mid_area_err;
            tables.('mid_len_grates'){ns,nm}{nr} = mid_len_grtable;
            tables.('mid_len_grates_r2'){ns,nm}{nr} = mid_len_r2;
            tables.('mid_len_grates_err'){ns,nm}{nr} = mid_len_err;
            tables.('birthareas'){ns,nm}{nr} = birthareas;
            tables.('divareas'){ns,nm}{nr} = divareas;
            tables.('birthlens'){ns,nm}{nr} = birthlens;
            tables.('divlens'){ns,nm}{nr} = divlens;
            tables.('mean_gfp'){ns,nm}{nr} = meangfps;
            tables.('max_gfp'){ns,nm}{nr} = maxgfps;
            tables.('mean_mcherry'){ns,nm}{nr} = meanmchs;
            tables.('obstime'){ns,nm}{nr} = obstime;
            tables.('last_mcherry'){ns,nm}{nr} = last_mcherry;
            tables.('last_length'){ns,nm}{nr} = last_length;
            tables.('init_length'){ns,nm}{nr} = init_length;
        end
    end
end

fclose(outlog);
save('mmc3_cellcycle_output','tables','intervals','repeats','datafolder','strains','medias','rmvsets','repeat2color');