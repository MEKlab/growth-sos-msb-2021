addpath('src/');
%
strains = {'WT','2pal'};
medias  = {'Gly','Glu','Gluaa'};
rmvsets = {'Gly_20191222','Gluaa_20191101'};

% we'll use a few miscellaneous anonymous functions.
getfirst = @(x) x(1); % first element of a vector
getlast = @(x) x(end); % 
getn = @(x,n) x(n); % nth element of a vector

% For accessing data
makeplots = 0;
divkey = 'divpositions';
lenkey = 'lengthmats';
areakey = 'areamats';
gfpkey = 'gfpmat_scaled';
%mchkey = 'mchmats';
mchkey = 'mchmat_scaled';
divtimekey = 'divtimes';
gratekey = 'len_grates';
r2key = 'len_grates_r2';

% For ploting
mediapos = [1,2,3];
repeatcolors = lines(3);

% for the secondfilter
mingrate = 0.03;
