mmc0_aux();
PaperPosition = [0,0,17,10];
outfolder = 'mmc12_export/'; % 
plotsfolder = 'mmc12_export/'; % 

load('mmc6_alpha_output.mat');
[nstrains,nmedias] = size(tables.(areakey));

% export mean, divs, and alphas as a .mat file

repeat_array = cell(nstrains,nmedias,3);
strain_array = cell(nstrains,nmedias,3);
media_array = cell(nstrains,nmedias,3);
divtime_array = zeros(nstrains,nmedias,3);
popgrate_array = zeros(nstrains,nmedias,3);
alpha_mat = tables.('alpha');
beta_mat = tables.('beta');
lambda1_mat = cell(size(alpha_mat));

for ns = 1:nstrains % for each strain folder
    for nm = 1:nmedias % for each media folder
        nrepeats = length(tables.(gratekey){ns,nm}); % number of repeats
        fig1 = figure('visible','off');
        ha11 = axes(); hold on;
        fig2 = figure('visible','off');
        ha21 = axes(); hold on;
        for nr = 1:nrepeats % for each repeat
            % actual work...
            repeat = repeats{ns,nm}{nr};
            repeat = split(repeat,'_');
            repeat = repeat{2};
            % repeat
            repeat_array{ns,nm,nr} = repeat; 
            % strain
            strain_array{ns,nm,nr} = strains{ns}; 
            % media
            media_array{ns,nm,nr} = medias{nm}; 
            interval = intervals{ns,nm}(nr)/60;
            divtable = tables.(divtimekey){ns,nm}{nr};
            % division time avg and std
            divtime_array(ns,nm,nr) = mean(divtable(divtable>0));
            % Computing pop grate
            divs = divtable(divtable(:)>0);
            [bestlam, lambdavec, costs] = estimatepopgrowth(interval,divs,0.001,2);
            popgrate_array(ns,nm,nr) = bestlam;
            % Computing pop rate conditioned on max SOS level
            lambda1_mat{ns,nm}{nr} = zeros(size(alpha_mat{ns,nm}{nr}));
            gfptable = tables.('max_gfp'){ns,nm}{nr};
            gfps = gfptable(divtable>0);
            if(not(length(gfps)== length(divs)))
                error('Inconsistent dimensions');
            end
            for ngfp = 1:length(sosthrvec)
                sos_thr = sosthrvec(ngfp);
                local_divs = divs(gfps<=sos_thr);
                %display([string(sos_thr),':',string(100*length(local_divs)/length(divs))])
                [bestlam, lambdavec, costs] = estimatepopgrowth(interval,local_divs,0.001,2);
                lambda1_mat{ns,nm}{nr}(ngfp) = bestlam;
            end
            % Plotting
            color = repeat2color.(repeats{ns,nm}{nr});
            axes(ha11); hold on;
            plot(lambdavec,costs,'LineWidth',2,'Color',color);
            plot([bestlam,bestlam],[0,1],'--','LineWidth',1,'Color',color);
            axis([0 2 0 1]);
            if(nm==1)
                xlim([0 0.5])
            elseif(nm==2)
                xlim([0 1])
            else
                xlim([0 2])
            end
            axes(ha21); hold on;
            vals = divs;
            bins = linspace(0,median(vals)*4,20);
            rf = 100*relhist(vals,bins);
            plot(bins,rf,'-','LineWidth',2,'Color',color);
            if(nm==1)
                xlim([0 20])
            elseif(nm==2)
                xlim([0 10])
            else
                xlim([0 5])
            end
        end
        set(0,'CurrentFigure',fig1); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'LAMBDAPOP_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig1);    
        set(0,'CurrentFigure',fig2); % 
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[plotsfolder,'DIVSDIST_STRAIN-',num2str(ns),'_MEDIA-',num2str(nm)]);
        close(fig2);    
    end
end


save(['./',outfolder,'mmc_fig3_data.mat'], ...
     'repeat_array', 'strain_array', ...
     'divtime_array', 'popgrate_array', ...
     'lambda1_mat', 'alpha_mat','beta_mat',...
     'sosthrvec')

% $$$ date = repeat_array(1:vcount);
% $$$ strain = strain_array(1:vcount);
% $$$ media = media_array(1:vcount);
% $$$ mmc_avgdivtime = divtime_array(1:vcount);
% $$$ mmc_popgrate = popgrate_array(1:vcount);
% $$$ 
% $$$ mainT = table(strain,media,date,...
% $$$               mmc_avgdivtime,mmc_popgrate);
% $$$ 
% $$$ writetable(mainT,['./',outfolder,'MMC_VALUES.xlsx'])
% $$$ 
% $$$ for n = 1:length(sosthrvec)
% $$$     thrstr = num2str(sosthrvec(n));
% $$$     colvec = alpha_mat(1:vcount,n);
% $$$     eval(['SOS_',thrstr,'=colvec;']);
% $$$ end
% $$$ alphaT = table(strain,media,date,...
% $$$                SOS_6,SOS_7,SOS_8,SOS_9,SOS_10,SOS_11,SOS_12,SOS_13,SOS_14,SOS_15,SOS_16);
% $$$ writetable(alphaT,['./',outfolder,'MMC_ALPHA.xlsx'])