% Making figures used for explaining the calculation of alpha in the supp
% methods.

mmc0_aux();
plotsfolder = 'mmc10_alpha_methods_plots/'; % Where plots will be saved
load('mmc4_secondfilter_output.mat');
%system(['rm ',plotsfolder,'*']); % remove all previous plots in that folder

nsmooth = 5; % we will smooth the trajectories to remove some of the noise.

alpharange1 = logspace(log10(1e-10),log10(1e-1),1000);
alpharange2 = linspace(0,0.1,200);
niter = 100000;
epsilon = 0.001;

highsosthr = 8;

for ns = 2%1:nstrains % for each strain folder
    for nm = 3%1:nmedias % for each media folder
        nrepeats = length(tables.(gfpkey){ns,nm}); % number of repeats
        for nr = 2%1:nrepeats % for each repeat
            color = repeat2color.(repeats{ns,nm}{nr});
            repeat = repeats{ns,nm}{nr};
            interval = intervals{ns,nm}(nr)/60;
            gfptable = tables.(gfpkey){ns,nm}{nr};
            timetable = tables.('times'){ns,nm}{nr};
            t1vec = [];
            t0vec = [];
            for lin = 1:size(gfptable,1)
                gfpsp = smooth(getvalids(gfptable(lin,:)),nsmooth);
                times = getvalids(timetable(lin,:));
                if((length(gfpsp)>0)&&(gfpsp(1)<highsosthr))
                    % we care only lineages that begin with low SOS
                    if(sum(gfpsp>=highsosthr)>0) % some high SOS
                        t1vec = [t1vec,times(getfirst(find(gfpsp>=highsosthr)))-times(1)];
% $$$                         if(lin<=10)
% $$$                             fig = figure('visible','off'); hold on; %
% $$$                             pos = getfirst(find(gfpsp>=highsosthr));
% $$$                             plot(times,gfpsp,'r','LineWidth',2);
% $$$                             plot([min(times),times(pos)],[highsosthr,highsosthr],'--','LineWidth',1,'color',[0.3,0.3,0.3]);
% $$$                             plot([times(pos),times(pos)],[min(gfpsp),highsosthr],'--','LineWidth',1,'color',[0.3,0.3,0.3]);
% $$$                             plot([times(pos)],[min(gfpsp)],'o','LineWidth',1,'color','black','markersize',8,'markerface',[0.5,0.5,0.5]);
% $$$                             set(gca,'YScale','log');
% $$$                             ylim([min(gfpsp) 20]);
% $$$                             yticks([1 2.5 5 10 20]);
% $$$                             xlim([0 15]);
% $$$                             xticks([0 5 10 15]);
% $$$                             mypdfpngexpfig('PaperPosition',[0 0 8 7],...
% $$$                                            'outname',[plotsfolder,'EXAMPLE_T1_',num2str(lin)]);
% $$$                             close(fig);    
% $$$                         end
                    else % lineage never reached
                        t0vec = [t0vec,times(end)-times(1)];
% $$$                         if(lin<=10)
% $$$                             fig = figure('visible','off'); hold on;
% $$$                             plot(times,gfpsp,'b','LineWidth',2);
% $$$                             plot([min(times),max(times)],[highsosthr,highsosthr],'--','LineWidth',1,'color',[0.3,0.3,0.3]);
% $$$                             plot([max(times),max(times)],[min(gfpsp),highsosthr],'--','LineWidth',1,'color',[0.3,0.3,0.3]);
% $$$                             plot([max(times)],[min(gfpsp)],'o','LineWidth',1,'color','black','markersize',8,'markerface',[0.5,0.5,0.5]);
% $$$                             set(gca,'YScale','log');
% $$$                             xlim([0 15]);
% $$$                             xticks([0 5 10 15]);
% $$$                             ylim([min(gfpsp) 20]);
% $$$                             yticks([1 2.5 5 10 20]);
% $$$                             mypdfpngexpfig('PaperPosition',[0 0 8 7],...
% $$$                                            'outname',[plotsfolder,'EXAMPLE_T0_',num2str(lin)]);
% $$$                             close(fig);    
% $$$                         end
                    end
                end
            end
            [lhs,alpha_maxl,maxlh] = getalphalhs2(t1vec,t0vec,interval,alpharange1);
            [alphawalk,naccepted] = alphaMC(t1vec, t0vec, interval, niter, epsilon);
            p5 = prctile(alphawalk,5);
            p95 = prctile(alphawalk,95);
        end
    end
end

color = [0.2 0.6 0.2];

fig = figure(); hold on; %
ts = linspace(0,15,30);
myupdownfill(ts, 1-exp(-p95.*ts),1-exp(-p5.*ts),color,0.1,0);%
plot(ts, 1-exp(-p95.*ts),'--','LineWidth',0.5,'Color',color);
plot(ts, 1-exp(-p5.*ts),'--','LineWidth',0.5,'Color',color);
plot(ts, 1-exp(-alpha_maxl.*ts),'LineWidth',2,'Color',color);
plot(sort(t1vec), cumsum(ones(size(t1vec)))/(length(t1vec)+length(t0vec)), '.-','LineWidth',2,'Color','b')
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[plotsfolder,'EXAMPLE_EXPFIT']);
close(fig);    

fig = figure('visible','off'); hold on; %
plot(alpharange1,1.*lhs,'-','LineWidth',2,'Color',color);
plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color,'markersize',8);
plot([alpha_maxl,alpha_maxl],[-1000,maxlh],':','LineWidth',1,'Color',color);
plot([alpha_maxl],[maxlh],'o','LineWidth',1,'Color',color,'markersize',8,'MarkerFaceColor',[1 1 1]);
axis([min(alpharange1) max(alpharange1) -700 -500]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[plotsfolder,'EXAMPLE_LLH']);
close(fig);    

fig = figure('visible','off'); hold on; %
rf = relhist(alphawalk,alpharange2);
% $$$ leftind = find(alpharange2>=p5);
% $$$ leftind = leftind(1);
% $$$ rightind = find(alpharange2>=p95);
% $$$ rightind = rightind(1);
% $$$ myupdownfill(alpharange2(leftind:rightind),zeros(size(leftind:rightind)),rf(leftind:rightind),color,0.2,0);%
plot([alpha_maxl,alpha_maxl],[0,max(rf)],':','LineWidth',1,'Color',color);
da = (alpharange2(2)-alpharange2(1))/2;
plot(alpharange2+da,rf,'-','LineWidth',2,'Color',color);
plot([alpha_maxl],[max(rf)-0.002],'o','LineWidth',1,'Color',color,'markersize',8,'MarkerFaceColor',[1 1 1]);
ylim([0 0.1]);
xlim([0 0.05]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[plotsfolder,'EXAMPLE_PDF']);
close(fig);    

tvec = linspace(0,100,100);
pdf = alpha_maxl.*exp(-alpha_maxl.*tvec);
fig = figure('visible','off');
plot(tvec,pdf,'LineWidth',2);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[plotsfolder,'EXAMPLE_EXPDIST']);
close(fig);    