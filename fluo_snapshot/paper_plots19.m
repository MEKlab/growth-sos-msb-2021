
load('mmc_data/mmc_fig3_data.mat');
organize_data2;
outfolder = 'paperfigs19/';
%% ================ Ref SOS  ================ 
% Compute reference value in SOS
sn = 2; % spontaneous
refsoss = zeros(4,1);
for g = [1,2,3,4]
    s       = sn;
    dat     = data{g,s};
    nr      = length(dat.careas);
    mets5   = [];
    for r = 1:nr
        vals      = dat.ctfluos{r}(:,1)./dat.careas{r};
        mets5(r)  = mean(vals);
    end
    refsoss(g) = mean(mets5);
end
refsos = mean(refsoss([1,2,4])); % reference SOS (spontaneous)
% Binning for SOS
lbins = linspace(-1,4,50);


%% ================ FIG 3 (model fits) ================ 
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)';...
             gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)';...
             gratesEA(:,8)'];

cut  = [1:30]; % a.u. SOS thresholds
mss1 = cell(4,7,length(cut)); % 
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets3 = zeros(nr,length(cut));
        csfs  = zeros(nr,length(lbins));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                mets3(r,nc) = length(find(vals>=cut(nc)))./length(vals);
            end
            vals  = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            for c = 1:length(lbins)
                csfs(r,c) = (length(find(vals>=lbins(c)))./length(vals));
            end
        end
        for nc = 1:length(cut)
            mss1{g,sc,nc} = mets3(:,nc);
        end
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
    end
end

%% ================ FIG 3  (cumulative histograms) ================ 
for sc = 4
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,6]);
    axis([log(5) log(17) 0 0.14]);
    ha2 = axes('Units','Pixels','Position',[300+50,190+50,150,150]); hold on;
    smartlogticks(0,40,[0,1/30],6,5,1,[1:2,6]);
    axis([log(0.5) log(40) 0 1]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_2PAL_F2_BATCH']);
    close(fig);
end


%% ================ FIG 3  (expected f2 / alpha and beta) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,5,6]);
axis([log(5) log(20) 0 0.14]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                if(sosthrvec(nthr)<13)
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),betas(nthr),grates(nthr));
                else
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),median(betas),grates(nthr));
                end
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_MCC_alpha_beta']);
close(fig);

%% ================ FIG 3  (expected f2 / alpha and beta) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,5,6]);
axis([log(5) log(20) 0 0.14]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                f2_vals(nr,nthr) = predicted_f2(alphas(nthr),0,grates(nthr));
            end
            
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_MCC_alpha']);
close(fig);


%% ================ FIG 3  (actual v/s predicted ) ================ 
cut = sosthrvec;
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs = zeros(nr,length(cut));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                csfs(r,nc) = (length(find(vals>=cut(nc)))./length(vals));
            end
        end
        mss4{g,sc} = mean(csfs);
        ers4{g,sc} = std(csfs)./sqrt(nr);
    end
end

fig = figure();
ha1 = axes(); hold on;
sc = 4;
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                if(sosthrvec(nthr)<13)
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),betas(nthr),grates(nthr));
                else
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),median(betas),grates(nthr));
                end
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    ms2 = mss4{nm2g(nm),sc}; % 2pal
    axes(ha1); hold on;
    plot(ms2,ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
axis([0 0.14 0 0.14]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_BATCH-VS-PRED_ALPHA_BETA']);
close(fig);


fig = figure();
ha1 = axes(); hold on;
sc = 4;
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                f2_vals(nr,nthr) = predicted_f2(alphas(nthr),0,grates(nthr));
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    ms2 = mss4{nm2g(nm),sc}; % 2pal
    axes(ha1); hold on;
    plot(ms2,ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
axis([0 0.14 0 0.14]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_BATCH-VS-PRED_ALPHA']);
close(fig);


%% ================ FIG 3  (beta estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 0.08]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    pos = find(sosthrvec<13);
    betas_vals = zeros(size(sosthrvec(pos)));
    nvals = 0;
    for nr = 1:3
        betas = beta_mat{ns,nm}{nr}(pos);
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            betas_vals(nr,:) = betas;
        end
    end
    ms = mean(betas_vals);
    err = std(betas_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec(pos))',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec(pos)),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_BETA_MCC']);
close(fig);


%% ================ FIG 3  (alpha estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 0.04]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    alphas_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            alphas_vals(nr,:) = alphas;
        end
    end
    ms = mean(alphas_vals);
    err = std(alphas_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_ALPHA_MCC']);
close(fig);

%% ================ FIG 3  (lambda1 estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 2]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    grates_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            grates_vals(nr,:) = lambda1_mat{ns,nm}{nr};
        end
    end
    ms = mean(grates_vals);
    err = std(grates_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_LAM1_MCC']);
close(fig);

%% ================ FIG 3  (grates) ================ 
% wt, 2pal
grates    = [gratesA([1,2,4],1)';...
             gratesA([1,2,4],7)'];
gratesE   = [gratesEA([1,2,4],1)';...
             gratesEA([1,2,4],7)'];
grates2    = zeros(size(grates));
gratesE2   = zeros(size(gratesE));
for ns = 1:2
    for nm = 1:3
        vals = [];
        nvals = 0;
        for nr = 1:3
            grate = popgrate_array(ns,nm,nr)/log(2);
            if(grate>0)
                nvals = nvals+1;
                vals = [vals,grate];
            end
        end
        grates2(ns,nm) = mean(vals);
        gratesE2(ns,nm) = std(vals)/sqrt(nvals);
    end
end
linespecs = {'o','d'};
nm2g = [1,2,4];
fig = figure(); hold on;
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
for ns = 1:2
    for nm = 1:3
        errorcross(grates(ns,nm),grates2(ns,nm),gratesE(ns,nm),gratesE2(ns,nm),0.2*[1,1,1],1,[0.02,.02]);
        plot(grates(ns,nm),grates2(ns,nm),linespecs{ns},...
             'MarkerSize',10,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(nm2g(nm),:));  
    end
end
axis([0 2 0 2]);
xticks([0,0.5,1,1.5,2]);
yticks([0,0.5,1,1.5,2]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_GRATE_COMPARISON']);
close(fig);
