organize_data2;
outfile = 'paperfigs17_values.csv';

valuess = {'','','','','','','','','','','',''};
values0 = {'','','','','','','','','','','',''};
strids  = {'eSJR206','eSJR301','eSJR214','eSJR302','eSJR206','eSJR206','eSJR206','eSJR400'};
strs    = {'wt','r-pal','l-pal','2-pal','wt','wt','wt','dlexA'};
cipros  = {'0','0','0','0','1','2','3','0'};

%% ================ Growth rates ================ 

metric = 'doubling rate [dlb/hr]';
grates = [gratesA(:,1)';...
          gratesA(:,6)';...
          gratesA(:,5)';...
          gratesA(:,7)';...
          gratesA(:,2)';...
          gratesA(:,3)';...
          gratesA(:,4)';...
          gratesA(:,8)'];
gratesE = [gratesEA(:,1)';...
           gratesEA(:,6)';...
           gratesEA(:,5)';...
           gratesEA(:,7)';...
           gratesEA(:,2)';...
           gratesEA(:,3)';...
           gratesEA(:,4)';...
           gratesEA(:,8)'];

for sc = 1:8
    values = {metric,strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        ms  = grates(sc,g);
        err = gratesE(sc,g);
        s   = getsig(err);
        values{g+4}   = num2str(round(ms,-1*s));
        values{g+4+4} = num2str(round(err,-1*s));
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

%% ================ Related to fig1 and fig2 ================ 

% Compute reference values
sn = 2; % spontaneous
refsoss = zeros(4,1);
for g = [1,2,3,4]
    s       = sn;
    dat     = data{g,s};
    nr      = length(dat.careas);
    mets5   = [];
    for r = 1:nr
        vals      = dat.ctfluos{r}(:,1)./dat.careas{r};
        mets5(r)  = mean(vals);
    end
    refsoss(g) = mean(mets5);
end
refsos = mean(refsoss([1,2,4])); % reference SOS (spontaneous)
refcnt = zeros(4,1);
refsize = zeros(4,1);
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = dat.careas{r};
            mets5(r)  = mean(vals);
            vals      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets4(r)  = mean(vals);
        end
        % calculate reference values
        refcnt(g)  = mean(mets4);
        refsize(g) = mean(mets5);
    end
end

nvals = size(valuess,1);
valuess(nvals+1,:) = {'Reference PsfiA-GFP',refsos,'','','','','','','','','',''};
nvals = size(valuess,1);
valuess(nvals+1,:) = {'Reference PtetO-mKate',refcnt(4),'','','','','','','','','',''};

strids  = {'eSJR203','eSJR292','eSJR206','eSJR301','eSJR214','eSJR302','eSJR206','eSJR206','eSJR206','eSJR400'};
strs    = {'wt-auto','lexA3','wt','r-pal','l-pal','2-pal','wt','wt','wt','dlexA'};
cipros  = {'0','0','0','0','0','0','1','2','3','0'};
mss1 = zeros(4,10); % number of repeats
mss2 = cell(4,10);  % number of cells
% computed values
mss3 = cell(4,10); % mean value below pcut percentile
mss4 = cell(4,10); % mean value above pcut percentile
mss5 = cell(4,10); % total mean value
mss6 = cell(4,10); % mode value
mss14 = cell(4,10); % mean constitutive
mss15 = cell(4,10); % mean cell size

% Compute values
sc = 0;
% order: auto, wt, lexA3, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3, dlexA
for sn = [1,3,2,9,8,10,5,6,7,4]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas); % all repeats
        mets2 = zeros(nr,1);
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        mets6 = zeros(nr,1);
        mets7 = zeros(nr,1);
        mets8 = zeros(nr,1);
        mets9 = zeros(nr,1);
        mets10 = zeros(nr,1);
        mets11 = zeros(nr,1);
        mets12 = zeros(nr,1);
        mets13 = zeros(nr,1);
        mets14 = zeros(nr,1);
        mets15 = zeros(nr,1);
        for r = 1:nr
            if(sc<4)
                pcut = 99;
            else
                pcut = 85;
            end
            vals      = (dat.ctfluos{r}(:,1)./dat.careas{r});
            % number of cells
            mets2(r)  = length(vals);
            % total mean SOS 
            mets5(r)  = mean(vals);
            % mean value below pcut percentile
            mets3(r)  = mean(vals(find(vals<=prctile(vals,pcut))));
            % mean value above pcut percentile
            mets4(r)  = mean(vals(find(vals>=prctile(vals,pcut))));
            % mode SOS
            mets6(r)  = mymode(vals,5,0.1);
            % total mean constitutive
            vals2      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets14(r)  = mean(vals2);
            % total mean size
            vals3      = dat.careas{r}.*(0.16^2);
            mets15(r)  = mean(vals3);
        end
        mss1(g,sc) = nr;
        mss2{g,sc} = mets2; % number of cells
        mss3{g,sc} = mets3; % mean value below pcut percentile
        mss4{g,sc} = mets4; % mean value above pcut percentile
        mss5{g,sc} = mets5; % total mean value
        mss6{g,sc} = mets6;
        mss14{g,sc} = mets14;
        mss15{g,sc} = mets15;
    end
end

% sample size
for sc = 1:10
    values = {'Repeats / sample size',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        ms  = mss1(g,sc);
        %err = gratesE(sc,g);
        %s   = getsig(err);
        values{g+4}   = num2str(ms);
        nrvec = mss2{g,sc};
        nr    = length(nrvec);
        str   = num2str(nrvec(1));
        for n = 2:nr
            str = [str,' ',num2str(nrvec(n))];
        end
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% mean SOS
for sc = 1:10
    values = {'Mean PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        nrvec = mss5{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1)));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n)))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% mode SOS
for sc = 1:10
    values = {'Mode PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        nrvec = mss6{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1)));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n)))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% bottom mean SOS
for sc = 1:10
    if(sc<4)
        values = {'Bottom 99 PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    else
        values = {'Bottom 85 PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    end
    for g = [1,2,3,4]
        nrvec = mss3{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1)));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n)))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% top mean SOS
for sc = 1:10
    if(sc<4)
        values = {'Top 1 PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    else
        values = {'Top 15 PsfiA-GFP [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    end
    for g = [1,2,3,4]
        nrvec = mss4{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1)));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n)))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% mean constitutive
for sc = 1:10
    values = {'Mean PtetO-mKate [a.u.]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        nrvec = mss14{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1)));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n)))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;

% mean cell size
for sc = 1:10
    values = {'Mean cell size [um^2]',strids{sc},strs{sc},cipros{sc},'','','','','','','',''};
    for g = [1,2,3,4]
        nrvec = mss15{g,sc};
        nr    = length(nrvec);
        str   = num2str(round(nrvec(1),2));
        for n = 2:nr
            str = [str,' ',num2str(round(nrvec(n),2))];
        end
        values{g+4}   = str;
        values{g+4+4} = str;
    end
    nvals = size(valuess,1);
    valuess(nvals+1,:) = values;
end
nvals = size(valuess,1);
valuess(nvals+1,:) = values0;


stream = fopen(outfile,'w');
for l = 1:(size(valuess,1))
    fprintf(stream,'%s',valuess{l,1});
    for c = 2:(size(valuess,2))
        fprintf(stream,',%s',valuess{l,c});
    end
    fprintf(stream,'\n');
end
