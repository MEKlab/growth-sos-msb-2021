function [] = smartlogticks(minv,maxv,ysize,nshort,longbase,force1,totlong)
    
short = [];
long  = minv:longbase:maxv;
longticks  = [];
longlabels = {};
for nl = 1:length(long)
    l = long(nl);
    longticks(nl)  = log(l);
    longlabels{nl} = num2str(l);
    if(nl<length(long))
        short = [short,linspace(l,long(nl+1),nshort)];
    end
end
if(force1>0)
    longticks(1)  = log(force1);
    longlabels{1} = num2str(force1);
end
if(length(totlong)>1)
    longticks  = longticks(totlong);
    longlabels = longlabels(totlong);
elseif(totlong>0)
    %longticks  = longticks(1:totlong);
    %ns = floor(linspace(1,length(longlabels),totlong));
    %longticks  = longticks(ns);
    longlabels = longlabels(1:totlong);
end
xticks(longticks);
xticklabels(longlabels);
%%short = linspace(minv,maxv,nshort); %minv:maxv;%
for s = short
    plot([log(s),log(s)],[ysize(1),ysize(2)],'-','color','black','LineWidth',0.5);
end

end