
load('mmc_data/mmc_fig3_data.mat');
organize_data2;
outfolder = 'paperfigs20/';
%% ================ Ref SOS  ================ 
% Compute reference value in SOS
sn = 2; % spontaneous
refsoss = zeros(4,1);
for g = [1,2,3,4]
    s       = sn;
    dat     = data{g,s};
    nr      = length(dat.careas);
    mets5   = [];
    for r = 1:nr
        vals      = dat.ctfluos{r}(:,1)./dat.careas{r};
        mets5(r)  = mean(vals);
    end
    refsoss(g) = mean(mets5);
end
refsos = mean(refsoss([1,2,4])); % reference SOS (spontaneous)

% Binning for SOS
lbins = linspace(-1,5,50);

%% ================ FIG 1  ================ 
% Order: spontaneous, lex3, autofluo, delta lexA
grates    = [gratesA(:,1)';gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';gratesEA(:,8)'];
linespecs = {'o','h','p','s'};
pcut = 99;
% histograms
mss1 = cell(4,3); % log hist
ers1 = cell(4,3); 
mss2 = cell(4,3); % log cum hist
ers2 = cell(4,3); 
% computed values
mss3 = zeros(4,3); % mean value below pcut percentile
ers3 = zeros(4,3); 
mss4 = zeros(4,3); % mean value above pcut percentile
ers4 = zeros(4,3); 
mss5 = zeros(4,3); % total mean value
ers5 = zeros(4,3); 
% Compute histograms and values
sc = 0;
for sn = [2,3,1,4] % spontaneous, lex3, autofluo, delta lexA
    sc = sc+1;
    for g = 1:4 % growth rate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas); % all repeats
        csfs  = zeros(nr,length(lbins));
        sfs   = zeros(nr,length(lbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            sfs(r,:)  = 100*relhist(vals,lbins); % relative histogram
            for c = 1:length(lbins) % cumulative histogram
                csfs(r,c) = (length(find(vals>=lbins(c))))./length(vals);
            end
            % mean value below pcut percentile
            mets3(r)  = mean(exp(vals(find(vals<=prctile(vals,pcut)))));
            % mean value above pcut percentile
            mets4(r)  = mean(exp(vals(find(vals>=prctile(vals,pcut)))));
            % total mean value
            mets5(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
        mss5(g,sc) = mean(mets5);
    end
end


%% ================ SUPP FIG 1 (relative histograms)  ================ 
for sc = 4
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(10,60,[0,1],60,10,10,0);
    axis([log(10) log(80) 0 100]);
    %ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    %smartlogticks(20,100,[0,0.002],80,20,1,3);
    %xticks(log([1,4,8,16]));
    %xticklabels({'1','4','8','16'});
    %axis([log(20) log(100) 0 0.1]);
    %set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        %axes(ha2); hold on;
        %myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        %plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG1_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 1 (cumulative histograms)  ================ 
for sc = 4
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(10,60,[0,0.01/40],60,10,10,0);
    axis([log(10) log(80) 0 1]);
    %ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    %smartlogticks(0,32,[0,0.01/40],9,8,1,3);
    %xticks(log([1,4,8,16]));
    %xticklabels({'1','4','8','16'});
    %axis([log(1) log(32) 0 0.01]);
    %smartlogticksY(0,100,log([0.5,0.52]),11,10,0.1,1);
    %yticks(log([0.1,1,10,100]));
    %yticklabels({'0.1','1','10','100'});
    for g = [1,2,4]
        ms = mss2{g,sc};
        err = ers2{g,sc};
        pos = find(isinf(ms)==0);
        ms  = ms(pos);
        err = err(pos);
        axes(ha1); hold on;
        myupdownfill(lbins(pos)',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins(pos),ms,'LineWidth',2,'Color',colors(g,:));
        %axes(ha2); hold on;
        %myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        %plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG1_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 1 (mean below percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 2.5]);
xticks([0.5,1,1.5,2]);
yticks([0,0.5,1,1.5,2,2.5]);
ha2 = axes('Units','Pixels','Position',[300+50,190+50,150,150]); hold on;
axis([0.3 2 0 50]);
xticks([0.5,1,1.5,2]);
yticks([0,10,20,30,40,50]);
for sc = 1:4
    for g = [1,2,4]%1:4
        ms = mss3(g,sc);
        err = ers3(g,sc);
        axes(ha1); hold on;
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
        axes(ha2); hold on;
        if(sc>3)
            errorcross(grates(2,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(2,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:)); 
        end
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_C']);
close(fig);

%% ================ SUPP FIG 1 (mean above percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 8]);
xticks([0.5,1,1.5,2]);
yticks([0,2,4,6,8]);
ha2 = axes('Units','Pixels','Position',[300+50,190+50,150,150]); hold on;
axis([0.3 2 0 100]);
xticks([0.5,1,1.5,2]);
yticks([0,25,50,75,100]);
for sc = 1:4
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
                axes(ha2); hold on;
        if(sc>3)
            errorcross(grates(2,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(2,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:)); 
        end
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_D']);
close(fig);

%% ================ EXTRA SUPP FIGURES (constitutive) ================ 
cbins = linspace(-1,2,50); % const log bins
refcnt = zeros(4,1); % reference
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets5(r)  = mean(vals);
        end
        refcnt(g) = mean(mets5);
    end
end
% Order: spontaneous, lex3, autofluo, delta lexA
grates    = [gratesA(:,1)';gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';gratesEA(:,8)'];
linespecs = {'o','h','p','s'};
pcut = 99;
% histograms
mss1 = cell(4,3); % log hist
ers1 = cell(4,3); 
mss2 = cell(4,3); % log cum hist
ers2 = cell(4,3); 
% computed values
mss3 = zeros(4,3); % mean value below pcut percentile
ers3 = zeros(4,3); 
mss4 = zeros(4,3); % mean value above pcut percentile
ers4 = zeros(4,3); 
mss5 = zeros(4,3); % total mean value
ers5 = zeros(4,3); 
sc = 0;
for sn = [2,3,1,4]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs  = zeros(nr,length(cbins));
        sfs   = zeros(nr,length(cbins));
        sfs2  = zeros(nr,length(cbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,2)./dat.careas{r})./refcnt(4));
            sfs(r,:)  = 100*relhist(vals,cbins);
            % mean value below pcut percentile
            mets3(r)  = mean(exp(vals(find(vals<=prctile(vals,pcut)))));
            % mean value above pcut percentile
            mets4(r)  = mean(exp(vals(find(vals>=prctile(vals,pcut)))));
            % total mean value
            mets5(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(sfs2);
        ers2{g,sc} = std(sfs2)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
    end
end

fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 5]);
xticks([0.5,1,1.5,2]);
yticks([0,1,2,3,4,5]);
for sc = 1:4
    for g = [1,2,4]%1:4
        ms = mss3(g,sc);
        err = ers3(g,sc);
        axes(ha1); hold on;
        if(sc>3)
            errorcross(grates(2,g),ms,gratesE(2,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(2,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));      
        else
            errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(1,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));      
        end
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_F']);
close(fig);

fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 5]);
xticks([0.5,1,1.5,2]);
yticks([0,1,2,3,4,5]);
for sc = 1:4
    for g = [1,2,4]%1:4
        ms = mss4(g,sc);
        err = ers4(g,sc);
        axes(ha1); hold on;
        if(sc>3)
            errorcross(grates(2,g),ms,gratesE(2,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(2,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));      
        else
            errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
            plot(grates(1,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));      
        end
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_G']);
close(fig);

%% ================ EXTRA SUPP FIGURES (constitutive) ================ 
% order: spontaneous, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3,delta lexA
cbins = linspace(-1,2,50); % const log bins
refcnt = zeros(4,1); % reference
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets5(r)  = mean(vals);
        end
        refcnt(g) = mean(mets5);
    end
end
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)'];
linespecs = {'h','>','<','d','v','^','s'};
pcut = 85; % percentile threshold
% histograms
mss1 = cell(4,8); % log hist
ers1 = cell(4,8); 
mss2 = cell(4,8); % log cum hist
ers2 = cell(4,8); 
% computed values
mss3 = zeros(4,8); % mean value below pcut percentile
ers3 = zeros(4,8); 
mss4 = zeros(4,8); % mean value above pcut percentile
ers4 = zeros(4,8); 
mss5 = zeros(4,8); % total mean value
ers5 = zeros(4,8); 
% Compute histograms and values
sc = 0;
for sn = [3,9,8,10,5,6,7]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs  = zeros(nr,length(cbins));
        sfs   = zeros(nr,length(cbins));
        sfs2  = zeros(nr,length(cbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,2)./dat.careas{r})./refcnt(4));
            sfs2(r,:) = 100*relhist(vals,cbins);
            mets4(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(sfs2);
        ers2{g,sc} = std(sfs2)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
    end
end

fig = figure();
ha1 = axes(); hold on;
%for sc = [1,5,6,7]%1:3
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 4]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_C_CONDITION_Chronic']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
%for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 4]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_C_CONDITION_Cipro']);
close(fig);


%% ================ FIG 3 (model fits) ================ 
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)';...
             gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)';...
             gratesEA(:,8)'];

cut  = [1:30]; % a.u. SOS thresholds
mss1 = cell(4,7,length(cut)); % 
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets3 = zeros(nr,length(cut));
        csfs  = zeros(nr,length(lbins));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                mets3(r,nc) = length(find(vals>=cut(nc)))./length(vals);
            end
            vals  = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            for c = 1:length(lbins)
                csfs(r,c) = (length(find(vals>=lbins(c)))./length(vals));
            end
        end
        for nc = 1:length(cut)
            mss1{g,sc,nc} = mets3(:,nc);
        end
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
    end
end

%% ================ FIG 3  (cumulative histograms) ================ 
for sc = 4
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,6]);
    axis([log(5) log(17) 0 0.14]);
    ha2 = axes('Units','Pixels','Position',[300+50,190+50,150,150]); hold on;
    smartlogticks(0,40,[0,1/30],6,5,1,[1:2,6]);
    axis([log(0.5) log(40) 0 1]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_2PAL_F2_BATCH']);
    close(fig);
end


%% ================ FIG 3  (expected f2 / alpha and beta) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,5,6]);
axis([log(5) log(20) 0 0.14]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                if(sosthrvec(nthr)<13)
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),betas(nthr),grates(nthr));
                else
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),median(betas),grates(nthr));
                end
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_MCC_alpha_beta']);
close(fig);

%% ================ FIG 3  (expected f2 / alpha and beta) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.14/30],6,5,1,[2,3,4,5,6]);
axis([log(5) log(20) 0 0.14]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                f2_vals(nr,nthr) = predicted_f2(alphas(nthr),0,grates(nthr));
            end
            
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_MCC_alpha']);
close(fig);


%% ================ FIG 3  (actual v/s predicted ) ================ 
cut = sosthrvec;
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs = zeros(nr,length(cut));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                csfs(r,nc) = (length(find(vals>=cut(nc)))./length(vals));
            end
        end
        mss4{g,sc} = mean(csfs);
        ers4{g,sc} = std(csfs)./sqrt(nr);
    end
end

fig = figure();
ha1 = axes(); hold on;
sc = 4;
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                if(sosthrvec(nthr)<13)
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),betas(nthr),grates(nthr));
                else
                    f2_vals(nr,nthr) = predicted_f2(alphas(nthr),median(betas),grates(nthr));
                end
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    ms2 = mss4{nm2g(nm),sc}; % 2pal
    axes(ha1); hold on;
    plot(ms2,ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
axis([0 0.14 0 0.14]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_BATCH-VS-PRED_ALPHA_BETA']);
close(fig);


fig = figure();
ha1 = axes(); hold on;
sc = 4;
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    f2_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        betas = beta_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        grates = lambda1_mat{ns,nm}{nr};
        if(grate>0)
            nvals = nvals+1;
            for nthr = 1:length(sosthrvec)
                f2_vals(nr,nthr) = predicted_f2(alphas(nthr),0,grates(nthr));
            end
        end
    end
    ms = mean(f2_vals);
    err = std(f2_vals)/sqrt(nvals);
    ms2 = mss4{nm2g(nm),sc}; % 2pal
    axes(ha1); hold on;
    plot(ms2,ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
axis([0 0.14 0 0.14]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_F2_BATCH-VS-PRED_ALPHA']);
close(fig);


%% ================ FIG 3  (beta estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 0.08]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    pos = find(sosthrvec<13);
    betas_vals = zeros(size(sosthrvec(pos)));
    nvals = 0;
    for nr = 1:3
        betas = beta_mat{ns,nm}{nr}(pos);
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            betas_vals(nr,:) = betas;
        end
    end
    ms = mean(betas_vals);
    err = std(betas_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec(pos))',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec(pos)),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_BETA_MCC']);
close(fig);


%% ================ FIG 3  (alpha estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 0.04]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    alphas_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        alphas = alpha_mat{ns,nm}{nr};
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            alphas_vals(nr,:) = alphas;
        end
    end
    ms = mean(alphas_vals);
    err = std(alphas_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_ALPHA_MCC']);
close(fig);

%% ================ FIG 3  (lambda1 estimations) ================ 
fig = figure();
ha1 = axes(); hold on;
smartlogticks(0,40,[0,0.04/30],6,5,1,[2,3,4,6]);
axis([log(5) log(20) 0 2]);
ns = 2;
nm2g = [1,2,4];
for nm = 1:3
    grates_vals = zeros(size(sosthrvec));
    nvals = 0;
    for nr = 1:3
        grate = popgrate_array(ns,nm,nr);
        if(grate>0)
            nvals = nvals+1;
            grates_vals(nr,:) = lambda1_mat{ns,nm}{nr};
        end
    end
    ms = mean(grates_vals);
    err = std(grates_vals)/sqrt(nvals);
    axes(ha1); hold on;
    myupdownfill(log(sosthrvec)',[ms+err]',[ms-err]',colors(nm2g(nm),:),0.3,0);%
    plot(log(sosthrvec),ms,'x:','LineWidth',2,'Color',colors(nm2g(nm),:));
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_2PAL_LAM1_MCC']);
close(fig);

%% ================ FIG 3  (grates) ================ 
% wt, 2pal
grates    = [gratesA([1,2,4],1)';...
             gratesA([1,2,4],7)'];
gratesE   = [gratesEA([1,2,4],1)';...
             gratesEA([1,2,4],7)'];
grates2    = zeros(size(grates));
gratesE2   = zeros(size(gratesE));
for ns = 1:2
    for nm = 1:3
        vals = [];
        nvals = 0;
        for nr = 1:3
            grate = popgrate_array(ns,nm,nr)/log(2);
            if(grate>0)
                nvals = nvals+1;
                vals = [vals,grate];
            end
        end
        grates2(ns,nm) = mean(vals);
        gratesE2(ns,nm) = std(vals)/sqrt(nvals);
    end
end
linespecs = {'o','d'};
nm2g = [1,2,4];
fig = figure(); hold on;
plot([0,2],[0,2],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
for ns = 1:2
    for nm = 1:3
        errorcross(grates(ns,nm),grates2(ns,nm),gratesE(ns,nm),gratesE2(ns,nm),0.2*[1,1,1],1,[0.02,.02]);
        plot(grates(ns,nm),grates2(ns,nm),linespecs{ns},...
             'MarkerSize',10,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(nm2g(nm),:));  
    end
end
axis([0 2 0 2]);
xticks([0,0.5,1,1.5,2]);
yticks([0,0.5,1,1.5,2]);
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'FIG3_GRATE_COMPARISON']);
close(fig);
