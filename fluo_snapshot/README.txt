Segmentation and quantification data for agar-pad ("snapshot" data) experiments are located in "/segmentfiles". 
The name indicates which strain, medium, and condition each file corresponds to.

Scripts can be run as follows:

for quantification
quantifyall3.m

for organising data
organize_data2.m

for plotting
paper_plots16.m and paper_plots20.m

for printing values
paper_plots17_values.m

