function [f2] = predicted_f2(alpha, beta, lambda1)

if((beta + alpha - lambda1).^2 + 4.*beta.*lambda1<=0)
    error('Irrational roots to the f2 equation');
end

disc = sqrt((beta + alpha - lambda1).^2 + 4.*beta.*lambda1);

% decide whether we want the plus or minus option
op1 = (alpha + beta + disc)./lambda1;
op2 = (alpha + beta - disc)./lambda1;
% calculate both
f2s = [0.5.*(1+op1),0.5.*(1+op2)];
goods = [0,0];
mindelta = 1e-12; % to avoid numerica issues
for n = 1:2
    if((f2s(n)>mindelta) && (f2s(n)<(1-mindelta)))
        goods(n) = 1;
    end
end

if(sum(goods)==2)
    error('Two valid values for f2 were found');
elseif(sum(goods)==0)
    error('No valid values for f2 were found');
else
    [~,ind] = max(goods);
    f2 = f2s(ind);
end
    
end