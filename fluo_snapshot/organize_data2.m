%% 

% path to segment scripts
addpath(genpath('~/2019_SOSpaper/matlab_utils/Segment/src/'));
% path to figure export scripts
addpath('~/2019_SOSpaper/matlab_utils/fig_export/');

% =========== ALL QUANTIFICATION FILES =========== 
folder = ['./'];
fileprefs = {};

% rows: 1 gly, 2 glu, 3 glyaa, 4 gluaa
fileprefs = {};

% col 1: 203 (auto fluorescence control)
fileprefs{1,1} = {'20181202_Gluy_e203','20181216_Gly_e203','20190113_Gly_e203','20190223_Gly_e203'};
fileprefs{2,1} = {'20181130_Glu_203_A','20190216_Glu_203','20190223_Glu_e203'};
fileprefs{3,1} = {'20181201_Glyaa_e203','20190223_Glyaa_e203','20190216_Glyaa_203'};
fileprefs{4,1} = {'20181202_Gluaa_e203','20181216_Gluaa_e203','20190114_Gluaa_e203'};
% col 2: 206 (Spontaneous SOS)
fileprefs{1,2} = {'20180920_e206_Gly',...
                  '20181001_e206_Gly',...
                  '20181008_e206_Gly',...
                  '20181010_e206_Gly',...
                  '20181202_Gluy_e206',...
                  '20181216_Gly_e206',...
                  '20190113_Gly_e206',...
                  '20190223_Gly_e206'};
fileprefs{2,2} = {'20180920_e206_Glu',...
                  '20181001_e206_Glu',...
                  '20181002_e206_Glu',...
                  '20181010_e206_Glu',...
                  '20181130_Glu_206_A',...
                  '20181130_Glu_206_B',...
                  '20181130_Glu_206_C',...
                  '20190216_Glu_206',...
                  '20190223_Glu_e206'};
fileprefs{3,2} = {'20181028_Glyaa_e206_cipro0',...
                  '20181103_Glyaa_e206_cipro0',...
                  '20181201_Glyaa_e206',...
                  '20190223_Glyaa_e206',...
                  '20190216_Glyaa_206'};
fileprefs{4,2} = {'20180920_e206_Gluaa',...
                  '20181001_e206_Gluaa',...
                  '20181202_Gluaa_e206',...
                  '20181216_Gluaa_e206',...
                  '20190114_Gluaa_e206',...
                  '20190125_Gluaa_e206',...
                  '20190216_Gluaa_206'};
% col 3: 292 (lexA3)
fileprefs{1,3} = {'20180920_e292_Gly',...
                  '20181001_e292_Gly',...
                  '20181002_e292_Gly'};
fileprefs{2,3} = {'20180920_e292_Glu',...
                  '20181001_e292_Glu',...
                  '20181002_e292_Glu'};
fileprefs{3,3} = {'20181028_Glyaa_e292',...
                  '20181103_Glyaa_e292',...
                  '20181201_Glyaa_e292'};
fileprefs{4,3} = {'20180920_e292_Gluaa',...
                  '20181002_e292_Gluaa',...
                  '20190114_Gluaa_e292'};
% col 4: 400 (delta lexA delta sulA)
fileprefs{1,4} = {'20181202_Gluy_e400',...
                  '20181216_Gly_e400',...
                  '20190113_Gly_e400'};
fileprefs{2,4} = {'20181130_Glu_400_A',...
                  '20181130_Glu_400_B',...
                  '20181130_Glu_400_C'};
fileprefs{3,4} = {'20181201_Glyaa_e400_A',...
                  '20181201_Glyaa_e400_B',...
                  '20181201_Glyaa_e400_C'};
fileprefs{4,4} = {'20181202_Gluaa_e400',...
                  '20181216_Gluaa_e400',...
                  '20190114_Gluaa_e400'};
% col 5: 206 + cipro 1 ng/ml
fileprefs{1,5} = {'20181216_Gly_e206_cipro1',...
                  '20190113_Gly_e206_cipro1',...
                  '20190223_Gly_e206_cipro1'};
fileprefs{2,5} = {'20181130_Glu_206_cipro1_B',...
                  '20181130_Glu_206_cipro1_C',...
                  '20190216_Glu_206_cipro1'};
fileprefs{3,5} = {'20181028_Glyaa_e206_cipro1',...
                  '20181103_Glyaa_e206_cipro1',...
                  '20181201_Glyaa_e206_cipro1'};
fileprefs{4,5} = {'20181216_Gluaa_e206_cipro1',...
                  '20190114_Gluaa_e206_cipro1',...
                  '20190125_Gluaa_e206_cipro1',...
                  '20190216_Gluaa_206_cipro1'};
% col 6: 206 + cipro 2 ng/ml
fileprefs{1,6} = {'20181216_Gly_e206_cipro2',...
                  '20190113_Gly_e206_cipro2',...
                  '20190223_Gly_e206_cipro2'};
fileprefs{2,6} = {'20181130_Glu_206_cipro2_B',...
                  '20181130_Glu_206_cipro2_C',...
                  '20190216_Glu_206_cipro2'};
fileprefs{3,6} = {'20181028_Glyaa_e206_cipro2',...
                  '20181103_Glyaa_e206_cipro2',...
                  '20181201_Glyaa_e206_cipro2'};
fileprefs{4,6} = {'20181216_Gluaa_e206_cipro2',...
                  '20190114_Gluaa_e206_cipro2',...
                  '20190125_Gluaa_e206_Cipro2',...
                  '20190216_Gluaa_206_cipro2'};
% col 7: 206 + cipro 3 ng/ml
fileprefs{1,7} = {'20181216_Gly_e206_cipro3',...
                  '20190113_Gly_e206_cipro3',...
                  '20190223_Gly_e206_cipro3'};
fileprefs{2,7} = {'20181130_Glu_206_cipro3_B',...
                  '20181130_Glu_206_cipro3_C',...
                  '20190216_Glu_206_cipro3'};
fileprefs{3,7} = {'20181028_Glyaa_e206_cipro3',...
                  '20181103_Glyaa_e206_cipro3',...
                  '20181201_Glyaa_e206_cipro3'};
fileprefs{4,7} = {'20181216_Gluaa_e206_cipro3',...
                  '20190114_Gluaa_e206_cipro3',...
                  '20190125_Gluaa_e206_Cipro3',...
                  '20190216_Gluaa_206_cipro3'};
% col 8: 214 (206 + l-pal)
fileprefs{1,8} = {'20180920_e214_Gly',...
                  '20181002_e214_Gly',...
                  '20181010_e214_Gly'};
fileprefs{2,8} = {'20180920_e214_Glu',...
                  '20181001_e214_Glu',...
                  '20181002_e214_Glu'};
fileprefs{3,8} = {'20181028_Glyaa_e214',...
                  '20181103_Glyaa_e214',...
                  '20181201_Glyaa_e214'};
fileprefs{4,8} = {'20180920_e214_Gluaa',...
                  '20181001_e214_Gluaa',...
                  '20181002_e214_Gluaa'};
% col 9: 301 (206 + r-pal)
fileprefs{1,9} = {'20180920_e301_Gly',...
                  '20181002_e301_Gly',...
                  '20181010_e301_Gly'};
fileprefs{2,9} = {'20180920_e301_Glu',...
                  '20181001_e301_Glu',...
                  '20181002_e301_Glu'};
fileprefs{3,9} = {'20181028_Glyaa_e301',...
                  '20181103_Glyaa_e301',...
                  '20181201_Glyaa_e301'};
fileprefs{4,9} = {'20180920_e301_Gluaa',...
                  '20181001_e301_Gluaa',...
                  '20181002_e301_Gluaa'};
% col 10: 302 (206 + l-pal + r-pal)
fileprefs{1,10} = {'20180920_e302_Gly',...
                   '20181001_e302_Gly',...
                   '20190223_Gly_e302'};
fileprefs{2,10} = {'20180920_e302_Glu',...
                   '20181001_e302_Glu',...
                   '20181010_e302_Glu'};
fileprefs{3,10} = {'20181028_Glyaa_e302',...
                   '20181103_Glyaa_e302',...
                   '20181201_Glyaa_e302'};
fileprefs{4,10} = {'20180920_e302_Gluaa',...
                   '20181002_e302_Gluaa',...
                   '20181202_Gluaa_e302',...
                   '20190125_Gluaa_e302'};

% =========== Load and organize data =============
data = {};
for g = 1:4
    for s = 1:10
        files    = fileprefs{g,s};    
        careas   = {};
        clens    = {};
        ctfluos  = {}; % total
        for r = 1:length(files)
            % for each quantification file
            % display([files{r},'_quantification.mat']);
            temp = load(['segmentfiles/',files{r},'_quantification.mat']);
            careas{r} = temp.areas; % cell area vector
            clens{r}  = temp.lens;  % cell length vector
            % total fluorescence
            % From raw data, column 2: GFP, column 3: mCherry
            ctfluos{r} = [temp.tfluos(:,2),temp.tfluos(:,3)];
            % now column 1 is GFP and 2 is mCherry
        end
        % save all as a structure
        % organised by growth and strain/cipro condition
        dat = struct;
        dat.careas  = careas;
        dat.clens   = clens;
        dat.ctfluos = ctfluos;
        data{g,s} = dat;
    end
end

% =========== Load calculated doubling rates =========== 
load('grates20181212.mat');

% =========== Colours used for plotting (by growth condition) =========== 
colors       = jet(4);
colors(2,:)  = [0.4,1,0.1];
colors(3,:)  = [1,0.7,0.1];

