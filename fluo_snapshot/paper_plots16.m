organize_data2;
outfolder = 'paperfigs16/';
%% ================ Ref SOS  ================ 
% Compute reference value in SOS
sn = 2; % spontaneous
refsoss = zeros(4,1);
for g = [1,2,3,4]
    s       = sn;
    dat     = data{g,s};
    nr      = length(dat.careas);
    mets5   = [];
    for r = 1:nr
        vals      = dat.ctfluos{r}(:,1)./dat.careas{r};
        mets5(r)  = mean(vals);
    end
    refsoss(g) = mean(mets5);
end
refsos = mean(refsoss([1,2,4])); % reference SOS (spontaneous)
% Binning for SOS
lbins = linspace(-1,4,50);

%% ================ FIG 1  ================ 
% Order: spontaneous, lex3, autofluo, delta lexA
grates    = [gratesA(:,1)';gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';gratesEA(:,8)'];
linespecs = {'o','h','p','p'};
pcut = 99;
% histograms
mss1 = cell(4,3); % log hist
ers1 = cell(4,3); 
mss2 = cell(4,3); % log cum hist
ers2 = cell(4,3); 
% computed values
mss3 = zeros(4,3); % mean value below pcut percentile
ers3 = zeros(4,3); 
mss4 = zeros(4,3); % mean value above pcut percentile
ers4 = zeros(4,3); 
mss5 = zeros(4,3); % total mean value
ers5 = zeros(4,3); 
% Compute histograms and values
sc = 0;
for sn = [2,3,1,4] % spontaneous, lex3, autofluo, delta lexA
    sc = sc+1;
    for g = 1:4 % growth rate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas); % all repeats
        csfs  = zeros(nr,length(lbins));
        sfs   = zeros(nr,length(lbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            sfs(r,:)  = 100*relhist(vals,lbins); % relative histogram
            for c = 1:length(lbins) % cumulative histogram
                csfs(r,c) = (length(find(vals>=lbins(c))))./length(vals);
            end
            % mean value below pcut percentile
            mets3(r)  = mean(exp(vals(find(vals<=prctile(vals,pcut)))));
            % mean value above pcut percentile
            mets4(r)  = mean(exp(vals(find(vals>=prctile(vals,pcut)))));
            % total mean value
            mets5(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
        mss5(g,sc) = mean(mets5);
    end
end

%% ================ FIG 1 (frequency histograms)  ================ 
for sc = 1:3
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,4,[0,1],5,1,0,0); % tick
    axis([log(0.5) log(4) 0 100]);
    ha2 = axes('Units','Pixels','Position',[300,190-50,200,250]); hold on;
    smartlogticks(0,32,[0,0.002],9,8,1,3);
    xticks(log([1,4,8,16]));
    xticklabels({'1','4','8','16'});
    axis([log(1) log(32) 0 0.1]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG1_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ FIG 1 (mean below percentile)  ================ 
for sc = 1:3
    fig = figure();
    ha1 = axes(); hold on;
    axis([0.3 2 0 8]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,4,6,8]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.1]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG1_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ FIG 1 (mean above percentile)  ================ 
for sc = 1:3
    fig = figure();
    ha1 = axes(); hold on;
    axis([0.3 2 0 8]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,4,6,8]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.1]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG1_C_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 1 (relative histograms)  ================ 
for sc = 1:3
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,4,[0,1],5,1,0,0);
    axis([log(0.5) log(4) 0 100]);
    ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    smartlogticks(0,32,[0,0.002],9,8,1,3);
    xticks(log([1,4,8,16]));
    xticklabels({'1','4','8','16'});
    axis([log(1) log(32) 0 0.1]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG1_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 1 (cumulative histograms)  ================ 
for sc = 1:3
    fig = figure();
    ha1 = axes(); hold on;
    axis([log(0.5) log(32) 0 1]);
    smartlogticks(0,40,[0,1/40],5,4,1,4);
    ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    smartlogticks(0,32,[0,0.01/40],9,8,1,3);
    xticks(log([1,4,8,16]));
    xticklabels({'1','4','8','16'});
    axis([log(1) log(32) 0 0.01]);
    %smartlogticksY(0,100,log([0.5,0.52]),11,10,0.1,1);
    %yticks(log([0.1,1,10,100]));
    %yticklabels({'0.1','1','10','100'});
    for g = [1,2,4]
        ms = mss2{g,sc};
        err = ers2{g,sc};
        pos = find(isinf(ms)==0);
        ms  = ms(pos);
        err = err(pos);
        axes(ha1); hold on;
        myupdownfill(lbins(pos)',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins(pos),ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG1_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 1 (mean below percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 2]);
xticks([0.5,1,1.5,2]);
yticks([0,0.5,1,1.5,2]);
for sc = 1:3
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_C']);
close(fig);

%% ================ SUPP FIG 1 (mean above percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
axis([0.3 2 0 8]);
xticks([0.5,1,1.5,2]);
yticks([0,2,4,6,8]);
for sc = 1:3
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.01]);
        plot(grates(1,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG1_D']);
close(fig);

%% ================ FIG 2  ================ 
% order: lexA3, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3, dlexA
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)';...
             gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)';...
             gratesEA(:,8)'];
linespecs = {'h','>','<','d','v','^','s','p'};
pcut = 85; % percentile threshold
% histograms
mss1 = cell(4,8); % log hist
ers1 = cell(4,8); 
mss2 = cell(4,8); % log cum hist
ers2 = cell(4,8); 
% computed values
mss3 = zeros(4,8); % mean value below pcut percentile
ers3 = zeros(4,8); 
mss4 = zeros(4,8); % mean value above pcut percentile
ers4 = zeros(4,8); 
mss5 = zeros(4,8); % total mean value
ers5 = zeros(4,8); 
% Compute histograms and values
sc = 0;
for sn = [3,9,8,10,5,6,7,4]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas); % all repeats
        csfs  = zeros(nr,length(lbins));
        sfs   = zeros(nr,length(lbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            sfs(r,:)  = 100*relhist(vals,lbins); % relative histogram
            for c = 1:length(lbins) % cumulative histogram
                csfs(r,c) = (length(find(vals>=lbins(c))))./length(vals);
            end
            % mean value below pcut percentile
            mets3(r)  = mean(exp(vals(find(vals<=prctile(vals,pcut)))));
            % mean value above pcut percentile
            mets4(r)  = mean(exp(vals(find(vals>=prctile(vals,pcut)))));
            % total mean value
            mets5(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
        mss5(g,sc) = mean(mets5);
    end
end

%% ================ FIG 2 (relative histogram)  ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.4],6,5,1,[1:3,6]);
    axis([log(0.5) log(40) 0 40]);
    ha2 = axes('Units','Pixels','Position',[300,190-50,200,250]); hold on;
    smartlogticks(0,40,[0,0.05],6,5,1,[1:2,6]);
    axis([log(1) log(40) 0 3]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG2_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ FIG 2 (mean below percentile)  ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    axis([0.3 2 0 14]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,6,10,14]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
        ms = mss3(g,1);
        err = ers3(g,1);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(1,g),ms,linespecs{1},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG2_C_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ FIG 2 (mean above percentile)  ================ 
for sc = 1:7%1:3
    fig = figure();
    ha1 = axes(); hold on;
    axis([0.3 2 0 14]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,6,10,14]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
        ms = mss4(g,1);
        err = ers4(g,1);
        errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(1,g),ms,linespecs{1},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG2_D_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 2 (relative histogram)  ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.4],6,5,1,[1:3,6]);
    axis([log(0.5) log(40) 0 40]);
    ha2 = axes('Units','Pixels','Position',[300,190-50,200,250]); hold on;
    smartlogticks(0,40,[0,0.05],6,5,1,[1:2,6]);
    axis([log(1) log(40) 0 3]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG2_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 2 (cumulative histogram)  ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    axis([log(0.5) log(50) log(0.1) log(101)]);
    smartlogticks(0,50,log([0.1,0.11]),6,5,1,[1:3,6]);
    smartlogticksY(0,100,log([0.5,0.52]),11,10,0.1,1);
    yticks(log([0.1,1,10,100]));
    yticklabels({'0.1','1','10','100'});
    for g = 1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        pos = find(isinf(ms)==0);
        ms  = ms(pos);
        err = err(pos);
        myupdownfill(lbins(pos)',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins(pos),ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG2_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 2 (mean SOS cipro)  ================ 
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
% $$$     axis([0.3 2 0 14]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,2,6,10,14]);
    axis([0.3 2 0 5]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss5(g,sc);
        err = ers5(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_C_CONDITION_Cipro']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,2,3,4]%1:3
% $$$     axis([0.3 2 0 14]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,2,6,10,14]);
    axis([0.3 2 0 5]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss5(g,sc);
        err = ers5(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_C_CONDITION_Chronic']);
close(fig);

%% ================ SUPP FIG 2 (mean SOS below percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
% $$$     axis([0.3 2 0 14]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,2,6,10,14]);
    axis([0.3 2 0 5]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_D_CONDITION_Cipro']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,2,3,4]%1:3
% $$$     axis([0.3 2 0 14]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,2,6,10,14]);
    axis([0.3 2 0 5]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_D_CONDITION_Chronic']);
close(fig);

%% ================ SUPP FIG 2 (mean SOS above percentile)  ================ 
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
    axis([0.3 2 0 14]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,6,10,14]);
% $$$     axis([0.3 2 0 5]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_E_CONDITION_Cipro']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 14]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,6,10,14]);
% $$$     axis([0.3 2 0 5]);
% $$$     xticks([0.5,1,1.5,2]);
% $$$     yticks([0,1,2,3,4]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG2_E_CONDITION_Chronic']);
close(fig);

%% ================ FIG 3 ================ 
% order: spontaneous, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)'];
linespecs = {'o','>','<','d','v','^','s'};
cut  = [1:30]; % a.u. SOS thresholds
mss1 = cell(4,7,length(cut)); % 
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets3 = zeros(nr,length(cut));
        csfs  = zeros(nr,length(lbins));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                mets3(r,nc) = length(find(vals>=cut(nc)))./length(vals);
            end
            vals  = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            for c = 1:length(lbins)
                csfs(r,c) = (length(find(vals>=lbins(c)))./length(vals));
            end
        end
        for nc = 1:length(cut)
            mss1{g,sc,nc} = mets3(:,nc);
        end
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
    end
end

sosthr = 10;

%% ================ FIG 3  (cumulative histograms) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.4/30],6,5,1,[1:3,6]);
    axis([log(0.5) log(40) 0 1]);
    ha2 = axes('Units','Pixels','Position',[300+20,190-50,180,250]); hold on;
    smartlogticks(0,40,[0,0.05/10],6,5,1,[1:2,3,6]);
    plot(log([sosthr,sosthr]),[0,1],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
    axis([log(2) log(40) 0 0.3]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ SUPP FIG 3  (idem but without lines) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.4/30],6,5,1,[1:3,6]);
    axis([log(0.5) log(40) 0 1]);
    ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    smartlogticks(0,40,[0,0.05/10],6,5,1,[1:2,3,6]);
    %plot(log([6,6]),[0,1],'--','LineWidth',1,'Color',0.3*[1,1,1,3*0.5]);
    axis([log(2) log(40) 0 0.3]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(lbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(lbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG3_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ FIG 3 (model fits) ================ 
cvec = [4:15]; % list of thresholds
for nc = cvec
    for sc = 1:7
        fig = figure();
        ha1 = axes(); hold on;
        yvals = [];
        xvals = [];
        for g = [1,2,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            % get the inverse of the growth rate without additional damage
            xvals = [xvals;1./(log(2).*grates(1,g).*ones(size(vals)))];
        end
        % linear fit (force intercept to 0)
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;
        inter = pfit.p2;
        % get confidence intervals
        cint  = confint(pfit);
        % and error
        err   = predint(pfit,(1./(log(2).*xvec)),0.95,'functional','off');
        % plot model fit
        myupdownfill(xvec',err(:,1),err(:,2),[0.3*[1,1,1]],0.3,0);%a
        plot(xvec,pfit(1./(log(2).*xvec)),':','LineWidth',2,'Color',[0.3*[1,1,1],0.8]);
        for g = [1,2,4]%1:4
            vals = mss1{g,sc,nc};
            ms   = mean(vals);
            if(g==1)
               % max y axis 
               maxy = 1.3*ms; 
            end
            err  = std(vals)./sqrt(length(vals));
            axes(ha1); hold on;
            errorcross(grates(1,g),ms,gratesE(1,g),err,0.2*[1,1,1],1,[0.03,maxy/50]);
            plot(grates(1,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));  
        end
        axis([0.3 2 0 maxy]);
        xticks([0.5,1,1.5,2]);
        %yticks([0,1,2,3]);
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[outfolder,'FIG3_C_CONDITION_',num2str(sc),'_',num2str(nc)]);
        close(fig);    
    end
end

%% ================ SUPP FIG 3 (model fit errors) ================ 
cvec = [4:15];
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    for nc = cvec
        yvals = [];
        xvals = [];
        for g = [1,2,3,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            % get the inverse of the growth rate without additional damage
            xvals = [xvals;1./(log(2).*grates(1,g).*ones(size(vals)))];
        end
        % linear fit (force intercept to 0)
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;
        inter = pfit.p2;
        for g = [1,2,3,4]%1:4
            pred = pfit(1./(log(2).*grates(1,g))); % predicted value
            vals = 100*(mss1{g,sc,nc}-pred)./pred; % relative error
            ms   = mean(vals); % mean
            err  = std(vals)./sqrt(length(vals)); % error
            axes(ha1); hold on;
            errorcross(nc,ms,0,err,colors(g,:),1,[0.2,1]);%[0.03,maxy/50]
            plot(nc,ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));  
        end
    end
    axis([3 16 -100 100]);
    xticks([3,6,9,12,15]);
    yticks([-100,-50,0,50,100]);
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_E_CONDITION_',num2str(sc)]);
    close(fig);    
end

%% ================ SUPP FIG 3 (model fit alphas) ================ 
cvec = [4:15];
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    maxy = 0;
    for nc = cvec
        yvals = [];
        xvals = [];
        for g = [1,2,3,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            xvals = [xvals;1./(log(2).*grates(1,g).*ones(size(vals)))];
        end
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;   
        if(slope>maxy)
            maxy = slope;
        end
        cfit = confint(pfit);
        err = mean(abs(cfit(:,1)-slope));
        axes(ha1); hold on;
        errorcross(nc,slope,0,err,0.2*[1,1,1],1,[0.2,0]);%[0.03,maxy/50]
        plot(nc,slope,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',0.8*[1,1,1]);  
    end
    axis([3 16 0 maxy*1.5]);
    xticks([3,6,9,12,15]);
% $$$     yticks([-100,-50,0,50,100]);
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_D_CONDITION_',num2str(sc)]);
    close(fig);    
end

%% ================ EXTRA SUPP FIGURES (constitutive) ================ 
% order: spontaneous, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3,delta lexA
cbins = linspace(-1,2,50); % const log bins
refcnt = zeros(4,1); % reference
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets5(r)  = mean(vals);
        end
        refcnt(g) = mean(mets5);
    end
end
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)'];
linespecs = {'o','>','<','d','v','^','s'};
pcut = 85; % percentile threshold
% histograms
mss1 = cell(4,8); % log hist
ers1 = cell(4,8); 
mss2 = cell(4,8); % log cum hist
ers2 = cell(4,8); 
% computed values
mss3 = zeros(4,8); % mean value below pcut percentile
ers3 = zeros(4,8); 
mss4 = zeros(4,8); % mean value above pcut percentile
ers4 = zeros(4,8); 
mss5 = zeros(4,8); % total mean value
ers5 = zeros(4,8); 
% Compute histograms and values
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs  = zeros(nr,length(cbins));
        sfs   = zeros(nr,length(cbins));
        sfs2  = zeros(nr,length(cbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log((dat.ctfluos{r}(:,2)./dat.careas{r})./refcnt(g));
            sfs(r,:)  = 100*relhist(vals,cbins);
            mets3(r)  = mean(exp(vals));
            vals      = log((dat.ctfluos{r}(:,2)./dat.careas{r})./refcnt(4));
            sfs2(r,:) = 100*relhist(vals,cbins);
            mets4(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(sfs2);
        ers2{g,sc} = std(sfs2)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
    end
end

%% ================ EXTRA SUPP FIGURES (constitutive relative histograms) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,10,[0,0.4],5,1,0,[1:5]);
    axis([log(0.5) log(5) 0 20]);
    ha2 = axes('Units','Pixels','Position',[300+20,190,180,200]); hold on;
    smartlogticks(0,10,[0,0.05],5,1,0,[1:5]);
    axis([log(1) log(5) 0 2]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,3,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG4_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ EXTRA SUPP FIGURES (constitutive histograms) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,10,[0,0.4],5,1,0,[1:5]);
    axis([log(0.5) log(8) 0 20]);
% $$$     ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
% $$$     smartlogticks(0,10,[0,0.05],5,1,0,[1:5]);
% $$$     axis([log(1) log(5) 0 3]);
% $$$     set([fig,ha2],'Units','normalized');
    for g = [1,2,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
% $$$         axes(ha2); hold on;
% $$$         myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
% $$$         plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG4_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ EXTRA SUPP FIGURES (constitutive absolute average) ================ 
fig = figure();
ha1 = axes(); hold on;
%for sc = [1,5,6,7]%1:3
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 3]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_C_CONDITION_Chronic']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
%for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 3]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_C_CONDITION_Cipro']);
close(fig);

%% ================ EXTRA SUPP FIGURES (constitutive relative average) ================ 
fig = figure();
ha1 = axes(); hold on;
%for sc = [1,5,6,7]%1:3
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 2]);
    xticks([0.5,1,1.5,2]);
    yticks([0,0.5,1,1.5,2]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_D_CONDITION_Chronic']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
%for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 2]);
    xticks([0.5,1,1.5,2]);
    yticks([0,0.5,1,1.5,2]);
    for g = [1,2,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG4_D_CONDITION_Cipro']);
close(fig);

%% ================ EXTRA SUPP FIGURES (cell size) ================ 
cbins = linspace(-2,4,50); % size bins
refsize = zeros(4,1);
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = dat.careas{r};
            mets5(r)  = mean(vals);
        end
        refsize(g) = mean(mets5);
    end
end
% order: spontaneous, r-pal, l-pal, 2 pal, cipro 1, cipro 2, cipro 3,delta lexA
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)'];
linespecs = {'o','>','<','d','v','^','s'};
pcut = 85; % percentile threshold
% histograms
mss1 = cell(4,8); % log hist
ers1 = cell(4,8); 
mss2 = cell(4,8); % log cum hist
ers2 = cell(4,8); 
% computed values
mss3 = zeros(4,8); % mean value below pcut percentile
ers3 = zeros(4,8); 
mss4 = zeros(4,8); % mean value above pcut percentile
ers4 = zeros(4,8); 
mss5 = zeros(4,8); % total mean value
ers5 = zeros(4,8); 
% Compute histograms and values
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs  = zeros(nr,length(cbins));
        sfs   = zeros(nr,length(cbins));
        sfs2  = zeros(nr,length(cbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = log(dat.careas{r}./refsize(g));
            sfs(r,:)  = 100*relhist(vals,cbins);
            mets3(r)  = mean(exp(vals));
            vals      = log(dat.careas{r}.*(0.16^2)); % convert to um^2
            sfs2(r,:) = 100*relhist(vals,cbins);
            mets4(r)  = mean(exp(vals));
        end
        mss1{g,sc} = mean(sfs);
        ers1{g,sc} = std(sfs)./sqrt(nr);
        mss2{g,sc} = mean(sfs2);
        ers2{g,sc} = std(sfs2)./sqrt(nr);
        mss3(g,sc) = mean(mets3);
        ers3(g,sc) = std(mets3)./sqrt(nr);
        mss4(g,sc) = mean(mets4);
        ers4(g,sc) = std(mets4)./sqrt(nr);
    end
end

%% ================ EXTRA SUPP FIGURES (histogram size absolute) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,50,[0,0.4],6,5,1,[1,2,3,5]);
    xticks(log([1,2,4,8,16]));
    xticklabels({'1','2','4','8','16'});
    axis([log(0.3) log(50) 0 20]);
% $$$     ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
% $$$     smartlogticks(0,40,[0,0.1],5,4,1,[1,2,3,5]);
% $$$     axis([log(1) log(40) 0 5]);
% $$$     set([fig,ha2],'Units','normalized');
    for g = [1,2,3,4]%1:4
        ms = mss2{g,sc};
        err = ers2{g,sc};
        axes(ha1); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
% $$$         axes(ha2); hold on;
% $$$         myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
% $$$         plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG5_A_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ EXTRA SUPP FIGURES (histogram relative size absolute) ================ 
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    smartlogticks(0,40,[0,0.4],5,4,1,[1,2,3,5]);
    xticks(log([1,2,4,8,16]));
    xticklabels({'1','2','4','8','16'});
    axis([log(0.3) log(40) 0 20]);
    ha2 = axes('Units','Pixels','Position',[300,190,200,200]); hold on;
    smartlogticks(0,40,[0,0.1],5,4,1,[1,2,3,5]);
    axis([log(1) log(40) 0 5]);
    set([fig,ha2],'Units','normalized');
    for g = [1,2,3,4]%1:4
        ms = mss1{g,sc};
        err = ers1{g,sc};
        axes(ha1); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
        axes(ha2); hold on;
        myupdownfill(cbins',[ms+err]',[ms-err]',colors(g,:),0.3,0);%
        plot(cbins,ms,'LineWidth',2,'Color',colors(g,:));
    end
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'SUPPFIG5_B_CONDITION',num2str(sc)]);
    close(fig);
end

%% ================ EXTRA SUPP FIGURES (size absolute average) ================ 
fig = figure();
ha1 = axes(); hold on;
%for sc = [1,5,6,7]%1:3
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 8]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,4,6,8]);
    for g = [1,2,3,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG5_C_CONDITION_Chronic']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
%for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 8]);
    xticks([0.5,1,1.5,2]);
    yticks([0,2,4,6,8]);
    for g = [1,2,3,4]%1:4
        axes(ha1); hold on;
        ms = mss4(g,sc);
        err = ers4(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG5_C_CONDITION_Cipro']);
close(fig);

%% ================ EXTRA SUPP FIGURES (size relative average) ================ 
fig = figure();
ha1 = axes(); hold on;
%for sc = [1,5,6,7]%1:3
for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 4]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,3,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG5_D_CONDITION_Chronic']);
close(fig);
fig = figure();
ha1 = axes(); hold on;
for sc = [1,5,6,7]%1:3
%for sc = [1,2,3,4]%1:3
    axis([0.3 2 0 4]);
    xticks([0.5,1,1.5,2]);
    yticks([0,1,2,3,4]);
    for g = [1,2,3,4]%1:4
        axes(ha1); hold on;
        ms = mss3(g,sc);
        err = ers3(g,sc);
        errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,.05]);
        plot(grates(sc,g),ms,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',colors(g,:));      
    end
end
mypdfpngexpfig('PaperPosition',[0 0 8 7],...
               'outname',[outfolder,'SUPPFIG5_D_CONDITION_Cipro']);
close(fig);

%% ================ EXTRA SUPP FIGURES (Scatter plots) ================ 
mss1 = cell(4,7); % 
mss2 = cell(4,7); % 
mss3 = cell(4,7); % 
refsize = zeros(4,1);
refsize = zeros(4,1);
for sn = [2]
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            vals      = dat.careas{r};
            mets5(r)  = mean(vals);
            vals      = (dat.ctfluos{r}(:,2)./dat.careas{r});
            mets4(r)  = mean(vals);
        end
        % calculate reference values
        refcnt(g)  = mean(mets4);
        refsize(g) = mean(mets5);
    end
end
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = 1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        csfs  = zeros(nr,length(cbins));
        sfs   = zeros(nr,length(cbins));
        sfs2  = zeros(nr,length(cbins));
        mets3 = zeros(nr,1);
        mets4 = zeros(nr,1);
        mets5 = zeros(nr,1);
        for r = 1:nr
            % SOS
            vals1     = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            mss1{g,sc} = [mss1{g,sc};vals1];
            % constitutive
            vals2     = (dat.ctfluos{r}(:,2)./dat.careas{r})./refcnt(4);
            mss2{g,sc} = [mss2{g,sc};vals2];
            % size
            vals3     = dat.careas{r}.*(0.16^2);
            mss3{g,sc} = [mss3{g,sc};vals3];
        end
    end
end

%% ================ EXTRA SUPP FIGURES (constitutive v/s SOS) ================ 
maxn = 12000;
for sc = 1:7
    for g = 1:4        
        fig = figure();
        vals1 = mss1{g,sc};
        vals2 = mss2{g,sc};
        pos   = floor((length(vals1)-1)*rand(maxn,1)+1);
        vals1 = vals1(pos);
        vals2 = vals2(pos);
        dscatter(vals2,vals1,'logx',1,'logy',1);
        axis([0.3 10 0.5 50]);
        xticks([0.5,1,2,5,10]);
        yticks([1,4,8,16,32]);
        grid on;
        grid minor;
        colormap('gray');
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[outfolder,'SUPPFIG6_A_CONDITION_',num2str(sc),'_',num2str(g)]);
        close(fig);
    end
end

%% ================ EXTRA SUPP FIGURES (size v/s SOS) ================ 
maxn = 12000;
for sc = 1:7
    for g = 1:4        
        fig = figure();
        vals1 = mss1{g,sc};
        vals3 = mss3{g,sc};
        pos   = floor((length(vals1)-1)*rand(maxn,1)+1);
        dscatter(vals3,vals1,'logx',1,'logy',1);
        axis([0.2 50 0.5 50]);
        xticks([1,4,8,16,32]);
        yticks([1,4,8,16,32]);
        grid on;
        grid minor;
        colormap('gray');
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[outfolder,'SUPPFIG6_B_CONDITION_',num2str(sc),'_',num2str(g)]);
        pause(0.5);
        close(fig);
        pause(0.5);
    end
end

%% ================ EXTRA SUPP FIGURES (size v/s constitutive) ================ 
maxn = 12000;
for sc = 1:7
    for g = 1:4        
        fig = figure();
        vals2 = mss2{g,sc};
        vals3 = mss3{g,sc};
        pos   = floor((length(vals1)-1)*rand(maxn,1)+1);
        vals2 = vals2(pos);
        vals3 = vals3(pos);
        dscatter(vals3,vals2,'logx',1,'logy',1);
        axis([0.2 50 0.3 10 ]);
        yticks([0.5,1,2,5,10]);
        xticks([1,4,8,16,32]);
        grid on;
        grid minor;
        colormap('gray');
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[outfolder,'SUPPFIG6_C_CONDITION_',num2str(sc),'_',num2str(g)]);
        pause(0.5);
        close(fig);
        pause(0.5);
    end
end
    
