organize_data2;
outfolder = 'paperfigs18/';
%% ================ Ref SOS  ================ 
% Compute reference value in SOS
sn = 2; % spontaneous
refsoss = zeros(4,1);
for g = [1,2,3,4]
    s       = sn;
    dat     = data{g,s};
    nr      = length(dat.careas);
    mets5   = [];
    for r = 1:nr
        vals      = dat.ctfluos{r}(:,1)./dat.careas{r};
        mets5(r)  = mean(vals);
    end
    refsoss(g) = mean(mets5);
end
refsos = mean(refsoss([1,2,4])); % reference SOS (spontaneous)
% Binning for SOS
lbins = linspace(-1,4,50);

%% ================ FIG 3 (model fits) ================ 
grates    = [gratesA(:,1)';...
             gratesA(:,6)';...
             gratesA(:,5)';...
             gratesA(:,7)';...
             gratesA(:,2)';...
             gratesA(:,3)';...
             gratesA(:,4)';...
             gratesA(:,8)'];
gratesE   = [gratesEA(:,1)';...
             gratesEA(:,6)';...
             gratesEA(:,5)';...
             gratesEA(:,7)';...
             gratesEA(:,2)';...
             gratesEA(:,3)';...
             gratesEA(:,4)';...
             gratesEA(:,8)'];

cut  = [1:30]; % a.u. SOS thresholds
mss1 = cell(4,7,length(cut)); % 
sc = 0;
for sn = [2,9,8,10,5,6,7]
    sc = sc+1;
    for g = [1,2,4]%1:4 %grate
        s   = sn;
        dat = data{g,s};
        nr  = length(dat.careas);
        mets3 = zeros(nr,length(cut));
        csfs  = zeros(nr,length(lbins));
        for r = 1:nr
            vals = (dat.ctfluos{r}(:,1)./dat.careas{r})./refsos;
            for nc = 1:length(cut)
                mets3(r,nc) = length(find(vals>=cut(nc)))./length(vals);
            end
            vals  = log((dat.ctfluos{r}(:,1)./dat.careas{r})./refsos);
            for c = 1:length(lbins)
                csfs(r,c) = (length(find(vals>=lbins(c)))./length(vals));
            end
        end
        for nc = 1:length(cut)
            mss1{g,sc,nc} = mets3(:,nc);
        end
        mss2{g,sc} = mean(csfs);
        ers2{g,sc} = std(csfs)./sqrt(nr);
    end
end

sosthr = 10;

%% ================ FIG 3 (model fits) ================ 
cvec = [4:15]; % list of thresholds
for nc = cvec
    for sc = 1:7
        fig = figure();
        ha1 = axes(); hold on;
        yvals = [];
        xvals = [];
        for g = [1,2,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            % get the inverse of the growth rate without additional damage
            xvals = [xvals;1./(log(2).*grates(sc,g).*ones(size(vals)))];
        end
        % linear fit (force intercept to 0)
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;
        inter = pfit.p2;
        % get confidence intervals
        cint  = confint(pfit);
        % and error
        err   = predint(pfit,(1./(log(2).*xvec)),0.95,'functional','off');
        % plot model fit
        myupdownfill(xvec',err(:,1),err(:,2),[0.3*[1,1,1]],0.3,0);%a
        plot(xvec,pfit(1./(log(2).*xvec)),':','LineWidth',2,'Color',[0.3*[1,1,1],0.8]);
        for g = [1,2,4]%1:4
            vals = mss1{g,sc,nc};
            ms   = mean(vals);
            if(g==1)
               % max y axis 
               maxy = 1.3*ms; 
            end
            err  = std(vals)./sqrt(length(vals));
            axes(ha1); hold on;
            errorcross(grates(sc,g),ms,gratesE(sc,g),err,0.2*[1,1,1],1,[0.03,maxy/50]);
            plot(grates(sc,g),ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));  
        end
        axis([0.3 2 0 maxy]);
        xticks([0.5,1,1.5,2]);
        %yticks([0,1,2,3]);
        mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                       'outname',[outfolder,'FIG3_C_CONDITION_',num2str(sc),'_',num2str(nc)]);
        close(fig);    
    end
end

%% ================ SUPP FIG 3 (model fit errors) ================ 
cvec = [4:15];
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    for nc = cvec
        yvals = [];
        xvals = [];
        for g = [1,2,3,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            % get the inverse of the growth rate without additional damage
            xvals = [xvals;1./(log(2).*grates(sc,g).*ones(size(vals)))];
        end
        % linear fit (force intercept to 0)
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;
        inter = pfit.p2;
        for g = [1,2,3,4]%1:4
            pred = pfit(1./(log(2).*grates(sc,g))); % predicted value
            vals = 100*(mss1{g,sc,nc}-pred)./pred; % relative error
            ms   = mean(vals); % mean
            err  = std(vals)./sqrt(length(vals)); % error
            axes(ha1); hold on;
            errorcross(nc,ms,0,err,colors(g,:),1,[0.2,1]);%[0.03,maxy/50]
            plot(nc,ms,linespecs{sc},...
                 'MarkerSize',7,...
                 'MarkerEdgeColor','black',...
                 'MarkerFaceColor',colors(g,:));  
        end
    end
    axis([3 16 -100 100]);
    xticks([3,6,9,12,15]);
    yticks([-100,-50,0,50,100]);
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_E_CONDITION_',num2str(sc)]);
    close(fig);    
end

%% ================ SUPP FIG 3 (model fit alphas) ================ 
cvec = [4:15];
for sc = 1:7
    fig = figure();
    ha1 = axes(); hold on;
    maxy = 0;
    for nc = cvec
        yvals = [];
        xvals = [];
        for g = [1,2,3,4]%1:4
            vals  = mss1{g,sc,nc};
            yvals = [yvals;vals];
            xvals = [xvals;1./(log(2).*grates(sc,g).*ones(size(vals)))];
        end
        foptions = fitoptions('poly1');
        foptions.Lower = [0 0];
        foptions.Upper = [Inf 0];
        [pfit,gof] = fit(xvals,yvals,'poly1',foptions);% 
        xvec = linspace(0.3,2,100);
        slope = pfit.p1;   
        if(slope>maxy)
            maxy = slope;
        end
        cfit = confint(pfit);
        err = mean(abs(cfit(:,1)-slope));
        axes(ha1); hold on;
        errorcross(nc,slope,0,err,0.2*[1,1,1],1,[0.2,0]);%[0.03,maxy/50]
        plot(nc,slope,linespecs{sc},...
             'MarkerSize',7,...
             'MarkerEdgeColor','black',...
             'MarkerFaceColor',0.8*[1,1,1]);  
    end
    axis([3 16 0 maxy*1.5]);
    xticks([3,6,9,12,15]);
% $$$     yticks([-100,-50,0,50,100]);
    mypdfpngexpfig('PaperPosition',[0 0 8 7],...
                   'outname',[outfolder,'FIG3_D_CONDITION_',num2str(sc)]);
    close(fig);    
end
