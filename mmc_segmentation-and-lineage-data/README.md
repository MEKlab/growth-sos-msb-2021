Folders organised by strain.

Files are labelled by date of experiment and medium.

**BACMMAN output**

Suffix key: 

- "_2" = constitutive expression channel - PtetO-mKate2
- "_3" = SOS expression channel - PsulA-mGFP

**Data tables**

- Lineages are organise by _row_
- Time intervals are organised by _column_. For example, Gluaa datasets were imaged in 5 minute intervals, therefore, each column represents 5 minutes.

Suffix key: 

- "_areamats" = cell area [µm^2]
- "_divframemats" = indicates time at which divisions occurs within a lineage [hours]
- "_divmats" = interdivision times [min]
- "_gfpmat_scaled" = SOS fluorescence marker PsulA-mGFP (normalised by cell area) [arbitrary units]
- "_growthratemats" = cell length growth rates [/h] 
- "_lengthmats" = cell length [µm]
- "_mchmats" = constitutive fluorescence marker PtetO-mKate2 (normalised by cell area) [arbitrary units]

